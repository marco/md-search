#!/usr/bin/env bash

# Load files from /run/secrets/env and use them to populate the environment
if [ -d /run/secrets/env ]; then
  for f in /run/secrets/env/*; do
    [ -e "$f" ] && export $(cat "$f")
  done
fi

exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /registry-search-service.jar