#!/usr/bin/env python3

#  Copyright (c) 2019 Marco Massenzio. All rights reserved.
#
#

#
#
import json
import os
import time

import requests

from rs_utils import auto_detect_type

host = os.getenv("ES_HOST")
port = os.getenv("ES_PORT")
retries = int(os.getenv("RETRIES", "15"))

host = "http://{}:{}".format(host, port)
health = "_cat/health"
headers = {'Accept': 'application/json'}
index = "docs"

index_body = {
    "mappings": {
        "library": {
            "properties": {
                "model.versions": {
                    "type": "nested"
                }
            }
        },
        "artifact": {
            "properties": {
                "model.artifactLinks.url": {
                    "type": "keyword"
                }
            }
        }
    }
}


print("Connecting to:", host)
connected = False

print("Waiting for {} seconds at most".format(retries))
while retries > 0 and not connected:
    try:
        res = requests.get("{}/{}".format(host, health), headers=headers)
        connected = True
    except:
        time.sleep(1)
        retries -= 1
        continue

if connected and res.ok:
    status = res.json()[0].get('status')
else:
    print("Server unavailable")
    exit(1)

print("Status:", status)
if status == 'red':
    print("Elasticsearch server is not in a healthy state, aborting tests")
    exit(1)

print("Creating '{}' index".format(index))
with open('index_config.json') as jj:
    idx_config = ''.join([l.strip() for l in jj.readlines()])

res = requests.put("{}/{}".format(host, index), data=idx_config)
if not res.ok:
    print("Failed to create the index, aborting. ({}) {}".format(
        res.status_code, res.reason))
    exit(1)

with open('testdata.json') as testdata:
    data = json.load(testdata)

# Sanity check.
assert isinstance(data, list)
print("Inserting {} items in the '{}' index".format(len(data), index))

for item in data:
    try:
        doctype = auto_detect_type(item)
    except ValueError as e:
        print(e)
        continue
    # Remove the "type" field, if present, which is not part of the ModelWrapper classes.
    print("POSTing {} id: {}".format(item.pop("type", doctype), item.get('id')))
    res = requests.post("{}/{}/{}/{}".format(host, index, doctype, item.get('id')),
                        headers=headers, json=item)
    if not res.ok:
        print("Failed ({}): {}".format(res.status_code, res.reason))

print("SUCCESS Test data uploaded")
