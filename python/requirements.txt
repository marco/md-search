# Created M. Massenzio, 2017-03-14
#
# Requirements to run RMQ test scripts.

pika==0.10.0
requests==2.13.0
robotframework==3.0.2
