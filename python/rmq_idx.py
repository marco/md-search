#!/usr/bin/env python
#
#  Copyright (c) 2019 Marco Massenzio. All rights reserved.
#
# Testing script to send messages to RS via a locally running instance
# of RMQ - can produce both indexing requests and popularity updates.


import argparse
import json

import pika

ALLOWED_TYPES = ['LIBRARY', 'ARTIFACT']
DEFAULT_TYPE = ALLOWED_TYPES[0]
QUEUE_IDX = 'elasticsearch-index'
QUEUE_POP = 'elasticsearch-popularity'

HDR = {
    '__TypeId__': 'io.kapsules.search.api.data.SearchServiceMessage',
}

USAGE = """Posts a message to a local RabbitMQ queue to index the given item in
its registry, or update its popularity ranking."""


def send(channel, queue, msg):
    """ Sends a message in JSON format on the given `queue` using an already open `channel`.

    :param channel: the channel to use to connect to a RMQ server
    :param queue: the queue name that the message will be posted to
    :param msg: the body of the message to be converted to JSON
    :type msg: dict

    :return: None
    """
    payload = json.dumps(msg)
    properties = pika.BasicProperties(app_id='registry-search-indexer',
                                      headers=HDR,
                                      content_type='application/json')
    print("Publishing JSON: {}".format(payload))
    channel.basic_publish(exchange='', routing_key=queue,
                          body=payload, properties=properties)


def post_msg(queue, lib_type, lib_id, lib_pop=None):
    """ Opens a connection channel to a running RMQ server on localhost and posts the message.

    :param queue: the name of the queue to use (one of QUEUE_IDX or QUEUE_POP)
    :param lib_type: whether a library or a vulnerability
    :param lib_id: the index ID
    :param lib_pop: optional, the popularity ranking for the Library (default: None; an index
    request will be sent instead)
    :return: None
    """
    creds = pika.credentials.PlainCredentials('local', 'local')
    conn = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=creds))
    ch = conn.channel()

    body = {
        # TODO: replace the key with 'type' when the name is updated in RS.
        'indexType': lib_type,
        'id': lib_id,
        'popularity': lib_pop
    }

    send(ch, queue, body)
    conn.close()


def index(indx, doc_type):
    """ Convenience method, sends an index request to the RS server.

    :param indx: the entity ID
    :param doc_type: whether a LIBRARY or ARTIFACT
    :return: None
    """
    post_msg(QUEUE_IDX, doc_type, indx)


def update_popularity(indx, pop):
    """ Convenience method to update a Library popularity ranking score.

    :param indx: the library ID
    :param pop: its popularity score
    :return: None
    """
    post_msg(QUEUE_POP, 'LIBRARY', indx, pop)


def parse_args():
    """ Parse command line arguments and returns a configuration.

        :return: the configured namespace from the CLI arguments
        :rtype: Namespace
    """
    parser = argparse.ArgumentParser(description=USAGE)

    parser.add_argument("--type", choices=ALLOWED_TYPES, default=DEFAULT_TYPE,
                        help="Whether a LIBRARY or ARTIFACT should be updated: "
                             "optional, if missing '{default}' will be used.".format(
                            default=DEFAULT_TYPE))

    parser.add_argument("id", type=int,
                        help="The unique id for the artifact/library, as known internally "
                             "in the system.")

    parser.add_argument("popularity", nargs='?', type=float,
                        help="An optional float value to update the popularity ranking "
                             "of the Library whose; `id` matches ID (if missing, a re-index "
                             "message will be instead sent).")

    return parser.parse_args()


def main(config):
    doc_idx = config.id
    doc_type = config.type

    if config.popularity:
        doc_pop = config.popularity
        if doc_type != 'LIBRARY':
            raise NotImplementedError("Only Libraries currently support popularity ranking")
        update_popularity(doc_idx, doc_pop)
    else:
        index(doc_idx, doc_type)


if __name__ == '__main__':
    main(parse_args())
