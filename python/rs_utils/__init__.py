#  Copyright (c) 2019 Marco Massenzio. All rights reserved.
#
#

import sys
import time

import requests

from rmq_idx import index, ALLOWED_TYPES


def auto_detect_type(item):
    """ Returns whether the item is a library or a vulnerability document.

        If `type` is already declared for the `item` then this method takes the
        lazy approach and just returns that.

        Otherwise, based on whether it has the `_severity` field for `artifact` or
        the `_vulnCount` for `library`, it returns the document type to use
        in building the search API path.

        :param item: either a library or vulnerability document
        :type item: dict

        :return: the type of the document, all lowercase, to be compatible with ES index names
        :rtype: str
    """
    if 'type' in item:
        doctype = item.get('type', '')
        if doctype not in ALLOWED_TYPES:
            raise ValueError("Item type {} is not one of the allowed types ({})".format(
                doctype, ALLOWED_TYPES))
        return doctype.lower()

    if item.get('_severity'):
        return 'artifact'
    elif item.get('_vulnCount') is not None:
        return 'library'
    else:
        raise ValueError("Item id = '{}' cannot be determined to be either library or "
                         "vulnerability: `_vulnCount` = {}, `_severity` = {}".format(
            item.get('id'),
            item.get('_vulnCount'),
            item.get('_severity')))


def rebuild_testdata(ids, host='localhost', port=9200, es_index="docs",
                     wait_for=30):
    """ Rebuilds the test data, by forcing a re-index of the data from Librarian

        :param ids: the list of (id, type) tuples to re-index
        :type ids: list[tuple]

        :param host: the hostname for the Elasticsearch server
        :param port: the port where the ES server is listening for incoming HTTP requests

        :param es_index: the Elasticsearch index under which the documents are kept
        :param wait_for: because of "throttling" of index requests, there is an interval of time
                that has to elapse, before the updated items are available: specify here how long
                to wait for
        :type wait_for: int

        :return: a list of all the updated entities
        :rtype: list[dict]
    """
    headers = {'Accept': 'application/json'}

    for idx, doctype in ids:
        index(idx, doctype)

    # We need to wait for a while until all docs are indexed; the 10 seconds delay is related to
    # our "throttling" updates (see the Redis code in registry-search).
    print("INFO Waiting for indexing to take place...")
    while wait_for > 0:
        time.sleep(1)
        print(". ", end=" ", flush=True)
        wait_for -= 1
    print()

    results = list()
    for idx, doctype in ids:
        endpoint = "http://{host}:{port}/{index}/{doctype}/{id}".format(
            host=host,
            port=port,
            index=es_index,
            doctype=doctype.lower(),
            id=idx
        )
        response = requests.get(endpoint, headers=headers)
        if response.ok:
            source = response.json().get("_source")
            if source:
                source['type'] = doctype
                results.append(source)
        else:
            print("ERROR Could not find document id = {}, type = {}, url = {}, reason = {}".format(
                idx, doctype, endpoint, response.reason), file=sys.stderr)

    return results


def isup(host, port=None):
    """ Checks whether the RS server at `http://host:port` is up and healthy.

    :param host: the hostname for the application server, or the full URL
    :type host: str

    :param port: the port the RS server is listening on (optional) - ONLY used if
        the hostname is given, not a URL (if using a URL, add the port at the end)
    :type port: int

    :return: whether the server is up and healthy
    :rtype: bool
    """
    if not port and host.startswith('http'):
        endpoint = "{url}/health".format(url=host)
    else:
        endpoint = "http://{host}:{port}/health".format(host=host, port=port)

    try:
        response = requests.get(endpoint)
        return response.ok
    except requests.exceptions.ConnectionError:
        print("ERROR The server (at {host}:{port}) is not responding (endpoint: {api})".format(
            host=host, port=port, api=endpoint), file=sys.stderr)
        return False
