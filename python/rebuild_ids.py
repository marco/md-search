#!/usr/bin/env python3

#  Copyright (c) 2019 Marco Massenzio. All rights reserved.
#
#

#
#
# --- DO NOT USE ---
#
# This is just a "memo" for how the (type, id) data has been extracted
# from existing test data; in actual practice, it is the other way round:
# given the list of (type, id) items to use for test data (in `test_ids.json),
# we build the `testdata.json` file.
#
# WARNING -- THIS WILL DELETE THE test_ids.json FILE

import json
import os

from rs_utils import auto_detect_type

IDS_JSON = 'test_ids.json'
DATAFILE = 'testdata.json'


with open(DATAFILE) as testdata:
    data = ''.join([line.strip() for line in testdata.readlines()])

tdj = json.loads(data)
type_ids = [(item['id'], auto_detect_type(item).upper()) for item in tdj]

if os.path.exists(IDS_JSON):
    print("WARNING - {name} file already exists; moved to '{name}.bak'".format(name=IDS_JSON))
    os.rename(IDS_JSON, '{}.bak'.format(IDS_JSON))

with open(IDS_JSON, 'wt') as test_ids:
    json.dump(type_ids, test_ids, indent=4)
