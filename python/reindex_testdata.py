#!/usr/bin/env python3
#
#  Copyright (c) 2019 Marco Massenzio. All rights reserved.
#

import argparse
import json
import os
import sys

from rs_utils import rebuild_testdata, isup


DEFAULT_TESTDATA = "testdata.json"


def parse_args():
    """ Parse command line arguments and returns a configuration.

        :return: the configured namespace from the CLI arguments
        :rtype: Namespace
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--bogus",
                        help="If some of the testdata has been manually generated and would be "
                             "lost when re-indexing, save it in this file, then pass it as an "
                             "argument when rebuilding the test data (optional)")

    parser.add_argument("--dest", default=DEFAULT_TESTDATA,
                        help="The destination file for the test data; if it already exists, "
                             "a copy will be saved with .bak extension (default: {})".format(
                                DEFAULT_TESTDATA))

    parser.add_argument("-s", dest='host', default="localhost",
                        help="The Elasticsearch server hostname (by default: localhost)")

    parser.add_argument("-p", dest='port', default=9200, type=int,
                        help="The Elasticsearch server port (default: 9200)")

    parser.add_argument("--pause", default=15, type=int,
                        help="The amount of time to wait, in seconds, for the data to be "
                             "re-indexed, before attempting to retrieve it (by default, "
                             "it will wait 15 seconds)")

    parser.add_argument("--url", default="http://localhost:8080",
                        help="The URL for the running RS server; only used to ensure it is "
                             "running and thus able to process the indexing messages (by default "
                             "http://localhost:8080) - for the script to work, the RS server "
                             "must be running")

    parser.add_argument("ids", help="The name of a file containing a list of [id, type] tuples")
    return parser.parse_args()


def main(config):
    if not os.path.exists(config.ids):
        print("ERROR Cannot find file {}, it must exist before this script is executed".format(
            config.ids), file=sys.stderr)
        exit(1)

    if not isup(config.url):
        print("ERROR The RS server is either not running or in a non-healthy state",
              file=sys.stderr)
        exit(1)

    with open(config.ids) as tids:
        items = json.load(tids)

    data = rebuild_testdata(items, host=config.host, port=config.port, wait_for=config.pause)
    print("INFO Read {} items from the server".format(len(data)))

    if config.bogus and os.path.exists(config.bogus):
        with open(config.bogus) as bogus:
            items = json.load(bogus)
        print("INFO Loaded {} bogus items".format(len(items)))

        for item in items:
            data.append(item)

    if os.path.exists(config.dest):
        print("WARN Saving existing {ex} file to {ex}.bak".format(ex=config.dest))
        os.rename(config.dest, "{}.bak".format(config.dest))

    with open(config.dest, 'wt') as td:
        json.dump(data, td, indent=4)

    print("INFO Test data saved to", config.dest)


if __name__ == '__main__':
    main(parse_args())
