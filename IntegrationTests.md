# Running Functional and Integration tests for registry-search

## Configuration and Setup

### Build & Push Image

__Python base image__

    $ cd python && docker build -t python:3.5 -f Dockerfile .
    
Remember to add the private registry to the `insecure-registries` table in Docker's Preferences.

__Python test runner container__

    TODO
    


#### Run the Docker Web UI

From [here](https://github.com/kwk/docker-registry-frontend):

    docker run -d -e ENV_DOCKER_REGISTRY_HOST=... \
        -e ENV_DOCKER_REGISTRY_PORT=5000 \
        -e ENV_DOCKER_REGISTRY_USE_SSL=1 \
        -p 8088:80 \
        docker-registry-frontend:v2
