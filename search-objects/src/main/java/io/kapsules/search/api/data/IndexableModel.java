// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data;

/**
 * Abstracts a wrapper that has an ID (and thus a getter for the ID).
 *
 * @author marco
 */
public interface IndexableModel {
  Long getId();
}
