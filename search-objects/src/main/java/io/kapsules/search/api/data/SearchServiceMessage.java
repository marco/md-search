// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.api.data;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Represent the JSON object that is being sent on "the wire" on a RabbitMQ queue, to this
 * Registry Search service to either signal that one of the entities has been updated and should
 * thus be re-indexed or its popularity attribute may have changed.
 *
 * <p>To properly being processed by the listeners, the messages should have their {@literal
 * content-type} correctly set to be {@literal "application/json"}; see also
 * <a href="http://docs.spring.io/spring-amqp-net/docs/1.0.x/reference/html/amqp.html">
 * the Spring AMQP documentation</a>.
 *
 * @author marco
 */
public class SearchServiceMessage {

  /**
   * Convenience constructor for indexing messages, that do not require setting a popularity
   * ranking value (assumes default value of 0.0).
   *
   * @param type whether a library or vulnerability
   * @param id the unique ID
   */
  public SearchServiceMessage(IndexType type, long id) {
    this(type, id, 0.0f);
  }

  @SuppressWarnings("unused")
  public enum IndexType {
    LIBRARY,
    ARTIFACT,
  }


  // TODO: re-enable once we no longer need to support backward compatibility.
  //  @JsonProperty("type")
  private IndexType indexType;

  @JsonProperty
  private long id;

  @JsonProperty("popularity")
  private float popularityRanking;

  /**
   * Required by the Jackson generator, do not use directly in code.
   */
  public SearchServiceMessage() {
  }

  /**
   * Use this class to send a message via RMQ to either index the given entity (use a {@literal
   * null} for the {@literal popularityRanking}) or update its popularity (only supported for
   * {@link IndexType#LIBRARY} types).
   *
   * @param indexType whether a library or vulnerability
   * @param id the ID of the entity to update
   * @param popularityRanking optional, if a request to update the library popularity ranking
   */
  @SuppressWarnings("unused")
  public SearchServiceMessage(IndexType indexType, long id, float popularityRanking) {
    this.indexType = indexType;
    this.id = id;
    this.popularityRanking = popularityRanking;
  }


  public IndexType getIndexType() {
    return indexType;
  }

  public long getId() {
    return id;
  }

  public float getPopularity() {
    return popularityRanking;
  }

  @Override
  public String toString() {
    return "SearchServiceMessage{indexType=" + indexType
        + ", id=" + id
        + ", popularityRanking=" + popularityRanking + '}';
  }
}
