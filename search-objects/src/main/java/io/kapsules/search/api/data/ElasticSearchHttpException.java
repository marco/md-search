// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.api.data;

import org.apache.http.HttpResponse;

/**
 *  Exception to indicate a greater than 400 response from elasticsearch when we are trying
 *  to index a library or an artifact
 */
public class ElasticSearchHttpException extends RuntimeException {

  private static final long serialVersionUID = 181553077317552565L;

  public ElasticSearchHttpException(String type, long id, HttpResponse response) {
    super("Could not index " + type + ", id: " + id + " status code: " + response.getStatusLine().getStatusCode()
            + " [ " + response.getStatusLine().getReasonPhrase() + "]");
  }
}
