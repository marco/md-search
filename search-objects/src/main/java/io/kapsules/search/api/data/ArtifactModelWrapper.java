// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

import static io.kapsules.search.api.data.Constants.TIMESTAMP_KEY;

/**
 * Wrapper class for a Markdown document, so that it can be
 * indexed and stored by ElasticSearch.
 *
 * @author marco
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ArtifactModelWrapper implements IndexableModel {

  private String docText;

  @JsonProperty("id")
  private Long id;

  @JsonProperty(TIMESTAMP_KEY)
  private Date when;

  @Override
  public Long getId() {
    return this.id;
  }
}
