// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data;

/**
 * The stage of a vulnerability, while it gets analyzed by the Research team.
 *
 * <p>We explicitly exclude those that are not in {@literal public} stages, as defined by the
 * {@link PubliclyAvailable} interface.
 */
public enum Stage implements PubliclyAvailable {

  REJECTED(false),
  RELEASED(true),
  REVIEW(false),
  DRAFT(false),
  QA(false);

  private boolean publiclyAvailable;

  Stage(boolean publiclyAvailable) {
    this.publiclyAvailable = publiclyAvailable;
  }

  @Override
  public boolean isPubliclyAvailable() {
    return publiclyAvailable;
  }

  /**
   * Just some syntactic sugar for the streaming predicate, as Java 8 does not offer a {@literal
   * filterNot()} method.
   *
   * @return the opposite of {@link #isPubliclyAvailable()}
   */
  public boolean unreleased() {
    return !isPubliclyAvailable();
  }
}
