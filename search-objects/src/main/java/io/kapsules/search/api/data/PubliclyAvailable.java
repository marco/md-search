// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data;

/**
 * Some of our data may not be available publicly, due to commercial considerations and/or
 * confidentiality issues (e.g., around vulnerabilities' public disclosure).
 *
 * <p>Those entities ought to implement this interface for ease of consistency.
 *
 * @author marco
 */
public interface PubliclyAvailable {
  boolean isPubliclyAvailable();
}
