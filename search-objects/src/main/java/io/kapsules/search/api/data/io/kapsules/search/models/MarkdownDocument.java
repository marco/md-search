// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data.io.kapsules.search.models;

import java.util.Date;

public class MarkdownDocument {
  public String getDocument() {
    return null;
  }

  public Long getId() {
    return  -1L;
  }

  public Date getReleasedDate() {
    return new Date();
  }

  public String getTitle() {
    return null;
  }
}
