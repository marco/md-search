// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.api.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Encapsulates the results of a search query: carries some metadata, along with the full list of
 * the matching artifacts and libraries.
 *
 * @author marco
 */
@JsonIgnoreProperties
@Data
public class SearchResults {

  public static final SearchResults EMPTY = new SearchResults(0, 1, 0, 0.0f);

  @JsonIgnoreProperties
  @Data
  public static class ResultsMetadata {

    private long hits;

    // Derived values from 'hits' and the given 'offset' and 'size' from the search query, if
    // available.
    private int page;
    private int pageSize;
    private int totalPages;

    @JsonProperty("libraries_count")
    private int librariesCount;

    @JsonProperty("artifacts_count")
    private int artifactsCount;

    @JsonProperty("high_score")
    private float highestScore;

    @JsonProperty("low_score")
    private float lowestScore;

    public ResultsMetadata(long hits, int pageSize, int offset, float highestScore) {
      checkArgument(pageSize > 0, "Cannot create search results with a 0 page size");

      this.hits = hits;
      this.pageSize = pageSize;
      this.highestScore = highestScore;

      // Note these values are necessarily rounded and may indeed be off-by-one if "offset" isn't
      // perfectly aligned with a multiple of "size".
      this.totalPages = (int) Math.ceil((float) hits / pageSize);
      this.page = (int) Math.ceil((float) offset / pageSize) + 1;
    }

    @SuppressWarnings("unused") // required by Jackson to deserialize inbound requests
    public ResultsMetadata() { }
  }

  @JsonIgnoreProperties
  @Data
  public static class Content {
    String type;
    Float hitScore;

    // Unwrapped property key to be expected from wrapper
    int _enhanced;
    Float _severity;
    boolean _vulnerableMethods;
    int _vulnCount;
    Object model;

    /**
     * This is the content of the {@literal _source} field in the returned results from
     * Elasticsearch and will have to be cast to the appropriate type (library or vulnerability)
     * wrapper, based on the metadata.
     *
     * <p>This "wrapper" in turn will contain the actual library/vulnerability data in its
     * {@literal model} field.
     *
     * @see ArtifactModelWrapper
     */
    @JsonUnwrapped
    Object wrapper;

    public Content(String type, Float hitScore, Object content) {
      this.type = type;
      this.hitScore = hitScore;
      this.wrapper = content;
    }

    @SuppressWarnings("unused") // required by Jackson to deserialize inbound requests
    public Content() { }
  }

  /**
   * Constructs an object to hold the results of a search query, for which we already know the
   * number of total hits and the highest score.
   *
   * @param hits the total number of hits for the search
   * @param pageSize the actual number of hits returned
   * @param offset the starting offset of the first result returned, 0-based, in score-ranking order
   * @param highestScore the highest score for the returned results
   */
  public SearchResults(long hits, int pageSize, int offset, float highestScore) {
    metadata = new ResultsMetadata(hits, pageSize, offset, highestScore);
  }

  /**
   * Convenience constructor, for when we do not know (yet) the number of hits and the highest
   * score.
   *
   * @param pageSize the number of results per page returned
   * @param offset the first index (0-based) of the document returned, in score ranking.
   */
  public SearchResults(int pageSize, int offset) {
    this(0, pageSize, offset, 0.0f);
  }

  @JsonCreator
  public SearchResults(@JsonProperty("metadata") ResultsMetadata resultsMetadata,
                       @JsonProperty("contents") List<Content> content) {
    metadata = resultsMetadata;
    contents = content;
  }

  List<Content> contents = Lists.newArrayList();
  final ResultsMetadata metadata;

  public void append(ArtifactModelWrapper artifact, Float score) {
    contents.add(new Content(
            "artifact",
            score,
            artifact
    ));
    metadata.setArtifactsCount(metadata.getArtifactsCount() + 1);
  }

  public void setHits(long hits) {
    metadata.hits = hits;
  }

  public void setHighestScore(float highestScore) {
    metadata.highestScore = highestScore;
  }

  public void setLowestScore(float lowestScore) {
    metadata.lowestScore = lowestScore;
  }
}
