// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * Convenience class to group all constants (DO NOT use hardcoded strings sprinkled all over the
 * code).
 *
 * @author marco
 */
public interface Constants {

  // The main Index for Elasticsearch indexing; contains all the documents, according to their TYPE.
  String MAIN_ES_INDEX = "docs";

  // Document Types for Elasticsearch indexing.
  String ARTIFACT = "artifact";
  String LIBRARY = "library";

  // API Endpoints
  String ARTIFACTS_ENDPOINT = "/" + ARTIFACT;
  String INDEX_ENDPOINT = "/index";
  String LIBRARIES_ENDPOINT = "/" + LIBRARY;
  String QUEUE_ENDPOINT = "/queue";
  String SEARCH_ENDPOINT = "/search";

  String QUERY_PARAM_KEY = "q";


  // INDEX METADATA KEYS
  //
  // These keys are added during the indexing phase to facilitate filtering and scoring.
  // Please keep each of the following groups of keys in alpha order, helps keeping our sanity.
  //
  String ENHANCED_KEY = "_enhanced";
  String POPULARITY_KEY = "_popularity";
  String SCORE_KEY = "_score";
  String SEVERITY_KEY = "_severity";
  String SOURCE_KEY = "_source";
  String TIMESTAMP_KEY = "_when";
  String TYPE_KEY = "_type";
  String VULNERABLE_METHOD_KEY = "_vulnerableMethods";
  String VULN_COUNT_KEY = "_vulnCount";


  // Search Filters constants.
  String MODEL_KEY = "model";
  String VERSIONS_KEY = MODEL_KEY + ".versions";

  String COMPONENT_ID_KEY = MODEL_KEY + ".artifactComponents.componentId";
  String CVE_ID_KEY = MODEL_KEY + ".cveDigits";
  String CVE_YEAR_KEY = MODEL_KEY + ".cveYear";
  String DISCLOSED_KEY = MODEL_KEY + ".disclosureDate";
  String ID_KEY = MODEL_KEY + ".id";
  String LANGUAGE_KEY = MODEL_KEY + ".language";
  String LANGUAGE_TYPE_KEY = MODEL_KEY + ".languageType";
  String LICENSE_NAME_KEY = VERSIONS_KEY + ".licenseInfoModels.license";
  String SOURCE_NAME_KEY = MODEL_KEY + ".coordinateType";
  String STAGE_NAME_KEY = MODEL_KEY + ".stage";

  int MIN_TEARDOWN_LEN = 0;

  // Alternative type: filter for vulnerabilities (ARTIFACT can be used too).
  String VULNERABILITY = "vulnerability";

  // ELASTICSEARCH CONSTANTS
  //
  // Search Scoring constants.
  String SCORING_TIME_FUNCTION = "exp";
  String SCORING_TIME_ORIGIN = "origin";
  //
  // NOTE: the following two names MUST match the names in application.yaml, or however the
  // scoring configuration is set up.
  String SCORING_TIME_OFFSET = "offset";
  String SCORING_TIME_SCALE = "scale";
  //
  // QUERY property keys, defined by Elasticsearch query DSL.
  String BOOL = "bool";
  String FUNCTION_SCORE = "function_score";
  String FUNCTIONS = "functions";
  String MATCH = "match";
  String MULTI_MATCH = "multi_match";
  String MUST = "must";
  String QUERY = "query";
  String RANGE_QUERY = "range";
  String SHOULD = "should";
  String WEIGHT = "weight";
  String FIELD_VALUE_FACTOR = "field_value_factor";
  String BOOST_MODE = "boost_mode";
  String SCORE_MODE = "score_mode";
  String BOOST = "boost";
  String GREATER_THAN_OP = "gt";
  String GREATER_THAN_OR_EQUAL_OP = "gte";
  String LESSER_THAN_OR_EQUAL_OP = "lte";


  String EXCLUDES = "excludes";

  // Search results offset and size.
  String OFFSET = "from";
  String SIZE = "size";

  // The query parameter is a more commonly-used term.
  String Q_OFFSET = "offset";
  // RabbitMQ queues constants
  String RABBIT_INDEX_QUEUE = "elasticsearch-index";
  String RABBIT_INDEX_DEBOUNCED_QUEUE = "elasticsearch-index-debounced";

  String RABBIT_POPULARITY_QUEUE = "elasticsearch-popularity";

  // Redis Keyspaces
  String LIBRARY_KEYSPACE = "libraries";
  String VULNS_KEYSPACE = "vulns";

  long REDIS_INDEX_EXPIRES_IN_SEC = 10;
  String SOURCE_EXCLUDE_MODEL = "?_source_exclude=model";

  String FOUND = "found";

  Pattern CVE_PATTERN = Pattern.compile("(CVE-)?(?<year>\\d{4})-(?<id>\\d+)");
  Pattern ID_MATCH_REGEX_PATTERN =
      Pattern.compile("(?:(?<type>(?:[LS])ID)-)?(?<id>\\d+)", Pattern
          .CASE_INSENSITIVE);

  String RANGE_OP = "\\.\\.";

  Pattern RELEASED_PATTERN = Pattern.compile(
      "(?<op>[<>])?(?<from>\\d{4}-\\d{2}-\\d{2})(" + RANGE_OP + "(?<to>\\d{4}-\\d{2}-\\d{2}))?");

  // Making date format thread safe
  // See: http://stackoverflow.com/questions/4021151/java-dateformat-is-not-threadsafe-what-does-this-leads-to
  // TODO: use java.util.time.DateTimeFormatter instead.
  ThreadLocal<DateFormat> RELEASED_DATE_FORMAT =
          ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));

  Pattern SEVERITY_PATTERN = Pattern.compile(
          "(?<op>[<>])?(?<from>\\d*\\.?\\d*)(" + RANGE_OP + "(?<to>\\d*\\.?\\d*))?");
  /**
   * Used when checking float/double values in test assertions.
   */
  double TOLERANCE = 0.001;
}
