FROM java:8

# Make sure we have updates
RUN set -e; set -x; \
    export DEBIAN_FRONTEND=noninteractive; \
    for i in 1 2 3; do apt-get update && break; done; \
    apt-get purge -y git subversion; \
    apt-get autoremove -y; \
    for i in 1 2 3; do apt-get upgrade -y && break; done; \
    rm -rf /var/*/apt /var/cache/debconf

# TODO: change the location to use Gradle.
COPY search-service/target/search-service*.jar /search-service.jar
COPY dockerfile-entrypoint.sh /

EXPOSE 8080
ENTRYPOINT ["/dockerfile-entrypoint.sh"]
