// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.util;

import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.parsing.SearchQuery;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static io.kapsules.search.test.util.Assert.assertIsEmpty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Tests for my OptionalFunc class.
 *
 * @author marco
 */
public class OptionalFuncTest {
  @Test
  public void ifPresentOrElse() throws Exception {
    String s = "test";
    StringBuilder sb = new StringBuilder();

    OptionalFunc.of(s).ifPresent(sb::append)
        .ifPresent(sb::append)
        .ifPresentOrElse(sb::append, () -> {fail("unexpected empty"); return null; });

    assertThat(sb.toString(), equalTo("testtesttest"));

    sb.setLength(0);
    OptionalFunc.of(null).ifPresent(sb::append)
        .ifPresent(sb::append)
        .ifPresentOrElse(sb::append, () -> { return null; });

    assertThat(StringUtils.isBlank(sb.toString()), is(true));
  }

  @Test
  public void ifPresent() throws Exception {

  }

  @Test
  public void of() throws Exception {
    assertFalse(OptionalFunc.of((String)null).isPresent());
    assertIsEmpty((OptionalFunc.of(null).ifPresent(s -> {
      fail("Should never execute on a null");
    }).value()));

    OptionalFunc.of("foobar").orElseThrow(AssertionError::new);
    OptionalFunc.of(SearchQuery.builder().build()).orElseThrow(AssertionError::new);
    OptionalFunc.of(SearchFilterKey.LANGUAGE).orElseThrow(AssertionError::new);
  }

  @Test
  public void empty() throws Exception {
    StringBuilder sb = new StringBuilder();

    assertFalse(OptionalFunc.empty().isPresent());
    OptionalFunc.empty().ifPresentOrElse(t -> fail("an empty should never trigger execution"),
        () -> { sb.append("success"); return null; });
    assertTrue(sb.length() > 0);
  }
}
