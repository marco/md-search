// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import io.kapsules.search.api.data.Constants;
import io.kapsules.search.keys.SearchWeightKey;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author marco
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "functionaltests")
public class SearchConfigurationIT {

  @Autowired
  private SearchConfiguration searchConfiguration;

  @Test
  public void canConfigureScoringParams() {
    Map<String, Long> recentScoreParams = searchConfiguration.getScoring().getRecent();
    assertEquals(7 * 24 * 3600, recentScoreParams.get("offset") / 1000);
  }

  @Test
  public void hasPopularityConfig() {
    assertTrue(searchConfiguration.getScoring()
        .getFieldValueFactors()
        .stream()
        .anyMatch(p -> Objects.equals(p.getField(), Constants.POPULARITY_KEY)));
  }

  @Test
  public void getWeights() {
    Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>> weights =
        searchConfiguration.getWeights();

    assertThat(weights.size(), equalTo(3));

    assertThat(weights.get(SearchConfiguration.WeightType.DEFAULT), notNullValue());
    assertThat(weights.get(SearchConfiguration.WeightType.LIBRARY), notNullValue());
    assertThat(weights.get(SearchConfiguration.WeightType.ARTIFACT), notNullValue());

    assertThat(weights.get(SearchConfiguration.WeightType.DEFAULT).get(SearchWeightKey.RELEVANCE),
        equalTo(0.1f));
    assertThat(weights.get(SearchConfiguration.WeightType.LIBRARY).get(SearchWeightKey.VULN_COUNT),
        equalTo(0.5f));
  }
}
