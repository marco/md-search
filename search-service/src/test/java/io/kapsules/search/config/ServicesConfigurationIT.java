// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Test for {@literal @ConfigurationProperties} for the various HTTP services that we will be using.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "functionaltests")
public class ServicesConfigurationIT {

  @Value("${services.elasticsearch.hostname}")
  String hostname;

  @Value("${services.elasticsearch.port}")
  int port;

  @Autowired
  ServicesConfiguration configuration;

  @Autowired
  String librarianUrl;

  @Test
  public void canCreate() {
    assertThat(configuration, notNullValue());

    assertThat(configuration.getElasticsearch(), notNullValue());
  }

  @Test
  public void elasticsearchConfigurationIsCorrect() {
    assertEquals(hostname, configuration.getElasticsearch().getHostname());
    assertEquals(port, configuration.getElasticsearch().getPort());
  }

  @Test
  public void canConfigureLibrarianUrl() {
    assertThat(librarianUrl, not(isEmptyOrNullString()));
    assertEquals("http://notavalid.server.fail", librarianUrl);
  }

  @Test
  public void connectionParams() {
    assertThat(configuration.defaultMaxConnectionsPerRoute(), equalTo(10));
    assertThat(configuration.maxTotalConcurrentConnections(), equalTo(10));
  }

}
