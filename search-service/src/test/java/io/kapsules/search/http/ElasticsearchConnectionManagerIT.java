// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.http;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Integration test for the Connection Manager: requires a running instance of Elasticsearch to
 * complete HTTP requests against.
 *
 * @author marco
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "functionaltests")
public class ElasticsearchConnectionManagerIT {

  @Rule
  public Timeout timeout = new Timeout(500, TimeUnit.MILLISECONDS);

  @Autowired
  private ElasticsearchConnectionManager connectionManager;

  private HttpResponse response;

  @After
  public void teardown() {
    if (response != null) {
      EntityUtils.consumeQuietly(response.getEntity());
    }
  }

  @Test
  public void notFound() throws Exception {
    response = connectionManager.sendRequest(HttpMethod.GET.name(), "/nindex/nadocument/445");
    assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_NOT_FOUND));
  }

  /**
   * See RS-54
   */
  @Test(timeout = 2000L)
  public void poolTimeoutsWhenConnectionsExhausted() throws Exception {
    for (int i = 0; i < 10; ++i) {
      HttpResponse response = connectionManager.get("/_cat/health");
      assertThat("Request # " + i + "failed: " + response.getStatusLine().getReasonPhrase(),
          response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
      EntityUtils.consumeQuietly(response.getEntity());
    }
  }
}
