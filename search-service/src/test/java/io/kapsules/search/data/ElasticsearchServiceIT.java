// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.api.data.SearchResults;
import io.kapsules.search.parsing.QueryParseException;
import io.kapsules.search.parsing.QueryParser;
import io.kapsules.search.parsing.SearchQuery;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Date;

import static io.kapsules.search.api.data.Constants.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

/**
 * Integration tests for the {@link ElasticsearchService}: these require a running instance of
 * Elasticsearch, connected according to the configuration in the {@literal functionaltests}
 * Spring profile.
 *
 * @author marco
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "functionaltests")
public class ElasticsearchServiceIT {

  private static final String JSON_QUERY = "{" +
      "  \"query\": {  " +
      "    \"bool\": {  " +
      "      \"must\": [  " +
      "        {  " +
      "          \"match\": { \"model.description\": \"json api\" }  " +
      "        },  " +
      "        {  " +
      "          \"match\": { \"model.name\": \"api\" }  " +
      "        }  " +
      "    ]  " +
      "    }  " +
      "  },  " +
      "  \"from\": 2,  " +
      "  \"size\": 4  " +
      "}";


  @Autowired
  private SearchService service;

  @Autowired
  private QueryParser parser;

  private SearchResults searchFor(String query) throws ParseException {
    SearchQuery sq = parser.parse(query);
    return service.search(sq);
  }

  /**
   * See RS-53
   */
  @Test
  public void typeArtifactReturnsOnlyVulnerabilities() throws ParseException {
    String query = "csv api type:artifact";
    SearchQuery sq = parser.parse(query);
    SearchResults results = service.search(sq);

    assertThat(results.getMetadata().getLibrariesCount(), equalTo(0));
  }

  /**
   * See RS-53
   */
  @Test(expected = ParseException.class)
  public void typeNonsenseReturnsError() throws ParseException {
    service.search(parser.parse("maple syrup type:pancakes"));
  }

  /**
   * See RS-60
   */
  @Test
  public void noSearchResultsReturnEmpty() throws ParseException {
    SearchResults results = searchFor("fettuccineFakeQuery");
    assertEquals(SearchResults.EMPTY, results);
  }


  /**
   * Searching with a query that contains a numeric value that matches a CVE ID ("cveDigits")
   * should result in matching the artifact.
   *
   * See RS-88.
   */
  @Test
  public void findByCveId() throws ParseException {
    String query = "7371";  // CVE ID of Artifact SID-1065
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), greaterThan(0L));
    assertThat(results.getContents().get(0).getType(), equalTo(ARTIFACT));

    ArtifactModelWrapper wrapper = (ArtifactModelWrapper) results.getContents().get(0).getWrapper();
    assertThat(wrapper.getId(), equalTo(1065L));

  }

  /**
   * Searching with a query that contains a numeric value that matches a CVE Year ("cveYear")
   * should result in matching the artifact.
   *
   * See RS-88.
   */
  @Test
  public void findByCveYear() throws ParseException {
    String query = "2016";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(2L));
    results.getContents()
        .forEach(a -> {
          assertThat(a.getType(), equalTo(ARTIFACT));
          ArtifactModelWrapper wrapper = (ArtifactModelWrapper) a.getWrapper();
          assertThat(wrapper.getDocText(), equalTo(query));
        });
  }

  /**
   * See RS-88
   */
  @Test
  public void filterByCveYear() throws ParseException {
    // The only 2 vulns in the dataset for ITs in 2014 have nothing to do with XSS; the ones that
    // do (4 of them) are all in other years (2011 and 2016).
    //
    // A query such as "XSS 2014" would turn up those too; a filter instead prevents any
    // non-2014 result to be returned.
    String query = "XSS cve_year:2014";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(2L));
    results.getContents()
        .forEach(a -> {
          assertThat(a.getType(), equalTo(ARTIFACT));
          // TODO: modified test just to make it compile: this makes no sense.
          assertThat(((ArtifactModelWrapper) a.getWrapper()).getWhen(),
              equalTo("2014"));
        });
  }

  /**
   * See RS-88
   */
  @Test
  public void filterByCveID() throws ParseException {
    // As this filters by CVE ID (which is a unique identifier) any other search term will be
    // irrelevant: we only expect ONE match (again, because this is a *filter*; a query of the
    // form "sql injection 4992" would return many more vulnerabilities, including the one that
    // has "cveDigits: 4992".
    String query = "sql injection xss cve_id:4992";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(1L));

    // "cveDigits": "4992",
    // TODO: test modified to make it compile: makes no sense.
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()),
              equalTo("4992"));
  }

  /**
   * See RS-98
   */
  @Test
  public void filterByCve() throws ParseException {
    String query = "cve:2016-4567";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(1L));

    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getId(),
        equalTo(2350L));

    // "cveDigits": "4567",
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getDocText(),
              equalTo("4567"));
  }

  /**
   * See RS-98
   */
  @Test
  public void filterByCveFull() throws ParseException {
    String query = "cve:CVE-2016-4469";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(1L));
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getId(),
        equalTo(2536L));
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getDocText(),
              equalTo("4469"));
  }


  /**
   * See RS-98
   */
  @Test
  public void findByCveString() throws ParseException {
    String query = "CVE-2016-4469";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(1L));
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getId(),
        equalTo(2536L));
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getDocText(),
        equalTo("4469"));
  }

  /**
   * See RS-102
   */
  @Test
  public void cannotFetchUnreleasedUnlessAllowed() throws ParseException {
    // This is one of the "bogus" artifacts (see bogusdata.json), which has been marked DRAFT.
    String query = "CVE-2017-4492";

    SearchQuery sq = parser.parse(query);
    sq.setPublicView(true);

    SearchResults empty = service.search(sq);
    assertThat(empty.getContents(), Matchers.empty());

    // If we now set the query to be "internal" we should find it.
    sq.setPublicView(false);
    empty = service.search(sq);

    assertThat(empty.getContents().size(), equalTo(1));
    ArtifactModelWrapper wrapper = (ArtifactModelWrapper) empty.getContents().get(0).getWrapper();

    assertThat(wrapper.getId(), equalTo(19876876L));
    assertThat(wrapper.getDocText(), equalTo("DRAFT"));
  }

  @Test
  public void releasedDate() throws ParseException {
    String query ="type:artifact";
    SearchResults results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(12L));

    query ="type:artifact released:>2016-05-01";
    results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(4L));

    query ="type:artifact released:2016-07-01..2016-07-30";
    results = searchFor(query);

    assertThat(results.getMetadata().getHits(), equalTo(2L));
    assertThat(((ArtifactModelWrapper)results.getContents().get(0).getWrapper()).getId(),
        equalTo(1599L));
  }

  @Test(expected = QueryParseException.class)
  public void malformedDateQuery() throws ParseException {
    String query ="type:artifact released:>2016-05-01,2016-05-30";
    searchFor(query);
  }

  @Test
  public void searchByLicense() throws ParseException {
    String query = "type:library language:java license:apache";
    SearchResults results = searchFor(query);
    assertThat(results.getMetadata().getHits(), equalTo(1L));

    assertThat(results.getContents().get(0).getType(), equalTo(LIBRARY));
  }

  @Test
  public void searchByLicenseWithTerms() throws ParseException {
    String query = "JSON parser license:MIT";
    SearchResults results = searchFor(query);
    assertThat(results.getMetadata().getHits(), equalTo(4L));

    results.getContents()
        .forEach(result -> {
          assertThat(result.getType(), equalTo(LIBRARY));
        });
  }

  @Test
  public void searchByDisclosedDate() throws ParseException {
    String query = "type:artifact disclosed:2010-07-28";
    SearchResults results = searchFor(query);
    assertThat(results.getMetadata().getHits(), equalTo(11L));

    query = "type:artifact disclosed:2015-07-28";
    results = searchFor(query);
    assertThat(results.getMetadata().getHits(), equalTo(2L));

    Date compareTo = RELEASED_DATE_FORMAT.get().parse("2015-07-28");
    results.getContents()
        .forEach(result -> {
          assertThat(result.getType(), equalTo(ARTIFACT));
          ArtifactModelWrapper wrapper = (ArtifactModelWrapper) result.getWrapper();
          assertThat(wrapper.getWhen(), greaterThan(compareTo));
        });
  }

  @Test
  public void searchByDisclosedDateRange() throws ParseException {
    String first = "2012-06-30";
    String last = "2015-12-31";
    String query = String.format("type:artifact disclosed:%s..%s", first, last);

    SearchResults results = searchFor(query);
    assertThat(results.getMetadata().getHits(), equalTo(6L));

    Date from = RELEASED_DATE_FORMAT.get().parse(first);
    Date to = RELEASED_DATE_FORMAT.get().parse(last);

    results.getContents()
        .forEach(result -> {
          assertThat(result.getType(), equalTo(ARTIFACT));
          ArtifactModelWrapper wrapper = (ArtifactModelWrapper) result.getWrapper();
          assertThat(wrapper.getWhen(), greaterThan(from));
          assertThat(wrapper.getWhen(), lessThan(to));
        });
  }


}

