// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import com.google.common.collect.Maps;
import io.kapsules.search.config.SearchConfiguration;
import io.kapsules.search.http.ElasticsearchConnectionManager;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.search.parsing.JsonQueryRenderer;
import io.kapsules.search.parsing.SearchQuery;
import io.kapsules.util.JsonUtils;
import org.junit.Before;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Map;

import static io.kapsules.search.api.data.Constants.*;
import static io.kapsules.util.JsonUtils.get;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author marco
 */
public class ElasticsearchServiceTest {

  private ElasticsearchService service;

  @Before
  public void setUp() throws Exception {
    ElasticsearchConnectionManager connectionManager = mock(ElasticsearchConnectionManager.class);
    SearchConfiguration searchConfiguration = mock(SearchConfiguration.class);
    SearchConfiguration.ScoringFunctionsParams params = mock(SearchConfiguration
        .ScoringFunctionsParams.class);

    Map<String, Long> map = Maps.newHashMap();
    map.put(SCORING_TIME_OFFSET, 1000L);
    map.put(SCORING_TIME_SCALE, 1000L);

    when(params.getRecent()).thenReturn(map);

    when(searchConfiguration.getBoostMode()).thenReturn("sum");
    when(searchConfiguration.getScoreMode()).thenReturn("sum");
    when(searchConfiguration.getScoring()).thenReturn(params);

    when(searchConfiguration.getWeights()).thenReturn(createDefaultWeights());

    service = new ElasticsearchService(connectionManager,
        new JsonQueryRenderer(searchConfiguration),
        10);

  }

  /**
   * Helper method to setup some default weights, to avoid NPEs when using a
   * {@link org.mockito.Mock mock configuration}.
   *
   * @return some values for the weights which are entirely random and meaningless: this method
   *    will only ensure that your mock returns valid values when requested.
   */
  public static Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>>
      createDefaultWeights() {
    Map<SearchWeightKey, Float> defaultWeights = Maps.newHashMap();
    defaultWeights.put(SearchWeightKey.TIME, 0.2F);
    defaultWeights.put(SearchWeightKey.RELEVANCE, 0.2F);
    defaultWeights.put(SearchWeightKey.ENHANCED, 0.2F);
    defaultWeights.put(SearchWeightKey.SEVERITY, 0.2F);
    defaultWeights.put(SearchWeightKey.TIME, 0.2F);

    Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>> weights =
        Maps.newHashMap();
    weights.put(SearchConfiguration.WeightType.DEFAULT, defaultWeights);
    weights.put(SearchConfiguration.WeightType.LIBRARY, defaultWeights);
    weights.put(SearchConfiguration.WeightType.ARTIFACT, defaultWeights);

    return weights;
  }

  @Test
  public void getQueryTemplate() throws Exception {
    String template = service.getQueryTemplate();

    JsonObject query = Json.createReader(new StringReader(template)).readObject();
    assertThat(query.toString(), query, notNullValue());

    JsonValue should = JsonUtils.get(query, "query.function_score.query.bool.should");
    assertThat(should.getValueType(), is(JsonValue.ValueType.ARRAY));
    JsonObject multiMatch = ((JsonArray)should).getJsonObject(0);
    assertThat(multiMatch.toString(), multiMatch.getJsonObject("multi_match"), notNullValue());

    JsonValue functions = JsonUtils.get(query, "query.function_score.functions");
    assertThat(functions.getValueType(), is(JsonValue.ValueType.ARRAY));
    JsonObject gauss = ((JsonArray)functions).getJsonObject(0);
    assertThat(gauss.toString(), JsonUtils.get(gauss, "exp._when.offset"), notNullValue());

    assertEquals((Integer)1000, (Integer)((JsonNumber) JsonUtils.get(gauss, "exp._when.offset")).intValue());
  }

  @Test
  public void canFilterByType() throws ParseException {
    SearchQuery.builder()
        .withFilter(SearchFilterKey.TYPE, ARTIFACT)
        .withTerm("sql").build();

  }

  @Test(expected = IllegalArgumentException.class)
  public void testBuildNonsenseThrows() {
    service.buildEndpoint("nonsense");
  }
}
