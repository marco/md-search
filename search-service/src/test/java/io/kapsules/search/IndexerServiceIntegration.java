// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search;

import com.google.common.collect.Lists;
import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.data.Repository;
import io.kapsules.search.services.RegistrySearchIndexerService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;

/**
 * Full-fledged integration test, requires running instances of Librarian and Vuln-service, in
 * addition to Elasticsearch and RMQ containers.
 *
 * <strong>TODO: this needs to be refactored into a "real" integration test.</strong>
 *
 * @author marco
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "qa")
// Renamed to avoid matching the `*IT` pattern and prevent this being run during functional testing.
public class IndexerServiceIntegration {

  // No point in waiting if the server is not responding.
  @Rule
  public Timeout timeout = new Timeout(45, TimeUnit.SECONDS);

  @Autowired
  private RegistrySearchIndexerService service;

  @Autowired
  private Repository<ArtifactModelWrapper> artifactRepository;

  @Test
  public void canFetchOne() throws IOException {
    Long id = 5011L;

    Map<ElasticSearchOutcome, Set<Long>> results = service.indexLibraries(Lists.newArrayList(id));

    Set<Long> successful = results.get(ElasticSearchOutcome.SUCCESS);
    assertThat(successful, notNullValue());
    assertThat(successful, not(empty()));
    assertTrue(successful.contains(id));

    assertThat(results.get(ElasticSearchOutcome.FAILURE), empty());
  }

  @Test
  public void canFetchManyLibraries() throws IOException {
    List<Long> ids = Lists.newArrayList(23364L, 24521L, 5010L);
    Map<ElasticSearchOutcome, Set<Long>> results = service.indexLibraries(ids);

    // Because Hamcrest has badly typed generics, we need to do this, instead of just passing the
    // List of IDs.
    assertThat(results.get(ElasticSearchOutcome.SUCCESS), containsInAnyOrder(23364L, 24521L, 5010L));
    assertThat(results.get(ElasticSearchOutcome.FAILURE), empty());
  }

  @Test
  public void canFetchSomeLibrariesWithErrors() throws IOException {
    List<Long> ids = Lists.newArrayList(23364L, 12349876L, 5010L);
    Map<ElasticSearchOutcome, Set<Long>> results = service.indexLibraries(ids);

    assertThat(results.get(ElasticSearchOutcome.SUCCESS), containsInAnyOrder(23364L, 5010L));
    assertThat(results.get(ElasticSearchOutcome.FAILURE), hasSize(1));
    assertThat(results.get(ElasticSearchOutcome.FAILURE), contains(12349876L));
  }

  @Test
  public void canIndexArtifact() throws Exception {
    artifactRepository.delete(1065L);

    Map<ElasticSearchOutcome, Set<Long>> results = service.indexArtifacts(Lists.newArrayList(1065L));
    assertThat(results.get(ElasticSearchOutcome.SUCCESS).size(), equalTo(1));

    Optional<ArtifactModelWrapper> wrapper = artifactRepository.findOne(1065L);
    assertTrue(wrapper.isPresent());
    assertThat(wrapper.get().getDocText(), notNullValue());
  }

  @Test
  public void canIndexManyArtifacts() throws Exception {
    List<Long> ids = Lists.newArrayList(1065L, 1176L, 1458L, 1197L);
    ids.forEach(artifactRepository::delete);

    Map<ElasticSearchOutcome, Set<Long>> results = service.indexArtifacts(ids);
    assertThat("Found: " + results.get(ElasticSearchOutcome.SUCCESS).toString() + ", but expected: " + ids.toString(),
        results.get(ElasticSearchOutcome.SUCCESS).size(), equalTo(ids.size()));

    // Verify that the artifacts made it to the ES repository.
    ids.forEach(id -> assertThat("Missing artifact ID: " + id,
        artifactRepository.findOne(id), notNullValue()));
  }

  @Test
  public void canIndexManyArtifactsWithErrors() throws Exception {
    List<Long> ids = Lists.newArrayList(876L, 877L, 878L, 879L);
    Map<ElasticSearchOutcome, Set<Long>> results = service.indexArtifacts(ids);

    // I've seen spurious errors here, want to emit an error message when these don't match
    assertThat("Found: " + results.get(ElasticSearchOutcome.SUCCESS).toString() + ", but expected: [876, 878, 879]",
        results.get(ElasticSearchOutcome.SUCCESS).size(), equalTo(3));

    // Artifacts ID 878 does not exist.
    assertThat("Found: " + results.get(ElasticSearchOutcome.FAILURE).toString() + ", but expected: [877]",
        results.get(ElasticSearchOutcome.FAILURE).size(), equalTo(1));
  }

  @Test
  public void checkMetadata() {
    ArtifactModelWrapper wrapper = artifactRepository.findOne(1065L).orElseThrow(() -> {
      return new AssertionError("Could not find artifact 1065");
    });
    // TODO: test gutted to make it compile: re-implement.
  }
}
