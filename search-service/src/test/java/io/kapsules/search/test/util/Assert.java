// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.test.util;

import java.util.Optional;

/**
 * Some useful assertions around new Java 8 paradigms, that were not around when the original
 * {@link org.junit.Assert} class was created.
 *
 * @author marco
 */
// TODO: convert to a Matcher
public class Assert {
  public static<T> void assertIsNotEmpty(Optional<T> value) throws AssertionError {
    if (!value.isPresent()) {
      throw new AssertionError("Optional value expected to be non-empty, but found empty");
    }
  }

  public static<T> void assertIsEmpty(Optional<T> value) throws AssertionError {
    if (value.isPresent()) {
      throw new AssertionError("Optional value expected to be empty, but found " + value.get());
    }
  }
}
