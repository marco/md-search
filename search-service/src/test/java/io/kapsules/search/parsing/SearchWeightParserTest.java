// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.search.test.util.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * @author marco
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
public class SearchWeightParserTest {

  @Test
  public void getWeightValue() throws Exception {
    String token = "wr:0.4";

    Optional<Map.Entry<SearchWeightKey, Float>> res = SearchWeightParser.of(token);

    Assert.assertIsNotEmpty(res);
    assertEquals(SearchWeightKey.RELEVANCE, res.get().getKey());
    assertEquals(0.4, res.get().getValue(), 0.01);
  }

  @Test
  public void getWeightValueIgnoreCase() throws Exception {
    String token = "WE:9.9987";

    Optional<Map.Entry<SearchWeightKey, Float>> res = SearchWeightParser.of(token);

    Assert.assertIsNotEmpty(res);
    assertEquals(SearchWeightKey.ENHANCED, res.get().getKey());
    assertEquals(9.9987, res.get().getValue(), 0.00001);
  }

  /**
   * A bunch of nonsense queries, we want to make sure that no combination, no matter how
   * nonsensical, causes the parsing to throw: they just all return an {@link Optional#empty()}.
   */
  @Test
  public void getWeightValueFails() {
    Optional<Map.Entry<SearchWeightKey, Float>> res =
        SearchWeightParser.of("foobar:0.4");
    Assert.assertIsEmpty(res);

    res = SearchWeightParser.of("wr:not me");
    Assert.assertIsEmpty(res);

    res = SearchWeightParser.of("wr:9a");
    Assert.assertIsEmpty(res);

    res = SearchWeightParser.of("ws:critical");
    Assert.assertIsEmpty(res);

    res = SearchWeightParser.of("wre:55");
    Assert.assertIsEmpty(res);

    // Because, of course...
    res = SearchWeightParser.of("ws:`DROP ALL TABLES`");
    Assert.assertIsEmpty(res);

    // Because, of course...
    res = SearchWeightParser.of("wr:1234567 a very long text that goes on and ona d=ange" +
        " and on and oand onad and on and on and a very very long string and we can " +
        "carry on forever");
    Assert.assertIsEmpty(res);
  }

  @Test
  public void getWeightValueSeverity() throws Exception {
    String token = "ws:1.0";

    Optional<Map.Entry<SearchWeightKey, Float>> res = SearchWeightParser.of(token);

    Assert.assertIsNotEmpty(res);
    assertEquals(SearchWeightKey.SEVERITY, res.get().getKey());
    assertEquals(1.0, res.get().getValue(), 0.01);
  }
}
