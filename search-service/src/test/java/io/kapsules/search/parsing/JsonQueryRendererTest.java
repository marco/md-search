// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.kapsules.search.config.SearchConfiguration;
import io.kapsules.search.data.ElasticsearchServiceTest;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.util.JsonUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;

import static io.kapsules.search.api.data.Constants.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author marco
 */
public class JsonQueryRendererTest {

  private static final double DELTA = 0.0001;

  private JsonQueryRenderer jsonRenderer;
  private SearchConfiguration configuration;

  @Before
  public void setup() {
    configuration = mock(SearchConfiguration.class);
    SearchConfiguration.ScoringFunctionsParams params = mock(SearchConfiguration
        .ScoringFunctionsParams.class);

    Map<String, Long> myValues = Maps.newHashMap();
    myValues.put(SCORING_TIME_OFFSET, 123456L);
    myValues.put(SCORING_TIME_SCALE, 987654L);

    when(configuration.getScoring()).thenReturn(params);
    when(params.getRecent()).thenReturn(myValues);

    when(configuration.getBoostMode()).thenReturn("sum");
    when(configuration.getScoreMode()).thenReturn("log_sqrt");

    when(configuration.getWeights()).thenReturn(ElasticsearchServiceTest.createDefaultWeights());

    SearchConfiguration.FieldValueScoringParams severityParams = new SearchConfiguration
        .FieldValueScoringParams();
    severityParams.setField(SEVERITY_KEY);
    severityParams.setEnabled(true);
    severityParams.setModifier("none");
    severityParams.setMissing(0.0f);
    severityParams.setWeightKey("ws");

    when(params.getFieldValueFactors()).thenReturn(Collections.singletonList(severityParams));

    jsonRenderer = new JsonQueryRenderer(configuration);
  }

  @Test
  public void testLanguageFilter() {
    JsonObject actual = SearchFilterKey.LANGUAGE.json("JAVA").get();
    assertThat(actual.toString(), equalTo("{\"bool\":{\"should\":[" +
        "{\"match\":{\"model.language\":\"JAVA\"}}," +
        "{\"match\":{\"model.languageType\":\"JAVA\"}}" +
        "]}}"));
  }

  //INT-197: Expect javascript to be transformed to js
  @Test
  public void testLanguageJavascriptFilter() {
    JsonObject actual = SearchFilterKey.LANGUAGE.json("javascript").get();
    assertThat(actual.toString(), equalTo("{\"bool\":{\"should\":[" +
            "{\"match\":{\"model.language\":\"js\"}}," +
            "{\"match\":{\"model.languageType\":\"js\"}}" +
            "]}}"));
  }

  @Test
  public void canFilterByVulnMethods() {
    JsonObject actual = SearchFilterKey.VULNERABLE_METHOD.json("ignored").get();
    assertThat(actual.toString(), equalTo("{\"match\":{\"_vulnerableMethods\":true}}"));
  }

  @Test
  public void filterBySeverity() throws ParseException {
    JsonObject filter = SearchFilterKey.SEVERITY.json("8.0").get();
    assertThat(JsonUtils.get(filter, "range._severity.gte").toString(), equalTo("\"8.0\""));
  }

  @Test
  public void filterBySeverityRange() throws ParseException {
    JsonObject filter = SearchFilterKey.SEVERITY.json("1.0..5.0").get();

    assertThat(JsonUtils.get(filter, "range._severity.gte").toString(), equalTo("\"1.0\""));
    assertThat(JsonUtils.get(filter, "range._severity.lte").toString(), equalTo("\"5.0\""));
  }

  @Test(expected = QueryParseException.class)
  public void nonNumericSeverityThrows() {
    SearchFilterKey.SEVERITY.json("nine");
  }

  @Test
  public void testLicense() throws ParseException, IOException {
    JsonObject filterLicense = SearchFilterKey.LICENSE.json("BSD3").get();
    List<Map<?, ?>> must = JsonUtils.get(filterLicense, "nested.query.bool.must", List.class);
    assertEquals(1, must.size());

    assertThat(((Map<?, ?>)must.get(0).get(MATCH)).get(LICENSE_NAME_KEY), equalTo("BSD3"));
  }

  @Test
  public void testScoringFunctions() throws ParseException {
    Map<SearchWeightKey, Float> weights = Maps.newHashMap();
    weights.put(SearchWeightKey.TIME, 2.0f);
    weights.put(SearchWeightKey.SEVERITY, 3.0f);
    JsonArray functions = jsonRenderer.functions(weights);

    assertEquals(2, functions.size());
    JsonObject func = functions.getJsonObject(0);

    assertEquals(123456, ((JsonNumber) JsonUtils.get(func, String.format("%s.%s.%s",
        SCORING_TIME_FUNCTION, TIMESTAMP_KEY, SCORING_TIME_OFFSET))).intValue());
  }

  @Test
  public void testScoringFunctionsFullQuery() throws ParseException {
    float wr = 0.8f;
    float wt = 0.55f;
    float ws = 1.56f;

    SearchQuery query = SearchQuery.builder()
        .withTerm("sql injection")
        .withWeight(SearchWeightKey.RELEVANCE, wr)
        .withWeight(SearchWeightKey.TIME, wt)
        .withWeight(SearchWeightKey.SEVERITY, ws)
        .build();

    JsonObject json = jsonRenderer.render(query);

    // These are the scores related to `severity` and `recency`.
    // The mocked configuration does not set the ones relative to `popularity` and `enhanced`.
    JsonArray functions = (JsonArray) JsonUtils.get(json, "query.function_score.functions");
    assertThat(json.toString(), functions.size(), equalTo(2));

    // Time recency scoring.
    assertThat("Expecting a time function scoring element in this array: " + functions.toString(),
        JsonUtils.get(functions.getJsonObject(0), SCORING_TIME_FUNCTION), notNullValue());
    assertEquals(wt, ((JsonNumber) JsonUtils.get(functions.getJsonObject(0), "weight")).doubleValue(),
        DELTA);

    // Severity scoring.
    assertThat("Expecting a field factor for \"_severity\" in this array: " + functions.toString(),
        JsonUtils.get(functions.getJsonObject(1), FIELD_VALUE_FACTOR), notNullValue());
    assertEquals(ws, ((JsonNumber) JsonUtils.get(functions.getJsonObject(1),
        FIELD_VALUE_FACTOR + ".factor")).doubleValue(),
        DELTA);

    // Test that we added the relevance scoring.
    JsonNumber relevanceBoost = (JsonNumber) JsonUtils.get(json, "query.function_score.query.bool.boost");
    assertThat(relevanceBoost, Matchers.notNullValue());
    assertEquals(wr, relevanceBoost.doubleValue(), DELTA);

    assertThat("Missing the \"score_mode\" field (should be something like \"sum\"): " +
            JsonUtils.get(json, "query.function_score"),
        JsonUtils.get(json, "query.function_score.score_mode").toString(), equalTo("\"log_sqrt\""));

    assertThat("Missing the \"boost_mode\" field (should be something like \"sum\"): " +
            JsonUtils.get(json, "query.function_score"),
        JsonUtils.get(json, "query.function_score.boost_mode").toString(), equalTo("\"sum\""));
  }

  @Test
  public void testSeverityRanking() {
    Map<SearchWeightKey, Float> weights = Maps.newHashMap();
    weights.put(SearchWeightKey.SEVERITY, 3.0f);

    Optional<SearchConfiguration.FieldValueScoringParams> params = configuration.getScoring()
        .getFieldValueFactors()
        .stream()
        .filter(p -> Objects.equals(SEVERITY_KEY, p.getField()))
        .findFirst();
    assertTrue(params.isPresent());

    Optional<JsonObject> severityRanking = jsonRenderer.fieldValueFactor(params.get(), weights);

    assertTrue(severityRanking.isPresent());
    JsonObject score = severityRanking.get().getJsonObject(FIELD_VALUE_FACTOR);
    assertEquals(3.0, score.getJsonNumber("factor").doubleValue(), DELTA);
    assertEquals(SEVERITY_KEY, score.getString("field"));
  }

  @Test
  public void testHasSeverityRankingIfNoWeight() throws ParseException {
    Map<SearchWeightKey, Float> weights = Maps.newHashMap();

    Optional<SearchConfiguration.FieldValueScoringParams> params = configuration.getScoring()
        .getFieldValueFactors()
        .stream()
        .filter(p -> Objects.equals(SEVERITY_KEY, p.getField()))
        .findFirst();
    assertTrue(params.isPresent());

    Optional<JsonObject> severityRanking = jsonRenderer.fieldValueFactor(params.get(), weights);
    assertFalse(severityRanking.isPresent());
  }

  @Test
  public void testPublicView() throws ParseException {
    when(configuration.getExcludeFields())
        .thenReturn(Collections.singletonList("wrapper.artifactComponents.coordOne"));

    SearchQuery query = SearchQuery.builder()
        .withPublicView(true)
        .build();

    JsonObject json = jsonRenderer.render(query);

    // Ensure there's a must_not filter for all the non-released status
    JsonArray mustNotFilters = (JsonArray) JsonUtils.get(json, "query.function_score.query.bool"
        + ".must_not");
    assertThat(mustNotFilters, notNullValue());
    assertThat(json.toString(), mustNotFilters.size(), equalTo(4));

    //Ensure those 4 non-released status are there
    assertEquals(JsonUtils.get(mustNotFilters.getJsonObject(0), "match").toString(),
        "{\"model.stage\":\"REJECTED\"}");
    assertEquals(JsonUtils.get(mustNotFilters.getJsonObject(1), "match").toString(),
        "{\"model.stage\":\"REVIEW\"}");
    assertEquals(JsonUtils.get(mustNotFilters.getJsonObject(2), "match").toString(),
        "{\"model.stage\":\"DRAFT\"}");
    assertEquals(JsonUtils.get(mustNotFilters.getJsonObject(3), "match").toString(),
        "{\"model.stage\":\"QA\"}");
    // Ensure the exclude filters are there
    JsonArray excludeFilter = (JsonArray) JsonUtils.get(json, "_source.excludes");
    assertThat(excludeFilter.size(), greaterThan(0));
  }

  @Test
  public void patternMatch() {
    String id = "1234";
    Matcher m = ID_MATCH_REGEX_PATTERN.matcher(id);
    assertTrue(m.matches());
    assertEquals(2, m.groupCount());
    assertThat(m.group("type"), nullValue());
    assertEquals(1234, Integer.parseInt(m.group("id")));
    List<String> strings = Lists.newArrayList(
        "SID-53214",
        "LID-53214",
        "SID-53214",
        "LID-53214",
        "lid-53214",
        "sid-53214");

    strings.forEach(s -> assertTrue(s, ID_MATCH_REGEX_PATTERN.matcher(s).matches()));

    strings.forEach(s -> {
      Matcher m2 = ID_MATCH_REGEX_PATTERN.matcher(s);
      if (m2.matches()) {
        assertEquals(2, m2.groupCount());
        assertEquals(53214, Integer.parseInt(m2.group("id")));
        assertThat(m2.group("type"), isIn(new String[] {"SID", "LID", "sid", "lid"}));
      } else {
        fail("No match for " + s);
      }
    });
  }

  /**
   * RS-98  -- cve: filter for things like: cve:CVE-2011-9906 or cve:2007-2247
   */
  @Test
  public void cveFilterRenders() throws ParseException {
    SearchQuery cveQuery = SearchQuery.builder()
        .withFilter(SearchFilterKey.CVE, "CVE-2011-3345")
        .build();
    JsonObject json = jsonRenderer.render(cveQuery);

    JsonObject filters = ((JsonArray)JsonUtils.get(json,
        "query.function_score.query.bool.must")).getJsonObject(0);

    // This is the rendered JSON for the filters:
    // {"bool":{ "must": [
    //    {"match": {"model.cveYear":"2011"}}, {"match": {"model.cveDigits":"3345"}}
    // ]}}
    JsonArray cveMatch = ((JsonArray)JsonUtils.get(filters, "bool.must"));

    // Note the use of % to match the `model.cveYear` key.
    assertEquals("\"2011\"", JsonUtils.get(cveMatch.getJsonObject(0), "match.model%cveYear")
        .toString());

    assertEquals("\"3345\"", JsonUtils.get(cveMatch.getJsonObject(1), "match.model%cveDigits")
        .toString());
  }

  /**
   * RS-98  -- cve: filter for things like: cve:CVE-2011-9906 or cve:2007-2247
   * will throw a ParseException if nonsense is given (note the missing dash).
   */
  @Test(expected = QueryParseException.class)
  public void cveFilterFails() throws ParseException {
    SearchQuery cveQuery = SearchQuery.builder()
        .withFilter(SearchFilterKey.CVE, "CVE2011-3345")
        .build();
    jsonRenderer.render(cveQuery);
  }
}
