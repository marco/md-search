// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.test.util.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author marco
 */
public class SearchFilterKeyTest {

  @Test
  public void testParse() {
    String search = "apache commons type:library language:java license:apache";

    for (String token : search.split(" ")) {
      if (SearchFilterParser.isValid(token)) {
        Assert.assertIsNotEmpty(SearchFilterParser.of(token));
      }
    }
  }

  @Test
  public void testOneKey() {
    Assert.assertIsNotEmpty(SearchFilterParser.of("language:java"));
    assertThat(SearchFilterParser.of("type:library").get().getValue(), equalTo("library"));
    assertThat(SearchFilterParser.of("license:BSD").get().getValue(), equalTo("BSD"));
    assertThat(SearchFilterParser.of("license:apache").get().getKey(), equalTo(SearchFilterKey.LICENSE));
    assertThat(SearchFilterParser.of("language:ruby").get().getValue(), equalTo("ruby"));
    assertThat(SearchFilterParser.of("severity:7.5").get().getValue(), equalTo("7.5"));

    // See RS-88
    assertThat(SearchFilterParser.of("cve_year:2016").get().getValue(), equalTo("2016"));
    assertThat(SearchFilterParser.of("cve_id:7371").get().getValue(), equalTo("7371"));
  }

  @Test
  public void parseFailsGracefully() {
    Assert.assertIsEmpty(SearchFilterParser.of("language: java"));
    Assert.assertIsEmpty(SearchFilterParser.of("tap:library"));
    Assert.assertIsEmpty(SearchFilterParser.of("license:who cares, really?"));
    Assert.assertIsEmpty(SearchFilterParser.of("apache.commons.parse.Option"));
    Assert.assertIsEmpty(SearchFilterParser.of("languate:mispelling"));
    Assert.assertIsEmpty(SearchFilterParser.of("cveYear:2015"));
    Assert.assertIsEmpty(SearchFilterParser.of("cveID:9987"));
  }

  @Test
  public void testSeverityFilter() {
    assertEquals(SearchFilterKey.SEVERITY, SearchFilterKey.valueOf("severity".toUpperCase()));
    assertEquals(SearchFilterKey.SEVERITY, SearchFilterParser.of("severity:3.5")
        .orElseThrow(AssertionError::new).getKey());
    assertEquals("3.5", SearchFilterParser.of("severity:3.5")
        .orElseThrow(AssertionError::new).getValue());
  }

  @Test
  public void testCveFilter() {
    assertEquals(SearchFilterKey.CVE_YEAR, SearchFilterKey.valueOf("cve_year".toUpperCase()));
    assertEquals(SearchFilterKey.CVE_ID, SearchFilterParser.of("cve_id:35987").get().getKey());
    assertEquals("35987", SearchFilterParser.of("cve_id:35987").get().getValue());
  }

  /**
   * See RS-98.
   */
  @Test
  public void testSimpleCveFilter() {
    Assert.assertIsNotEmpty(SearchFilterParser.of("cve:2011-9987"));
    assertEquals("2011-9987", SearchFilterParser.of("cve:2011-9987")
        .orElseThrow(AssertionError::new)
        .getValue());

  }

  /**
   * See RS-97.
   */
  @Test
  public void testDisclosedFiter() {
    Assert.assertIsNotEmpty(SearchFilterParser.of("released:2011-03-31"));
    assertEquals("2011-06-30", SearchFilterParser.of("released:2011-06-30")
        .orElseThrow(AssertionError::new)
        .getValue());
  }

  @Test(expected = QueryParseException.class)
  public void testDisclosedMalformedFiter() {
    SearchFilterKey.RELEASED.json(">2011-06-30,2016-12-31");
  }

  @Test(expected = QueryParseException.class)
  public void testDisclosedMalformedFiter2() {
    SearchFilterKey.RELEASED.json(">2011-06");
  }

  @Test(expected = QueryParseException.class)
  public void testDisclosedMalformedFiter3() {
    SearchFilterKey.RELEASED.json("<2011-Feb-04");
  }
}
