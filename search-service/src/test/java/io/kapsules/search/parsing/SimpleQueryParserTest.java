// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import io.kapsules.search.data.ElasticsearchServiceTest;
import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.search.keys.SearchFilterKey;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.text.ParseException;

import static io.kapsules.search.api.data.Constants.ARTIFACT;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * A set of tests for our query parser.
 *
 * This class does NOT rely on Spring injection, it tests the {@link SimpleQueryParser} class as
 * a POJO without relying on any configuration and injection mechanism.
 *
 * See the {@link ElasticsearchServiceTest} test class for
 * tests that rely on injection and configuration for the correct parsing of queries.
 *
 * @author marco
 */
public class SimpleQueryParserTest {

  SimpleQueryParser parser = new SimpleQueryParser();

  @Before
  public void setup() {
    parser.setDefaultWeights(ElasticsearchServiceTest.createDefaultWeights());
  }

  @Test
  public void parser() throws Exception {
    SearchQuery query = parser.parse("java.lang enum wr:0.6 wv:0.4 license:apache");

    assertThat(query.getMatchTerms().trim(), equalTo("java.lang enum"));

    assertThat(query.getFilters().size(), equalTo(1));
    assertThat(query.getFilters().get(SearchFilterKey.LICENSE), equalTo("apache"));

    assertThat(query.getWeights().size(), equalTo(SearchWeightKey.values().length));
    assertThat(query.getWeights().get(SearchWeightKey.RELEVANCE), equalTo(0.6f));
    assertThat(query.getWeights().get(SearchWeightKey.VULN_COUNT), equalTo(0.4f));

    query = parser.parse("sql injection severity:7.5 type:artifact");
    assertEquals(Float.parseFloat(query.getFilters().get(SearchFilterKey.SEVERITY)), 7.5, 0.01);
    assertEquals(query.getFilters().get(SearchFilterKey.TYPE), ARTIFACT);

    query = parser.parse("windows sucks severity:0.99 wr:9.9 license:none");
    assertEquals(Float.parseFloat(query.getFilters().get(SearchFilterKey.SEVERITY)), 0.99, 0.001);
    assertThat(query.getFilters().get(SearchFilterKey.TYPE), nullValue());
    assertThat(query.getMatchTerms(), containsString("windows"));
    assertEquals(query.getWeights().get(SearchWeightKey.RELEVANCE), 9.9, 0.01);
  }

  @Test(expected = QueryParseException.class)
  public void parseWeightsFail1() throws ParseException {
    parser.shouldValidateWeights(true);
    parser.shouldRebalanceWeights(false);

    // The default value for the "balanced value" is 1.0, and will cause the parse to fail.
    assertEquals(1.0, ((SimpleQueryParser) parser).getBalancedWeightsSumTotal(), 0.01);
    parser.parse("java.lang enum wr:0.2 we:0.9 wt:0.5 license:apache");
  }

  @Test(expected = QueryParseException.class)
  public void parseWeightsFail2() throws ParseException {
    parser.shouldValidateWeights(true);
    parser.shouldRebalanceWeights(false);

    // ... as will this one.
    ((SimpleQueryParser) parser).setWeightsBalancedSum(99);
    parser.parse("java.lang enum wr:22 we:19 wt:5 license:apache");
  }

  @Test
  public void parseWeightsSucceeds() throws ParseException {

    // If we validate, but don't enable re-balancing, queries must match the desired weights total.
    parser.shouldValidateWeights(true);
    parser.shouldRebalanceWeights(false);

    // The default value for the "balanced value" is 1.0
    assertEquals(1.0, ((SimpleQueryParser) parser).getBalancedWeightsSumTotal(), 0.01);
    parser.parse("java.lang enum wr:0.2 we:0.3 wt:0.5 wp:0 ws:0 license:apache");

    // But it can be changed to whatever we want it to be.
    ((SimpleQueryParser) parser).setWeightsBalancedSum(99);
    parser.parse("java.lang enum wr:80 we:9 wt:10 wp:0 ws:0 license:apache");
  }

  @Test
  public void parseRebalances() throws ParseException {
    parser.shouldValidateWeights(true);
    parser.shouldRebalanceWeights(true);
    parser.setWeightsBalancedSum(1.0F);

    SearchQuery query = parser.parse("java.lang enum wr:2 we:9 wt:5 ");

    assertThat(query.getWeights().values(), not(empty()));

    assertThat(query.getWeights(), hasKey(SearchWeightKey.RELEVANCE));
    assertEquals(1.0f / 8.0f, query.getWeights().get(SearchWeightKey.RELEVANCE), 0.01);
    assertThat(query.getWeights(), hasKey(SearchWeightKey.ENHANCED));
    assertEquals(9.0f / 16.0f, query.getWeights().get(SearchWeightKey.ENHANCED), 0.01);
  }

  /**
   * SAF-87: Allow researcher to filter by stage:qa
   */
  @Test
  public void parsePublicView() throws ParseException {
    SearchQuery privateQuery = parser.parse("stage:qa", false);
    assertThat(privateQuery.getFilters().size(), equalTo(1));
    assertThat(privateQuery.getFilters().get(SearchFilterKey.STAGE), equalTo("qa"));


    SearchQuery publicQuery = parser.parse("stage:qa", true);
    assertThat(publicQuery.getFilters().size(), equalTo(0));
  }
}
