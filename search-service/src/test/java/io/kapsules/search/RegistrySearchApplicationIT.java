// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search;

import io.kapsules.search.config.ServicesConfiguration;
import io.kapsules.search.data.Repository;
import io.kapsules.search.data.SearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "functionaltests")
public class RegistrySearchApplicationIT {

  @Autowired
  private ServicesConfiguration servicesConfiguration;

  @Autowired
  private SearchService searchService;


  @Test
  public void contextLoads() {
    assertThat(servicesConfiguration, notNullValue());
    assertThat(servicesConfiguration.getElasticsearch(), notNullValue());
  }

  @Test(timeout = 1500L)
  public void canConnect() throws IOException {
    // TODO: re-implement tests
  }

  @Test
  public void canBuildQuery() {
    assertThat(searchService, notNullValue());
    assertThat(searchService.getQueryTemplate(),
        containsString("\"query\":{\"bool\":{\"should\":[{\"multi_match\":{\"query\":\"###\""));
  }
}
