// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search;

/**
 * ElasticSearch bulk operations, as well as searches, indexLibraries, etc., return a list of failures:
 * this enum is useful in tagging back the results to the API caller.
 *
 * @author marco
 */
public enum  ElasticSearchOutcome {
  SUCCESS,
  FAILURE,
  TIMEOUT;
}
