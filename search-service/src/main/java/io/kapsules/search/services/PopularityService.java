// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.services;


import java.util.Optional;

/**
 * Offers a simple interface to retrieve and update a library popularity, as defined by a
 * "popularity index" kept in the metadata of the model
 *
 * @author marco
 */
public interface PopularityService {

  /**
   * @param id the internal ID for the library
   * @return if found, the value associated with the
   * {@link io.kapsules.search.api.data.Constants#POPULARITY_KEY}
   */
  Optional<Float> popularity(Long id);

  void update(Long id, Float popularity);
}
