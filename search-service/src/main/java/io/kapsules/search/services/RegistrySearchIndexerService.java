// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.services;

import io.kapsules.search.ElasticSearchOutcome;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <h1>Indexer service for ElasticSearch</h1>
 *
 * <p>Acts as a bridge between the Documents API services and the ElasticSearch API, by
 * enabling the fetching of the data ("models") from the API and loading them into
 * ES for further indexing.</p>
 *
 * @author marco
 */
public interface RegistrySearchIndexerService {
  Map<ElasticSearchOutcome, Set<Long>> indexLibraries(List<Long> libraryIds);

  Map<ElasticSearchOutcome, Set<Long>> indexArtifacts(List<Long> artifactsIds);
}
