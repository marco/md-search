// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.services;

// CHECKSTYLE:OFF

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import io.kapsules.search.ElasticSearchOutcome;
import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.api.data.SearchServiceMessage;
import io.kapsules.search.config.RMQConfig;
import io.kapsules.search.config.SearchConfiguration;
import io.kapsules.search.data.LibraryIndexRedisRepository;
import io.kapsules.search.data.Repository;
import io.kapsules.search.data.VulnerabilityIndexRedisRepository;
import io.kapsules.search.keys.LibraryIndexEntity;
import io.kapsules.search.keys.VulnerabilityIndexEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.*;

import static io.kapsules.search.api.data.Constants.RABBIT_INDEX_DEBOUNCED_QUEUE;
import static io.kapsules.search.api.data.Constants.RABBIT_INDEX_QUEUE;

// CHECKSTYLE:ON


/**
 * Implementation of the {@link RegistrySearchIndexerService indexer service}.
 *
 * @author marco
 */
@Service
public class IndexerServiceImpl implements RegistrySearchIndexerService {

  private static final Logger LOG = LoggerFactory.getLogger(IndexerServiceImpl.class);

  private final Repository<ArtifactModelWrapper> artifactsIndex;

  private final long maxErrors;

  private final LibraryIndexRedisRepository libraryIndexRedisRepository;

  private final VulnerabilityIndexRedisRepository vulnerabilityIndexRedisRepository;

  @Autowired
  public IndexerServiceImpl(Repository<ArtifactModelWrapper> artifactsIndex,
                            SearchConfiguration searchConfiguration,
                            LibraryIndexRedisRepository libraryIndexRedisRepository,
                            VulnerabilityIndexRedisRepository vulnerabilityIndexRedisRepository

  ) {
    this.artifactsIndex = artifactsIndex;
    this.maxErrors = searchConfiguration.getMaxErrorsCount();
    this.libraryIndexRedisRepository = libraryIndexRedisRepository;
    this.vulnerabilityIndexRedisRepository = vulnerabilityIndexRedisRepository;
  }

  /**
   * Convenience method to build a reverse indexLibraries of {@literal outcome -> IDs}.
   *
   * @return a map having all the possible outcomes as keys, and empty lists as values, to which
   * IDs can be appended, depending on the API call outcome.
   */
  private Map<ElasticSearchOutcome, Set<Long>> makeResultsMap() {
    Map<ElasticSearchOutcome, Set<Long>> results =
        Maps.newEnumMap(ElasticSearchOutcome.class);

    EnumSet<ElasticSearchOutcome> outcomes = EnumSet.allOf(ElasticSearchOutcome.class);
    for (ElasticSearchOutcome outcome : outcomes) {
      results.put(outcome, Sets.newHashSet());
    }
    return results;
  }

  @Override
  public Map<ElasticSearchOutcome, Set<Long>> indexLibraries(List<Long> libraryIds) {
    return null;
  }

  /**
   * Uploads Artifacts to the ES service.
   *
   * @param artifactsIds a list of documents' IDs
   * @return a map with respectively the entries of successful IDs, and failures.
   */
  @Override
  public Map<ElasticSearchOutcome, Set<Long>> indexArtifacts(List<Long> artifactsIds) {
    Map<ElasticSearchOutcome, Set<Long>> results = makeResultsMap();

    long errors = 0;
    for (Long id : artifactsIds) {
      try {
        LOG.debug("Fetching artifact ID '{}'", id);

        // TODO: actually fetch the model to save.
        artifactsIndex.save(new ArtifactModelWrapper());
        results.get(ElasticSearchOutcome.SUCCESS).add(id);

      } catch (HttpClientErrorException e) {
        LOG.debug("Got an error fetching artifact ID '{}'. ({})", id,
            e.getMessage());
        results.get(ElasticSearchOutcome.FAILURE).add(id);
        errors++;
      }

      if (errors > maxErrors) {
        LOG.error("Too many errors ({}) terminating.", errors);
        break;
      }
    }
    return results;
  }

  /**
   * Consumer for indexing request messages.
   * <p>
   * <p>If {@literal content_type} is specified, and its value is {@literal "application/json"} then
   * the Jackson converter will be used and this method will be called.
   *
   * @param body the payload of the message
   * @see RMQConfig
   */
  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = RABBIT_INDEX_QUEUE, durable = "true"),
          exchange = @Exchange(value = ""), // bind to default exchange
          key = RABBIT_INDEX_QUEUE
      )
  )
  private void onMessage(SearchServiceMessage body) {
    final SearchServiceMessage.IndexType indexType = body.getIndexType();
    final long id = body.getId();

    if (indexType == null) {
      LOG.error("Malformed message from queue '{}': {}", RABBIT_INDEX_QUEUE, body);
      return;
    }

    LOG.debug("Received message to index {} [{}]", indexType, id);

    switch (indexType) {
      case ARTIFACT:
        vulnerabilityIndexRedisRepository.save(new VulnerabilityIndexEntity(id));
        break;
      case LIBRARY:
        libraryIndexRedisRepository.save(new LibraryIndexEntity(id));
        break;
      default:
        LOG.error("Invalid type requested for index: {}", indexType);
    }
  }


  /**
   * Consumer for indexing request messages.
   * <p>
   * If {@literal content_type} is specified, and its value is {@literal "application/json"} then
   * the Jackson converter will be used and this method will be called.
   *
   * @param message the payload of the message
   * @see RMQConfig
   */
  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = RABBIT_INDEX_DEBOUNCED_QUEUE, durable = "true"),
          exchange = @Exchange(value = ""), // bind to default exchange
          key = RABBIT_INDEX_DEBOUNCED_QUEUE
      )
  )
  private void processDebounced(SearchServiceMessage message) {
    final SearchServiceMessage.IndexType type = message.getIndexType();

    if (type == null) {
      LOG.error("Malformed message from queue = '{}', message = '{}'", RABBIT_INDEX_DEBOUNCED_QUEUE,
          message);
      return;
    }

    final List<Long> oneId = Collections.singletonList(message.getId());
    switch (type) {
      case ARTIFACT:
        indexArtifacts(oneId);
        break;
      case LIBRARY:
        indexLibraries(oneId);
        break;
      default:
        LOG.error("Unexpected type = '{}'", type);
    }
  }
}
