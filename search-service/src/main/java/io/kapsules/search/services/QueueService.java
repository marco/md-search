// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.services;

import io.kapsules.search.api.data.SearchServiceMessage;

import java.util.List;

/**
 *
 */
public interface QueueService {
  /**
   * Add message to the indexing queue
   * @param indexType - either library or artifact
   * @param ids - list of library or artifact ids to be added to queue for indexing
   */
  void addMessageToQueue(SearchServiceMessage.IndexType indexType, List<Long> ids);

}
