// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.services;

import io.kapsules.search.api.data.SearchServiceMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class QueueServiceImpl implements QueueService {

  private static final Logger LOGGER = LoggerFactory.getLogger(QueueServiceImpl.class);

  private RabbitOperations indexQueueSender;

  @Autowired
  public QueueServiceImpl(RabbitOperations indexQueueSender) {
    this.indexQueueSender = indexQueueSender;
  }

  @Override
  public void addMessageToQueue(SearchServiceMessage.IndexType indexType, List<Long> ids) {
    ids.forEach(id -> indexQueueSender.convertAndSend(new SearchServiceMessage(indexType, id)));
  }
}
