// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.kapsules.search.api.data.Constants;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Configuration properties for the all the search configuration options.
 *
 * @author marco
 */
@Component
@ConfigurationProperties(prefix = "search")
@Data
@ManagedResource
public class SearchConfiguration {

  @Data
  public static class FieldValueScoringParams {
    String field;
    Boolean enabled;
    String modifier;
    Float missing;
    String weightKey;
  }

  @Data
  public static class ScoringFunctionsParams {
    Map<String, Long> recent = new HashMap<>();
    List<FieldValueScoringParams> fieldValueFactors;
  }

  /**
   * We allow different values of default weights to be applied to searches based on the
   * {@link SearchFilterKey#TYPE `type`} filter applied.
   *
   * <p>This enum is used to avoid using error-prone hard-coded strings, or hard-to-refactor
   * string constants: use this every time you need to make a selection based on the type of the
   * query.
   */
  public enum WeightType {
    DEFAULT, LIBRARY, ARTIFACT;

    /**
     * Simple parser for the weight type.
     *
     * @param type the `type:` filter used in the query string, if any.  If empty or null, the
     *    {@link #DEFAULT} value is returned.
     * @return the corresponding weight type, or {@link #DEFAULT} if none chosen
     * @throws ParseException if `type` is non-empty, but does not correspond to one of the
     *    allowed filter types.
     */
    public static WeightType parseFromTypeFilter(@Nullable String type) throws ParseException {
      if (StringUtils.isBlank(type)) {
        return DEFAULT;
      }

      switch (type) {
        case Constants.LIBRARY:
          return LIBRARY;
        case Constants.ARTIFACT:
        case Constants.VULNERABILITY:
          return ARTIFACT;
        default:
          throw new ParseException("Filter type value should be one of ["
              + Constants.LIBRARY + ", "
              + Constants.ARTIFACT + ", "
              + Constants.VULNERABILITY + "]. Value '" + type + "' is not allowed.", 0);
      }
    }
  }

  private boolean validateWeights;
  private boolean rebalanceWeights;
  private long maxErrorsCount;
  private float weightsExpectedSum;

  private String boostMode;
  private String scoreMode;

  private Integer defaultResultsSize;

  private List<String> matchFields = Lists.newArrayList();
  private List<String> excludeFields = Lists.newArrayList();

  private Map<WeightType, Map<SearchWeightKey, Float>> weights = Maps.newHashMapWithExpectedSize(
      WeightType.values().length);

  private ScoringFunctionsParams scoring;


  @ManagedAttribute(defaultValue = "100", description = "The maximum number of errors during a "
      + "bulk operation against the API, before we terminate the operation")
  public void setMaxErrors(long maxErrorsCount) {
    this.maxErrorsCount = maxErrorsCount;
  }

  public long getMaxErrorsCount() {
    return maxErrorsCount;
  }

  @ManagedAttribute(defaultValue = "false", description = "Whether the weights specified in the "
      + "query string ought to be validated (that they sum to 1.0)")
  public void setValidateWeights(boolean validateWeights) {
    this.validateWeights = validateWeights;
  }

  @ManagedAttribute(defaultValue = "true", description = "Whether we should rebalance the weights"
      + " in the search query so that they add up to 1.0 (otherwise a QueryParseException is thrown"
      + " if they don't)")
  public void setRebalanceWeights(boolean rebalanceWeights) {
    this.rebalanceWeights = rebalanceWeights;
  }
}
