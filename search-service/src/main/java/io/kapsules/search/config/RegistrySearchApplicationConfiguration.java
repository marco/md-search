// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import io.kapsules.search.keys.SearchWeightKey;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * The main configuration class, where the application beans are created as necessary and the
 * various other configuration properties classes (e.g., the {@link SearchConfiguration}) are
 * injected, so that all the various beans can be wired together.
 *
 * <p>All the configuration options are currently externalized in the {@literal application.yaml}
 * file.
 *
 * @author marco
 */
@Configuration
public class RegistrySearchApplicationConfiguration {

  @Bean
  public boolean shouldValidateWeights(SearchConfiguration searchConfiguration) {
    return searchConfiguration.isValidateWeights();
  }

  @Bean
  public boolean shouldRebalanceWeights(SearchConfiguration searchConfiguration) {
    return searchConfiguration.isRebalanceWeights();
  }

  @Bean
  public Float weightsExpectedSum(SearchConfiguration searchConfiguration) {
    return searchConfiguration.getWeightsExpectedSum();
  }

  @Bean
  public Long maxErrors(SearchConfiguration searchConfiguration) {
    return searchConfiguration.getMaxErrorsCount();
  }

  @Bean
  public List<String> matchFields(SearchConfiguration searchConfiguration) {
    return searchConfiguration.getMatchFields();
  }


  @Bean
  public Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>> defaultWeights(
      SearchConfiguration searchConfiguration) {
    return searchConfiguration.getWeights();
  }

  @Bean
  public Integer defaultSize(SearchConfiguration searchConfiguration) {
    return searchConfiguration.getDefaultResultsSize();
  }
}
