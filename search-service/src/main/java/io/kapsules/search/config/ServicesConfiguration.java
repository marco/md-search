// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Configuration properties for the external services that we need to connect to: ElasticSearch,
 *
 * @author marco
 */
@Component
@ConfigurationProperties(prefix = "services")
@Data
@ManagedResource
public class ServicesConfiguration {

  public int defaultMaxConnectionsPerRoute() {
    return maxConnectionsPerRoute;
  }

  public int maxTotalConcurrentConnections() {
    return maxTotalConcurrentConnections;
  }

  public Integer defaultTimeout() {
    return defaultTimeout;
  }

  @Data
  public static class HostConfiguration {
    @NotNull
    private String hostname;
    private int port;
  }

  @Data
  public static class ApiServiceConfiguration {
    @NotEmpty
    private String url;

    // This can be empty, in which case only the URL will be used.
    private String endpoint;
  }

  @NotNull @Valid
  private HostConfiguration elasticsearch;

  @NotNull
  private Integer maxConnectionsPerRoute;

  @NotNull
  private Integer maxTotalConcurrentConnections;

  @NotNull
  private Integer defaultTimeout;
}
