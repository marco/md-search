// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.config;

import com.getsentry.raven.Raven;
import com.getsentry.raven.RavenFactory;
import com.getsentry.raven.config.Lookup;
import com.getsentry.raven.environment.RavenEnvironment;
import com.getsentry.raven.event.EventBuilder;
import com.getsentry.raven.event.interfaces.ExceptionInterface;
import com.getsentry.raven.event.interfaces.HttpInterface;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;

/**
 *
 */
public class SentryHandlerExceptionResolver implements HandlerExceptionResolver {
  private static final Logger LOGGER = LoggerFactory.getLogger(SentryHandlerExceptionResolver.class);

  /**
   * Used in the {@link #handlerMap} as a factory for how to handle specific exception classes. The
   * handlers are matched by exception type hierarchy and can be used for example to silence certain
   * exception classes (via {@link #NOOP} or log them instead of capturing via Raven.
   */
  @FunctionalInterface
  private interface Handler {
    void handle(Raven raven, HttpServletRequest request, Exception ex);
  }

  @SuppressWarnings("unused")
  private static final Handler NOOP = (raven, request, ex) -> { };

  private static LinkedHashMap<Class<? extends Exception>, Handler> getDefaultHandlerMap() {
    final LinkedHashMap<Class<? extends Exception>, Handler> map = new LinkedHashMap<>();
    return map;
  }

  private final Raven raven;

  /**
   * Map of exception classes to handlers. The Map is traversed to find the first key which is a matching
   * type or supertype of the given exception class. To clarify the ordered map semantics, this is
   * declared as a LinkedHashMap instead of a Map. If no matching type is found, the default handler is
   * invoked which calls {@link #captureException(Raven, HttpServletRequest, Exception)}
   */
  private final LinkedHashMap<Class<? extends Exception>, Handler> handlerMap;

  SentryHandlerExceptionResolver() {
    this(RavenFactory.ravenInstance(), getDefaultHandlerMap());
  }

  @VisibleForTesting
  SentryHandlerExceptionResolver(Raven raven,
                                 LinkedHashMap<Class<? extends Exception>, Handler> handlerMap) {
    this.raven = raven;
    this.handlerMap = handlerMap;
  }

  @Override
  public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
                                       Object handler, Exception ex) {

    // If we want to handle ignoring exception, uncomment this chunk of code below.
    /*
    Handler exceptionHandler = null;
    for (Map.Entry<Class<? extends Exception>, Handler> entry : handlerMap.entrySet()) {
      final Class<? extends Exception> checkClass = entry.getKey();
      if (checkClass.isAssignableFrom(ex.getClass())) {
        exceptionHandler = entry.getValue();
        break;
      }
    }

    if (exceptionHandler == null) { // in the fallback case capture the exception via Raven
      exceptionHandler = SentryHandlerExceptionResolver::captureException;
    }
    */

    Handler exceptionHandler = SentryHandlerExceptionResolver::captureException;
    exceptionHandler.handle(raven, request, ex);

    return null;  // by returning null, processing will continue with the other configured HandlerExceptionResolver's
  }

  private static void captureException(Raven raven, HttpServletRequest request, Exception ex) {
    // Do not log the event if the current thread is managed by raven
    if (RavenEnvironment.isManagingThread()) {
      return;
    }

    RavenEnvironment.startManagingThread();
    try {
      raven.sendEvent(new EventBuilder()
              .withRelease(Lookup.lookup("release"))
              .withExtra("extra-url", request.getRequestURI()) // just try it out
              .withSentryInterface(new HttpInterface(request))
              .withSentryInterface(new ExceptionInterface(ex))
      );
    } catch (Exception e) {
      LOGGER.error("Unable to use Raven.sendEvent(EventBuilder)", e);
    } finally {
      RavenEnvironment.stopManagingThread();
    }
  }
}
