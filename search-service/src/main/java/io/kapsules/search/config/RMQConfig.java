// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.config;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static io.kapsules.search.api.data.Constants.RABBIT_INDEX_DEBOUNCED_QUEUE;
import static io.kapsules.search.api.data.Constants.RABBIT_INDEX_QUEUE;


/**
 * RabbitMQ Consumer configuration.
 *
 * @author marco
 */
@Configuration
@EnableRabbit
public class RMQConfig {

  @Value("${spring.rabbitmq.shouldRequeue:false}")
  Boolean shouldRequeue;

  @Bean
  public SimpleRabbitListenerContainerFactory messageListenerContainer() {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    factory.setMessageConverter(messageConverter());
    factory.setDefaultRequeueRejected(shouldRequeue);
    return factory;
  }

  @Bean
  public Jackson2JsonMessageConverter messageConverter() {
    return new Jackson2JsonMessageConverter();
  }

  /**
   * Whenever an artifact is updated, use this message queue sender.
   */
  @Bean
  public RabbitOperations indexQueueSender(ConnectionFactory connectionFactory) {
    final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(messageConverter());
    rabbitTemplate.setRoutingKey(RABBIT_INDEX_QUEUE);
    return rabbitTemplate;
  }

  /**
   * whenever an artifact is updated, use this message queue sender
   */
  @Bean
  public RabbitOperations indexDebouncedQueueSender(ConnectionFactory connectionFactory) {
    final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(messageConverter());
    rabbitTemplate.setRoutingKey(RABBIT_INDEX_DEBOUNCED_QUEUE);
    return rabbitTemplate;
  }
}
