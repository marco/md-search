// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.keys;

import java.util.Arrays;

/**
 *
 * The general formula for a {@literal weighted ranking} is:
 *
 * <pre>
 *  r = Wr x Rr + Ws x Rs + Wt x Rt + We x Re
 * </pre>
 *
 * where each of the component ranking factors are:
 *
 * <pre>
 *    Rt: time rank (more recent entries first)
 *    Rr: text relevancy rank
 *    Rs: severity of the vuln
 *    Re: enhanced vulnerability writeup
 * </pre>
 *
 * See the full document <a href="https://goo.gl/rAHXWb">here</a>.
 *
 */
public enum SearchWeightKey {

  TIME("wt"),
  RELEVANCE("wr"),
  SEVERITY("ws"),
  ENHANCED("we"),
  POPULARITY("wp"),
  VULN_COUNT("wv"),
  EXACT_ID("id");

  private final String key;

  SearchWeightKey(String key) {
    this.key = key;
  }

  public String key() {
    return key;
  }

  public static SearchWeightKey of(String key) {
    return Arrays.stream(values()).filter(w -> w.key().equalsIgnoreCase(key))
        .findFirst()
        .orElseThrow(IllegalArgumentException::new);
  }
}
