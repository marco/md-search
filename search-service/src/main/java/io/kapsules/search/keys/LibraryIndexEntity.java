// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.keys;

import io.kapsules.search.api.data.Constants;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

import java.io.Serializable;

/**
 *
 */
@RedisHash(Constants.LIBRARY_KEYSPACE)
public class LibraryIndexEntity implements Serializable {

  @Id
  private Long id;

  @TimeToLive
  private long secondsToExpireIn = Constants.REDIS_INDEX_EXPIRES_IN_SEC;


  public LibraryIndexEntity() {
  }

  public LibraryIndexEntity(Long id) {
    this.id = id;
  }


  @Override
  public String toString() {
    return "LibraryIndexEntity{id = " + id + ", secondsToExpireIn = " + secondsToExpireIn + '}';
  }


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public long getSecondsToExpireIn() {
    return secondsToExpireIn;
  }

  public void setSecondsToExpireIn(long secondsToExpireIn) {
    this.secondsToExpireIn = secondsToExpireIn;
  }
}
