// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.keys;

import io.kapsules.search.api.data.Constants;
import io.kapsules.search.api.data.PubliclyAvailable;
import io.kapsules.search.api.data.Stage;
import io.kapsules.search.parsing.QueryParseException;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.kapsules.search.api.data.Constants.*;
import static io.kapsules.search.keys.SearchFilterKey.NoOp.NOOP;

/**
 * Encapsulates the various filters that can be used in a search query, e.g., {@literal
 * language:java}.
 *
 * @author marco
 */
public enum SearchFilterKey implements PubliclyAvailable {

  /**
   * A {@literal type:library} or {@literal type:vulnerability} will not be used in the JSON
   * query, but will modify the URL endpoint to limit the search to the given document type.
   */
  TYPE(true, NOOP),

  /**
   * Libraries and Artifacts encode the type of language differently, we must cater for both when
   * building the filter in the query.
   */
  LANGUAGE(true, language -> {
    //INT-197: Transform javascript to JS since that's what stored in the database
    language = language.equals("javascript") ? "js" : language;

    JsonBuilderFactory factory = Json.createBuilderFactory(Collections.emptyMap());

    JsonObjectBuilder modelLanguage = factory.createObjectBuilder()
        .add(MATCH, factory.createObjectBuilder()
            .add(LANGUAGE_KEY, language));
    JsonObjectBuilder modelLanguageType = factory.createObjectBuilder()
        .add(MATCH, factory.createObjectBuilder()
            .add(Constants.LANGUAGE_TYPE_KEY, language));

    return factory.createObjectBuilder()
        .add(BOOL, factory.createObjectBuilder()
            .add(SHOULD, factory.createArrayBuilder()
                .add(modelLanguage)
                .add(modelLanguageType))).build();
  }),

  /*
    IMPLEMENTATION NOTE

    This filter needs to search in a `nested` type field (`versions`) thus requiring a
    bit of JSON-gymnastics; this is what the search query ought to look like:

      "must":[
        { "bool": {
            "should":[
                {"match":{"model.language":"java"} },
                {"match":{"model.languageType":"java"} }
            ]
          }
        },
        <<< This is the part we're implementing below >>>
        { "nested": {
            "path": "model.versions",
            "query": {
              "bool": {
                "must": [
                  {"match": {
                     "model.versions.licenseInfoModels.name": "APACHE20"
                  } }
                ]
              }
            }
        }
        <<< ends here >>>
      ]
   */
  LICENSE(true, license -> Json.createObjectBuilder()
      .add("nested", Json.createObjectBuilder()
          .add("path", VERSIONS_KEY)
          .add(QUERY, Json.createObjectBuilder()
              .add(BOOL, Json.createObjectBuilder()
                  .add(MUST, Json.createArrayBuilder()
                      .add(Json.createObjectBuilder()
                        .add(MATCH, Json.createObjectBuilder()
                          .add(Constants.LICENSE_NAME_KEY, license))
                      )
                  )
              )
          )
      ).build()),

  /**
   * Filters on where the library was sourced from (e.g., 'source:BOWER'); the value is the name of
   * the "source" (e.g., MAVEN or BOWER).
   */
  SOURCE(true, value -> Json.createObjectBuilder()
      .add(MATCH, Json.createObjectBuilder()
          .add(Constants.SOURCE_NAME_KEY, value)).build()),

  /**
   * Executes a "range search" for the {@literal severity} field, adding a term of the form:
   *
   * <pre>
   *   {"range": {"_severity" : { "gte" : 7.5, "lte": 9} } }
   * </pre>
   *
   * <p>The filter is of the form {@literal severity:>5} or {@literal
   * severity:2,5}; the operand in the first format may be omitted, in which
   * case we will assume a value of ">=", i.e., all vulns with severity greater or equal to the value.
   */
  SEVERITY(true, value -> renderRangeQuery(SEVERITY_PATTERN, SEVERITY_KEY, value, rangeItem -> {
    try {
      Float item =  Float.parseFloat(rangeItem);
      return String.valueOf(item);
    } catch (NumberFormatException e) {
      throw new QueryParseException("Cannot parse " + rangeItem + "into a valid float");
    }
  })),


  /**
   * Filters for those vulnerabilities where the {@literal vulnerableMethods} fields is non-empty.
   * the value is ignored (this is a simple "flag" filter).
   */
  VULNERABLE_METHOD(true, ignored -> Json.createObjectBuilder()
      .add(MATCH, Json.createObjectBuilder()
          .add(VULNERABLE_METHOD_KEY, true))
      .build()),

  /**
   * Filter on whether the artifact as a non-empty {@literal "teardown"} field.
   * The value is ignored
   */
  ENHANCED(true, ignored -> Json.createObjectBuilder()
      .add(RANGE_QUERY, Json.createObjectBuilder()
          .add(Constants.ENHANCED_KEY, Json.createObjectBuilder()
              .add(GREATER_THAN_OP, MIN_TEARDOWN_LEN))
      ).build()),

  /**
   * Filter on whether the library has at least one vulnerability count.
   */
  VULNERABLE(true, ignored -> Json.createObjectBuilder()
      .add(RANGE_QUERY, Json.createObjectBuilder()
          .add(VULN_COUNT_KEY, Json.createObjectBuilder()
              .add(GREATER_THAN_OP, 0))
      ).build()),

  /**
   * Filter on the {@literal year} part of the ``CVE-year-ID`` string.
   * Note that, although the year is a numeral, it is stored in the JSON (and the object model) as
   * a string.
   *
   * <p><strong>We do not validate that the `year` is a valid numeric value (let alone a sensible
   * year to discover a CVE).</strong>
   */
  CVE_YEAR(true, year -> Json.createObjectBuilder()
      .add(MATCH, Json.createObjectBuilder()
          .add(CVE_YEAR_KEY, year)).build()),

  /**
   * Filter on the {@literal ID} part of the ``CVE-year-ID`` string.
   * Note that, although the ID is a numeral, it is stored in the JSON (and the object model) as
   * a string.
   *
   * <p><strong>We do not validate that the ID is a valid numeric value.</strong>
   */
  CVE_ID(true, id -> Json.createObjectBuilder()
          .add(MATCH, Json.createObjectBuilder()
              .add(CVE_ID_KEY, id)).build()),

  /**
   * Filter for a {@literal CVE} match, of the type {@literal CVE-2016-0432}.
   *
   * Will generate a JSON clause similar to:
   * <pre>
   *   {
   *      "bool": {
   *        "must": [
   *          {"match": {"model.cveYear":"2011"} },
   *          {"match": {"model.cveDigits":"0432"} }
   *        ]
   *      }
   *    }
   * </pre>
   */
  CVE(true, (String clause) -> {
      Matcher cveMatch = CVE_PATTERN.matcher(clause);
    if (cveMatch.matches()) {
      //noinspection OptionalGetWithoutIsPresent
      JsonObject cveYearObj = CVE_YEAR.json(cveMatch.group("year")).get();
      //noinspection OptionalGetWithoutIsPresent
      JsonObject cveIdObj = CVE_ID.json(cveMatch.group("id")).get();

      return Json.createObjectBuilder()
          .add(BOOL, Json.createObjectBuilder()
              .add(MUST, Json.createArrayBuilder()
                  .add(cveYearObj)
                  .add(cveIdObj))
          )
          .build();
    }
    throw new QueryParseException("Cannot parse a cve: filter into a valid CVE-year-id pattern: "
        + clause);
  }),

  /**
   * Filter on the stage for the artifact (e.g., RELEASED, QA etc...).
   *
   * <p>This filter <strong>is not</strong> publicly available; reserved for internal use and
   * our research team.
   */
  STAGE(false, value -> {
    Stage stage = Stage.valueOf(value.toUpperCase());
    return Json.createObjectBuilder()
        .add(MATCH, Json.createObjectBuilder()
            .add(STAGE_NAME_KEY, stage.name())).build();
  }),

  /**
   * This filters artifacts (vulnerabilities) by their "disclosure date" (which is usually
   * different by their "released date:" see {@link #RELEASED}).
   */
  DISCLOSED(true, value -> renderRangeQuery(RELEASED_PATTERN, DISCLOSED_KEY, value, rangeItem -> {
    try {
      Date item = RELEASED_DATE_FORMAT.get().parse(rangeItem);
      return String.valueOf(item.getTime());

    } catch (ParseException e) {
      throw new QueryParseException("Cannot parse " + rangeItem + "into a valid date: format should be YYYY-MM-DD");
    }
  })),

  /**
   * Filters documents (libraries and artifacts alike) by their released date, using a (possibly,
   * open-ended) date range.
   *
   * <p>The filter is of the form {@literal released:>2015-01-04} or {@literal
   * released:2014-03-31,2014-09-30}; the operand in the first format may be omitted, in which
   * case we will assume a value of ">", i.e., all vulns disclosed after the date.
   */
  RELEASED(true, value -> renderRangeQuery(RELEASED_PATTERN, TIMESTAMP_KEY, value, rangeItem -> {
    try {
      Date item = RELEASED_DATE_FORMAT.get().parse(rangeItem);
      return String.valueOf(item.getTime());

    } catch (ParseException e) {
      throw new QueryParseException("Cannot parse " + rangeItem + "into a valid date: format should be YYYY-MM-DD");
    }
  }));

  static class NoOp {
    static final Function<String, JsonObject> NOOP = s -> null;
  }

  private final boolean publiclyAvailable;
  private final Function<String, JsonObject> render;

  SearchFilterKey(boolean publiclyAvailable, Function<String, JsonObject> render) {
    this.publiclyAvailable = publiclyAvailable;
    this.render = render;
  }

  @Override
  public boolean isPubliclyAvailable() {
    return publiclyAvailable;
  }

  public Optional<JsonObject> json(String value) {
    return Optional.ofNullable(render.apply(value));
  }

  private static JsonObject renderRangeQuery(Pattern pattern,
                                             String searchKey,
                                             String query,
                                             Function<String, String> parser) {
    Matcher m = pattern.matcher(query);
    if (!m.matches()) {
      throw new QueryParseException("Invalid query " + query + " for " + pattern);
    }

    String from = m.group("from");
    String to = m.group("to");
    String op = m.group("op");

    if (to != null && op != null) {
      throw new QueryParseException("Cannot specify both an operand and a range");
    }

    String since = parser.apply(from);
    String jsonOp = (op == null || ">".equals(op)) ? GREATER_THAN_OR_EQUAL_OP : LESSER_THAN_OR_EQUAL_OP;

    JsonObjectBuilder builder = Json.createObjectBuilder();
    builder.add(jsonOp, since);

    if (to != null) {
      String until = parser.apply(to);
      builder.add(LESSER_THAN_OR_EQUAL_OP, until);
    }

    return Json.createObjectBuilder()
            .add(RANGE_QUERY, Json.createObjectBuilder()
                    .add(searchKey, builder)
            ).build();
  }
}
