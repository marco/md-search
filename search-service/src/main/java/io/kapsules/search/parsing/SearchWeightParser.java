// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import io.kapsules.search.keys.SearchWeightKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;


public class SearchWeightParser extends ParseableToken<SearchWeightKey, Float> {

  private static final Logger LOG = LoggerFactory.getLogger(SearchWeightParser.class);
  private static final Pattern WEIGHT_REGEX = Pattern.compile("^(\\w+):(\\d+\\.?\\d*)$");

  /**
   * Singleton pattern to provide access to the instance's attributes, via static methods.
   */
  private static final SearchWeightParser PARSER = new SearchWeightParser();

  /**
   * This parser should never be instantiated; use the static {@link #of(String)} and
   * {@link #isValid(String)} methods instead.
   */
  private SearchWeightParser() { }

  @Override
  public SearchWeightKey parseKeyFromString(String name) {
    return SearchWeightKey.of(name);
  }

  @Override
  public Float parseValueFromString(String value) {
    return Float.valueOf(value);
  }

  @Override
  public Pattern getPattern() {
    return WEIGHT_REGEX;
  }

  /**
   * Parses the given token and returns a Pair of {@literal key, value}.
   *
   * @param token the `wr:0.88` type of string token to parse
   * @return the key,value pair
   */
  public static Optional<Map.Entry<SearchWeightKey, Float>> of(String token) {
    // We cannot make the parse() method static, so we simply use a static instance to use the
    // instance method.
    return PARSER.parse(token);
  }

  public static boolean isValid(String token) {
    return PARSER.isValidToken(token);
  }
}
