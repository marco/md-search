// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import io.kapsules.search.keys.SearchFilterKey;

import java.util.Map;
import java.util.Optional;

/**
 * These are search "modifiers" that will limit search results, via filtering; they can be used
 * by appending something like this in the search query:
 *
 * <pre>
 *   type:library language:java license:apache
 * </pre>
 *
 * In practice, they will be added to the ElasticSearch query via the use of {@literal filter}
 * clauses.
 *
 * @author marco
 */
public class SearchFilterParser extends ParseableToken<SearchFilterKey, String> {

  // We don't need a new PARSER every time; we'll just reuse the Singleton every time.
  private static final SearchFilterParser PARSER = new SearchFilterParser();

  /**
   * This should never be instantiated; use instead the static methods to parse a
   * {@link SearchFilterKey} or validate a {@literal token}.
   */
  private SearchFilterParser() { }

  @Override
  public SearchFilterKey parseKeyFromString(String s) {
    return SearchFilterKey.valueOf(s.toUpperCase());
  }

  @Override
  public String parseValueFromString(String s) {
    return s;
  }


  public static Optional<Map.Entry<SearchFilterKey, String>> of(String token) {
    return PARSER.parse(token);
  }

  public static boolean isValid(String token) {
    return PARSER.isValidToken(token);
  }
}
