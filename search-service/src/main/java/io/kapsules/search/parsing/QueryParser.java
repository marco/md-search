// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import java.text.ParseException;

/**
 * Implementation of this class will convert a query string into an "intermediate representation"
 * of a {@link SearchQuery query object} prior to it being converted into an actionable search
 * API request to Elasticsearch.
 *
 * @author marco
 */
public interface QueryParser {

  /**
   * Given a query string, expressed in a grammar that the parse implementation can understand,
   * this will convert it into an intermediate {@link SearchQuery} representation.
   *
   * @param search the search to be executed
   * @return the representation of that search as an actionable query
   * @throws ParseException if the query string cannot be parsed into a valid query object
   */
  default SearchQuery parse(String search) throws ParseException {
    return parse(search, true);
  }

  /**
   * Overload the above parse method. Provide the capability to search with private filter key
   * such as stage.
   *
   * @param publicView whether the search is only meant for public view. if this is true,
   *    private filter key should not be enabled
   */
  SearchQuery parse(String search, boolean publicView) throws ParseException;

  /**
   * Defines whether the parser should ensure that the weights are valid (for some definition of
   * "valid"); typically, that their sum adds up to 1.0.
   *
   * @param shouldValidateWeights if set, the parser will validate the weights according to the
   *    parser implementation.
   */
  void shouldValidateWeights(boolean shouldValidateWeights);

  /**
   * When the parser is configured to validate the weights, and assuming that one of the
   * criterion is that they all add up to a given value (typically, 1.0 or 100), then if this
   * flag is set, instead of throwing an error, the weights' values will be scaled so that their
   * sum meets the parser implementation's criteria.
   *
   * @param shouldRebalanceWeights if set, it will scale the weights accordingly (but only if
   *    validation is enabled).
   * @see #shouldValidateWeights(boolean)
   */
  void shouldRebalanceWeights(boolean shouldRebalanceWeights);

  /**
   * If the weights need to "balance" this is the value that their sum will be checked against.
   *
   * <p>This is only used if {@link #shouldValidateWeights(boolean)} is set to `true`, but
   * {@link #shouldRebalanceWeights(boolean)} is set to `false`: in this case, if the sum total
   * of the weights' values does not exactly match this value, an exception is thrown.
   *
   * @param sumTotal the expected total of the sum of the weights' values
   */
  void setWeightsBalancedSum(float sumTotal);
}
