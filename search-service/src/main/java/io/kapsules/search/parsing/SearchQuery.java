// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import com.google.common.collect.Maps;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;

/**
 * A search query is expressed as a string that contains a list of search terms,
 * {@link SearchFilterKey search filters} and {@link SearchWeightParser weight terms}, such as:
 *
 * <pre>
 *   apache commons language:java type:library wr:0.4 we:0.1
 * </pre>
 *
 * <p>This class encapsulates the query, parsing the string into its components and then emitting it
 * as a "query JSON" that is compatible with ElasticSearch search DSL.
 *
 * @author marco
 */
@Getter
public class SearchQuery {

  public static final Integer DEFAULT_SIZE = 50;

  private String matchTerms;
  private Map<SearchWeightKey, Float> weights = Maps.newEnumMap(SearchWeightKey.class);
  private Map<SearchFilterKey, String> filters = Maps.newEnumMap(SearchFilterKey.class);

  private Integer offset;
  private Integer size;

  private boolean publicView;


  public static class QueryBuilder {
    private SearchQuery query = new SearchQuery();

    private StringBuilder termsBuilder = new StringBuilder();

    public SearchQuery build() {
      query.matchTerms = termsBuilder.toString();

      // To avoid NPEs and other potentially subtle bugs, any weight that has not been assigned
      // while building this query, will be assigned a default value of 0.0.
      Arrays.stream(SearchWeightKey.values())
          .filter(w -> !query.weights.containsKey(w))
          .forEach(w -> withWeight(w, 0.0F));

      return query;
    }

    /**
     * Sometimes it may be necessary to get access to a partially-built query, in order to
     * progress building it, or for testing purposes; use this method to gain access to the
     * internal object.
     *
     * <p><strong>DO NOT MODIFY</strong> the returned query; and it is also extremely
     * inadvisable to use it to execute searches: results may be completely misleading.
     *
     * @return a partially-built query that may be necessary for intermediate processing.
     */
    public SearchQuery partialQuery() {
      return query;
    }

    public QueryBuilder withTerm(String term) {
      if (termsBuilder.length() > 0) {
        termsBuilder.append(' ');
      }
      termsBuilder.append(term);
      return this;
    }

    public QueryBuilder withFilter(SearchFilterKey filter, String value) {
      query.filters.put(filter, value);
      return this;
    }

    public QueryBuilder withWeight(SearchWeightKey weight, Float value) {
      query.weights.put(weight, value);
      return this;
    }

    public QueryBuilder withWeights(Map<SearchWeightKey, Float> weights) {
      query.weights.putAll(weights);
      return this;
    }

    public QueryBuilder withFilters(Map<SearchFilterKey, String> filters) {
      query.filters.putAll(filters);
      return this;
    }

    public QueryBuilder withOffset(Integer offset) {
      query.offset = offset;
      return this;
    }

    public QueryBuilder atMost(Integer size) {
      query.size = size;
      return this;
    }

    public QueryBuilder withPublicView(boolean publicView) {
      query.publicView = publicView;
      return this;
    }
  }

  private SearchQuery() {
    this.offset = 0;
    this.size = DEFAULT_SIZE;
  }

  /**
   * This class uses the Builder Pattern so that a query parse can build it while parsing the
   * tokens that make up the query string.
   *
   * @return a builder for the query
   */
  public static QueryBuilder builder() {
    return new QueryBuilder();
  }

  // Although this is mostly a Value Class (with a Builder Pattern) we still need to allow
  // modifications to some of its elements, as they may not be necessarily known at build time.

  /**
   * @param offset How many items to skip from the beginning of the search.
   */
  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  /**
   * @param size how many items at most to return.
   */
  public void setSize(Integer size) {
    this.size = size;
  }

  /**
   * Certain queries may return results that are only available to internal and/or premium users:
   * if the query is "public" then those elements will be removed from the search results.
   *
   * @param publicView whether this query is initiated by an internal user (if {@literal false})
   */
  public void setPublicView(boolean publicView) {
    this.publicView = publicView;
  }
}
