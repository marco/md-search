// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

/**
 * A "Renderer" takes our internal representation of a {@link SearchQuery} and renders it into an
 * object that can be eventually served to an external consumer.
 *
 * <p>The most obvious example is a JSON Renderer, which converts a search query into a form
 * suitable for submission to Elasticsearch via the REST API.</p>
 *
 * @param <T> the type of the rendered object
 *
 * @author marco
 */
public interface SearchQueryRenderer<T> {

  T render(SearchQuery query);
}
