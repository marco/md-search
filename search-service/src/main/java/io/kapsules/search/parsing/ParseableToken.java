// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstracts the parts of the query that can be represented as filters or weights, expressed as a
 * colon-separated {@literal {key, value} pair}.
 *
 * <p>Implementors may define a valid RegEx pattern if the default {@link #REGEX}
 * is not suitable: this pattern <strong>SHOULD</strong> define (at least) two capturing groups
 * that will be (respectively) parsed into a {@literal key} and a {@literal value}, using the
 * implementation-defined methods {@link #parseKeyFromString(String)} and
 * {@link #parseValueFromString(String)}.
 *
 * @see SearchWeightParser
 * @see SearchFilterParser
 *
 * @author marco
 */
public abstract class ParseableToken<K, V> {

  protected static final Logger LOG = LoggerFactory.getLogger(ParseableToken.class);

  /**
   * Default matching pattern, essentially a {@literal key:value} token.
   */
  protected static final Pattern REGEX = Pattern.compile("^(\\w+):(\\S+)$");

  public Pattern getPattern() {
    return REGEX;
  }

  protected abstract K parseKeyFromString(String s);
  protected abstract V parseValueFromString(String s);

  public Optional<Map.Entry<K, V>> parse(String token) {
    Matcher matcher = getPattern().matcher(token);

    if (matcher.matches() && matcher.groupCount() == 2) {
      String key = matcher.group(1);
      String value = matcher.group(2);

      try {
        return Optional.of(new HashMap.SimpleImmutableEntry<>(
            parseKeyFromString(key),
            parseValueFromString(value)));
      } catch (IllegalArgumentException ex) {
        // This is safe to ignore: it may just be the caller giving it a shot to see if it
        // parsed; if it was unexpected by the caller, the empty() value returned will flag and
        // it will know how to handle that.
        // TODO: remove the debug log once the code is stable.
        LOG.debug("Could not parse token '{}'; cause: {}", token, ex.getMessage());
      }
    }
    return Optional.empty();
  }

  public boolean isValidToken(String token) {
    return getPattern().matcher(token).matches();
  }
}
