// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

/**
 * Used to signal a query string that is not valid.
 */
public class QueryParseException extends IllegalArgumentException {
  public QueryParseException(String message) {
    super(message);
  }

  public QueryParseException(String message, Throwable cause) {
    super(message, cause);
  }

  public QueryParseException(Throwable cause) {
    super(cause);
  }
}
