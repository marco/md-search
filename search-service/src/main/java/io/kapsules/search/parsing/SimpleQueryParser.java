// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import com.google.common.collect.Maps;
import io.kapsules.search.api.data.Constants;
import io.kapsules.search.config.SearchConfiguration;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.util.OptionalFunc;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Map;

/**
 * {@inheritDoc}
 *
 * @author marco
 */
@Component
public class SimpleQueryParser implements QueryParser {

  private boolean shouldValidateWeights;
  private boolean shouldRebalanceWeights;
  private float balancedWeightsSumTotal;

  private Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>> defaultWeights;

  /**
   * Creates a parser, specifying explicitly whether weights in the query ought to be validated
   * and, failing to do so, whether they ought to be "rebalanced" so that their sum adds up to 1.0.
   *
   * <p>Alternatively, if they are not supposed to automatically rebalance, but are expected to
   * validate and match a predefined sum total whose value is different from {@literal 1.0f}, set
   * {@literal balancedWeightsSumTotal} to the desired value.
   *
   *
   * @param shouldValidateWeights whether weights in the query should be validated
   * @param shouldRebalanceWeights whether, failing the validation, they should be rebalanced so
   * @param balancedWeightsSumTotal the expected sum for all the weights (if they are expected to
   *    "validate", but not "rebalance" automatically; in other words, `shouldRebalanceWeights`
   *    is {@literal false}, but `shouldValidateWeights` is set to {@literal true}.  By default,
   *    set to 1.0 if the given value is not greater than 0
   */
  @SuppressWarnings("SpringJavaAutowiringInspection")
  @Autowired
  public SimpleQueryParser(boolean shouldValidateWeights, boolean shouldRebalanceWeights, float
      balancedWeightsSumTotal) {
    this.shouldValidateWeights = shouldValidateWeights;
    this.shouldRebalanceWeights = shouldRebalanceWeights;
    this.balancedWeightsSumTotal = balancedWeightsSumTotal > 0 ? balancedWeightsSumTotal : 1.0f;
    this.defaultWeights = Maps.newEnumMap(SearchConfiguration.WeightType.class);
  }

  /**
   * Default constructor, <strong>does not validate</strong> by default.
   */
  public SimpleQueryParser() {
    this(false, false, 1.0f);
  }

  @Autowired
  @Qualifier("defaultWeights")
  public void setDefaultWeights(
      Map<SearchConfiguration.WeightType, Map<SearchWeightKey, Float>> weights) {
    this.defaultWeights = weights;
  }


  /**
   * {@inheritDoc}
   * @param shouldValidateWeights if set, the parser will validate the weights according to the
   */
  @Override
  public void shouldValidateWeights(boolean shouldValidateWeights) {
    this.shouldValidateWeights = shouldValidateWeights;
  }

  /**
   * {@inheritDoc}
   * @param shouldRebalanceWeights if set, it will scale the weights accordingly (but only if
   *    validation is enabled).
   */
  @Override
  public void shouldRebalanceWeights(boolean shouldRebalanceWeights) {
    this.shouldRebalanceWeights = shouldRebalanceWeights;
  }

  @Override
  public void setWeightsBalancedSum(float sumTotal) {
    this.balancedWeightsSumTotal = sumTotal;
  }

  public float getBalancedWeightsSumTotal() {
    return balancedWeightsSumTotal;
  }


  private void validateWeights(SearchQuery query) throws QueryParseException {
    Map<SearchWeightKey, Float> weights = query.getWeights();
    double weightsSum = weights
        .values()
        .stream()
        .mapToDouble(x -> x)  // You gotta love Java... unboxing for the masses.
        .sum();

    // We enforce matching within a 0.1% rounding margin.
    if (Math.abs(weightsSum - balancedWeightsSumTotal) > Constants.TOLERANCE) {
      if (shouldRebalanceWeights) {
        weights.forEach((k, v) -> weights.put(k, (float) (v / weightsSum)));
      } else {
        throw new QueryParseException(String.format("Sum  total of Weights should be equal to "
            + "[%s], but was [%s]. Set `rebalanceWeights` to `true` to automatically re-calibrate "
            + "them to correctly balance.", balancedWeightsSumTotal, weightsSum));
      }
    }
  }

  @Override
  public SearchQuery parse(String search, boolean publicView) throws ParseException {
    SearchQuery.QueryBuilder builder = SearchQuery.builder();

    for (String token : search.split("\\s+")) {
      if (StringUtils.isBlank(token)) {
        continue;
      }
      OptionalFunc.of(SearchFilterParser.of(token))
          .ifPresentOrElse(
              f -> {
                if (f.getKey().isPubliclyAvailable() || !publicView) {
                  builder.withFilter(f.getKey(), f.getValue());
                } else {
                  builder.withTerm(token);
                }
              },
              () -> OptionalFunc.of(SearchWeightParser.of(token))
                  .ifPresentOrElse(
                      w -> builder.withWeight(w.getKey(), w.getValue()),
                      () -> builder.withTerm(token)
                  ));
    }

    // We now have potentially some weights from the query (or none) and we need to merge those
    // with the default weights that have been configured in application.yaml.
    builder.withWeights(mergeQueryDefaultWeights(builder.partialQuery()));

    SearchQuery query = builder.build();
    if (shouldValidateWeights) {
      validateWeights(query);
    }
    return query;
  }

  private Map<SearchWeightKey, Float> mergeQueryDefaultWeights(SearchQuery query) throws
      ParseException {

    SearchConfiguration.WeightType weightType = SearchConfiguration.WeightType.parseFromTypeFilter(
        query.getFilters().get(SearchFilterKey.TYPE));

    Map<SearchWeightKey, Float> mergedWeights =
        Maps.newHashMap(defaultWeights.get(weightType));

    // Obviously, the user-defined weights in the query take priority.
    query.getWeights().forEach(mergedWeights::put);

    return mergedWeights;
  }
}
