// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.parsing;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import io.kapsules.search.api.data.Stage;
import io.kapsules.search.config.SearchConfiguration;
import io.kapsules.search.keys.SearchFilterKey;
import io.kapsules.search.keys.SearchWeightKey;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.json.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static io.kapsules.search.api.data.Constants.*;

/**
 * <h1>Elasticsearch JSON Query Renderer</h1>
 *
 * <p>Builds the JSON Query in Elasticsearch's proprietary DSL, starting from the string-based
 * query syntax.
 *
 * <p>It also uses filters and weights to fine-tune the search results rankings: see the
 * {@link SearchWeightKey weights} and {@link SearchFilterKey filter} key classes.
 *
 * <p>In order to fully customize the structure of the ranking, we make use of the
 * <a href="https://www.elastic.co/guide/en/elasticsearch/guide/2.x/function-score-query.html">
 * "function_score"</a> function.
 *
 * <p>Most of the defaults and other fine-tuning is possible via
 * {@link SearchConfiguration configuration} values, in the {@literal application.yaml} file.
 *
 * <p>The end result is something similar to (see also the {@literal queries-samples.json}
 * examples).
 *
 * <pre>
    {
        "query":{
            "function_score":{
                "query": {
                    "bool": {
                        "should": [
                            { "multi_match": {
                                "query": "1034 sql injection",
                                "fields": ["model.name", ... ,"model.description"]
                            } },
                            { "match": {
                                    "model.id": {
                                        "query": "1034",
                                        "lenient": true,
                                        "boost": 5
                                    }
                            } },
                        ],
                        "boost": 0.3      -- Relevance weight (wr) applied to (raw) _score
                    }
                },
                "functions":[
                    {"exp":{"_when":{"origin":1482021910617,"offset":604800000,"scale":7776000000}},
                        "weight":0.4       -- Recency weight (wt)
                    },
                    {"field_value_factor": {
                        "field": "_enhanced",
                        "modifier": "none",
                        "factor": 0.3     -- Vulnerability has teardown weight ("enhanced", we) }
                    }
                ],
                "score_mode": "sum",  -- linear combination of functions
                "boost_mode": "sum"   -- linear combination with _score
            }
        }
    }
 * </pre>
 *
 * <p>The "raw" score (obtained applying the
 * <a href="https://www.elastic.co/guide/en/elasticsearch/guide/2.x/scoring-theory.html">TF/IDF</a>
 * algorithm) is combined (according to the
 * {@link io.kapsules.search.api.data.Constants#BOOST_MODE} method defined in the
 * {@link SearchConfiguration#boostMode configuration}), with a combination of the scoring
 * functions; these functions are themselves combined according to the
 * {@link io.kapsules.search.api.data.Constants#SCORE_MODE} field, as configured by
 * {@link SearchConfiguration#scoreMode}.
 *
 * @author marco
 * @see SearchConfiguration
 */
@Component
public class JsonQueryRenderer implements SearchQueryRenderer<JsonObject> {

  private List<String> matchFields = Lists.newArrayList();
  private final SearchConfiguration configuration;

  @Autowired
  public JsonQueryRenderer(SearchConfiguration configuration) {
    this.configuration = configuration;
  }

  @Autowired
  @Qualifier("matchFields")
  public void setMatchFields(List<String> matchFields) {
    this.matchFields.addAll(matchFields);
  }


  @Override
  public JsonObject render(SearchQuery query) {
    JsonBuilderFactory factory = Json.createBuilderFactory(Collections.emptyMap());

    JsonArrayBuilder fields = factory.createArrayBuilder();
    matchFields.forEach(
        s -> fields.add(String.format("model.%s", s))
    );

    JsonObjectBuilder termsMatch = factory.createObjectBuilder();

    if (StringUtils.isNotBlank(query.getMatchTerms())) {
      JsonArrayBuilder matchArray = factory.createArrayBuilder()
          .add(termsMatch(query, factory, fields));

      // TODO: these two methods should belong to the Parser, not here in the Renderer.
      idMatch(query, factory).ifPresent(matchArray::add);
      cveMatch(query);
      termsMatch.add(SHOULD, matchArray);
    }

    Map<SearchWeightKey, Float> weights = query.getWeights();
    termsMatch.add("boost", weights.get(SearchWeightKey.RELEVANCE));

    if (!query.getFilters().isEmpty()) {
      JsonArrayBuilder mustClause = factory.createArrayBuilder();

      query.getFilters()
          .forEach((filter, value) -> filter.json(value).ifPresent(mustClause::add));

      termsMatch.add(MUST, mustClause);
    }

    JsonArrayBuilder excludeFields = factory.createArrayBuilder();
    if (query.isPublicView()) {
      JsonArrayBuilder mustNotMatch = factory.createArrayBuilder();

      // Hide all artifacts except released stage
      EnumSet.allOf(Stage.class).stream()
          .filter(Stage::unreleased)
          .forEach(stage -> {
            mustNotMatch.add(Json.createObjectBuilder()
                .add(MATCH, Json.createObjectBuilder()
                    .add(STAGE_NAME_KEY, stage.name()))
                .build());
          });

      // Note - we cannot use a "positive" MUST clause, as we already have one (for the filters)
      // and this seems to cause issues with Elasticsearch parsing.  Thus we use a negative
      // assertion.
      termsMatch.add("must_not", mustNotMatch);

      // Exclude certain attributes from public view
      configuration.getExcludeFields()
              .forEach(excludeFields::add);
    }

    return factory.createObjectBuilder()
        .add(QUERY, factory.createObjectBuilder()
            .add(FUNCTION_SCORE, factory.createObjectBuilder()
                .add(QUERY, factory.createObjectBuilder()
                    .add(BOOL, termsMatch))
                .add(FUNCTIONS, functions(weights))
                .add(SCORE_MODE, configuration.getScoreMode())
                .add(BOOST_MODE, configuration.getBoostMode())
            )
        )
        .add(SOURCE_KEY, factory.createObjectBuilder()
            .add(EXCLUDES, excludeFields)
        )
        .add(OFFSET, query.getOffset())
        .add(SIZE, query.getSize())
        .build();
  }

  /**
   * Parses the query terms for fields of the type "CVE-2011-7432"
   * it adds the appropriate {@link SearchFilterKey#CVE_ID} and {@link SearchFilterKey#CVE_YEAR}
   * filters.
   *
   * @param query the query object, to which we will add appropriate filters if one of the terms
   *          matches the pattern.
   */
  private void cveMatch(SearchQuery query) {
    Splitter.on(' ')
        .omitEmptyStrings()
        .trimResults()
        .splitToList(query.getMatchTerms())
        .stream()
        .filter(s -> CVE_PATTERN.matcher(s).matches())
        .findFirst()  // this is just a safeguard for nonsense query: "CVE-2011-88 CVE-2011-98"
        .map(CVE_PATTERN::matcher)
        .ifPresent(m -> {
            if (m.matches()) {
              query.getFilters().put(SearchFilterKey.CVE_YEAR, m.group("year"));
              query.getFilters().put(SearchFilterKey.CVE_ID, m.group("id"));
            }
        });
  }

  private JsonObjectBuilder termsMatch(SearchQuery query,
                                       JsonBuilderFactory factory,
                                       JsonArrayBuilder fields
  ) {
    JsonObjectBuilder result = factory.createObjectBuilder();

    // If any of the search terms was an ID search term, we want to only
    // retain the `id` part only in the multi-match field (the pre-processing in idMatch() has
    // already taken care of adding the necessary clauses for exact ID match).
    String idOnlyTerms = Splitter.on(' ')
        .omitEmptyStrings()
        .trimResults()
        .splitToList(query.getMatchTerms())
        .stream()
            .map(s -> {
              Matcher m = ID_MATCH_REGEX_PATTERN.matcher(s);
              if (m.matches()) {
                return m.group("id");
              }
              // We leave all non-matching terms alone.
              return s;
            })
        .collect(Collectors.joining(" "));

    // If the original query was non-empty, but, after removing the ID matching it is empty, then we
    // can omit the 'multi-match' term, as this is an ID-only search.
    if (StringUtils.isNotBlank(query.getMatchTerms()) && StringUtils.isBlank(idOnlyTerms)) {
      return result;
    }
    result.add(MULTI_MATCH, factory.createObjectBuilder()
        .add(QUERY, idOnlyTerms)
        .add("fields", fields)
    );
    return result;
  }

  /**
   * Attempts to match {@literal model.id} with the query sent, if any of the terms matches
   * {@link io.kapsules.search.api.data.Constants#ID_MATCH_REGEX_PATTERN the regex pattern}.
   *
   * <p>It will attempt to find SID- or LID- occurrence and extract the ID to search.
   *
   * For example:
   * <ol>
   * <li>query="cross site SID-1042" will attempt to search model.id = 1042.</li>
   * <li>query="cross site 1042" will not specifically search for {@literal model.id} "1042"</li>
   * <li>query="1042 cross site" will search for id "1042" (and, obviously, for "cross site"
   * too</li>
   * </ol>
   * This builds a "multi-match" query on the {@literal "model.id"} and {@literal }:
   * <pre>
      {
          "multi_match": {
              "query": "1234",
              "fields": ["model.id", "model.artifactComponents.componentId"],
              "boost": <<EXACT_ID weight>>
          }
      }
   * </pre>
   */
  private Optional<JsonObjectBuilder> idMatch(SearchQuery query, JsonBuilderFactory factory) {
    JsonObjectBuilder result = factory.createObjectBuilder();

    // Check if there's any occurrence of SID- or LID- among the search string, or just a plain ID.
    final Optional<String> idSearch = Splitter.on(' ')
        .omitEmptyStrings()
        .trimResults()
        .splitToList(query.getMatchTerms())
        .stream()
        .map(s -> {
          Matcher m = ID_MATCH_REGEX_PATTERN.matcher(s);
          if (m.matches()) {
            // Refinement: by distinguishing between SID / LID, we
            // add a `type` filter to further limit the scope of the search
            // (vulnerability or library, respectively).
            //
            if (m.group("type") != null
                && !query.getFilters().containsKey(SearchFilterKey.TYPE)) {
              query.getFilters().put(SearchFilterKey.TYPE,
                  "LID".equals(m.group("type")) ? LIBRARY : ARTIFACT);
            }
            return m.group("id");
          }
          return null;
        })
        .filter(StringUtils::isNotBlank)
        .findFirst();

    JsonArray fields = factory.createArrayBuilder()
        .add(ID_KEY)
        .add(COMPONENT_ID_KEY)
        .add(CVE_ID_KEY)
        .add(CVE_YEAR_KEY)
        .build();

    return idSearch.map(id -> result.add(MULTI_MATCH, factory.createObjectBuilder()
        .add(QUERY, id)
        .add("fields", fields)
        .add(BOOST, query.getWeights().get(SearchWeightKey.EXACT_ID))
    ));
  }

  @VisibleForTesting
  JsonArray functions(Map<SearchWeightKey, Float> scoringWeights) {
    JsonArrayBuilder fieldValueFactors = Json.createArrayBuilder()
        .add(recencyRanking(scoringWeights));

    configuration.getScoring().getFieldValueFactors()
        .forEach(p -> fieldValueFactor(p, scoringWeights)
            .ifPresent(fieldValueFactors::add));

    return fieldValueFactors.build();
  }


  /**
   * Builds the scoring factor for the "recency" ranking (time-based).
   *
   * <p>All the parameters for the scoring function (including the type of function, e.g. "exp")
   * are retrieved from the configuration values.
   *
   * <pre>
         {
             "exp": {
                 "_when": {
                     "origin": 1482021910617,
                     "offset": 604800000,
                     "scale":7776000000
                 }
             },
             "weight": 0.4 <-- wt
         }
   * </pre>
   *
   * @param scoringWeights the map containing the ({@link SearchWeightKey#TIME} wt) weight
   * @return the scoring factor; if the "wt" weight is not present, an empty object is returned
   *
   * @see SearchConfiguration#scoring
   * @see SearchConfiguration.ScoringFunctionsParams
   */
  private JsonObject recencyRanking(Map<SearchWeightKey, Float> scoringWeights) {
    checkNotNull(scoringWeights, "Score ranking weights must be non-null in a search query");
    JsonBuilderFactory factory = Json.createBuilderFactory(Collections.emptyMap());

    if (!scoringWeights.containsKey(SearchWeightKey.TIME)) {
      return factory.createObjectBuilder().build();
    }
    Float wt = scoringWeights.get(SearchWeightKey.TIME);

    Map<String, Long> recentScoringParams = configuration.getScoring().getRecent();
    return factory.createObjectBuilder()
        .add(SCORING_TIME_FUNCTION, factory.createObjectBuilder()
            .add(TIMESTAMP_KEY, factory.createObjectBuilder()
                .add(SCORING_TIME_ORIGIN, System.currentTimeMillis())
                .add(SCORING_TIME_OFFSET, recentScoringParams.get(SCORING_TIME_OFFSET))
                .add(SCORING_TIME_SCALE, recentScoringParams.get(SCORING_TIME_SCALE))
            ))
        .add(WEIGHT, wt)
        .build();
  }

  /**
   * Builds the scoring factor for the fields configured to be "field_value_factor."
   *
   * <pre>
      {
          "field_value_factor": {
              "field": "_enhanced",
              "modifier": "none",
              "missing": 0,    -- As defined in the configuration, the value to use if the field is missing
              "factor": 0.3    -- Weight (we) for enhanced field.
          }
      }
   * </pre>
   *
   * <p>See also
   * <a href="https://www.elastic.co/guide/en/elasticsearch/guide/master/boosting-by-popularity.html">
   * "Boosting by Popularity"</a> in the Elasticsearch guide for examples.
   *
   * @param scoringWeights the map containing the {@link SearchWeightKey weights}
   * @return the scoring factor clause for inclusion in the "function_score" array.
   */
  @VisibleForTesting
  Optional<JsonObject> fieldValueFactor(
      SearchConfiguration.FieldValueScoringParams params,
      Map<SearchWeightKey, Float> scoringWeights
  ) {
    checkNotNull(params, "Must provide a valid configuration for the field value");
    checkNotNull(scoringWeights, "Score ranking weights must be non-null in a search query");

    // We should always have a valid weight value (if not specified, default configured values
    // will be used); however, even passing a `null` value is acceptable for ES.
    Float weight = scoringWeights.get(SearchWeightKey.of(params.getWeightKey()));
    if (weight == null) {
      return Optional.empty();
    }
    JsonBuilderFactory factory = Json.createBuilderFactory(Collections.emptyMap());

    return Optional.of(factory.createObjectBuilder()
        .add(FIELD_VALUE_FACTOR, factory.createObjectBuilder()
            .add("field", params.getField())
            .add("modifier", params.getModifier())
            .add("missing", params.getMissing())
            .add("factor", weight)
            .build()
        )
        .build());
  }
}
