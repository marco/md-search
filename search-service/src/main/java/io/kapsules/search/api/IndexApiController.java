// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api;

import io.kapsules.search.ElasticSearchOutcome;
import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.api.data.io.kapsules.search.models.MarkdownDocument;
import io.kapsules.search.data.Repository;
import io.kapsules.search.services.RegistrySearchIndexerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static io.kapsules.search.api.data.Constants.*;


/**
 * Main endpoint for the {@literal /index} endpoint.
 * <p>
 * <p>For a full description of the API, please see the README file.</p>
 *
 * @author marco
 */
@RestController
@RequestMapping(INDEX_ENDPOINT)
public class IndexApiController {

  private static final Logger LOG = LoggerFactory.getLogger(IndexApiController.class);

  private final RegistrySearchIndexerService indexerService;
  private final Repository<ArtifactModelWrapper> artifactRepository;

  @Autowired
  public IndexApiController(RegistrySearchIndexerService indexerService,
                            Repository<ArtifactModelWrapper> artifactRepository
  ) {
    this.indexerService = indexerService;
    this.artifactRepository = artifactRepository;
  }


  /**
   * Fetches the Document whose ID is passed in the path, if it exists and it has been indexed in
   * ElasticSearch; it will <strong>not</strong> try to index it, if missing.
   *
   * <p>Mostly intended for internal and diagnostic use, best NOT exposed to users.
   *
   * @param id document ID
   * @return the full document (in the {@literal "entity"} field.
   */
  @RequestMapping(ARTIFACTS_ENDPOINT + "/{id}")
  public ResponseEntity<?> findArtifactById(@PathVariable(value = "id") Long id) {
    LOG.debug("Looking up artifact [{}]", id);
    Optional<ArtifactModelWrapper> wrapper = artifactRepository.findOne(id);

    if (wrapper.isPresent()) {
      MarkdownDocument model = unwrap(wrapper);
      LOG.debug("Document '{}' found: {}",
          model.getId(),
          model.getTitle());
      return ResponseEntity.ok(model);
    } else {
      LOG.debug("Artifact [{}] not found in index", id);
      return ResponseEntity.notFound().build();
    }
  }

  private MarkdownDocument unwrap(Optional<ArtifactModelWrapper> wrapper) {
    return null;
  }

  /**
   * Cause the Libraries whose {@literal ids} are passed in the body of the request to be
   * retrieved from Librarian and pushed to ElasticSearch for indexing.
   *
   * @param ids the list of Libraries' IDs as known to Librarian.
   * @return a map of the successful/failed updated
   */
  @RequestMapping(value = LIBRARIES_ENDPOINT, method = RequestMethod.POST)
  public ResponseEntity<Map<ElasticSearchOutcome, Set<Long>>> indexLibraries(@RequestBody List<Long> ids) {
    return ResponseEntity.ok(indexerService.indexLibraries(ids));
  }

  /**
   * Cause the Artifacts whose {@literal ids} are passed in the body of the request to be
   * retrieved from the Platform API and pushed to ElasticSearch for indexing.
   *
   * @param ids the list of vulnerabilities' IDs as known to the Platform.
   * @return a map of the successful/failed updated
   */
  @RequestMapping(value = ARTIFACTS_ENDPOINT, method = RequestMethod.POST)
  public ResponseEntity<?> indexArtifacts(@RequestBody List<Long> ids) {
    return ResponseEntity.ok(indexerService.indexArtifacts(ids));
  }

}
