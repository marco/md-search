// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.api;

import com.google.common.collect.Maps;
import io.kapsules.search.data.SearchService;
import io.kapsules.search.parsing.QueryParser;
import io.kapsules.search.parsing.SearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static io.kapsules.search.api.data.Constants.QUERY_PARAM_KEY;
import static io.kapsules.search.api.data.Constants.SEARCH_ENDPOINT;

/**
 * The search API controller for the registry-search service: this is where stuff happens.
 *
 * @author marco
 */
@RestController
@RequestMapping(value = SEARCH_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
public class SearchApiController {

  private static final Logger LOG = LoggerFactory.getLogger(SearchApiController.class);

  private SearchService searchService;
  private QueryParser parser;

  @Autowired
  public SearchApiController(SearchService searchService, QueryParser parser) {
    this.searchService = searchService;
    this.parser = parser;
  }

  /**
   * The main Search API endpoint, accepts a query string and, optionally, an offset and size
   * arguments.
   * <p>
   * <p>See the README document for a full description of the query syntax and the meaning of the
   * filter and weight keys.
   *
   * @param queryString the search query.
   * @param publicView  if this is true, it would hide certain attribute from the result as well as
   *                    only showing released artifacts
   * @param pageable    paging and sorting query arguments ('page' and 'limit'); the offset for the
   *                    returned search result will be computed by {@literal page * limit}
   * @return the search result.
   * @see Pageable
   */
  @RequestMapping
  public ResponseEntity<?> get(@RequestParam(QUERY_PARAM_KEY) String queryString,
                               @RequestParam(value = "publicView", defaultValue = "true") boolean publicView,
                               Pageable pageable) {
    checkNotNull(queryString, String.format("`%s` query param required", QUERY_PARAM_KEY));
    LOG.debug("Query string: {}", queryString);


    SearchQuery query = null;
    try {
      query = parser.parse(queryString, publicView);
      query.setOffset(pageable.getOffset());
      query.setSize(pageable.getPageSize());
      query.setPublicView(publicView);

      LOG.debug("Terms: {}, Filters: {}, Weights: {}, Released only: {}, Public view: {} ", query.getMatchTerms(),
          query.getFilters(), query.getWeights(), query.isPublicView());

      return ResponseEntity.ok(searchService.search(query));

    } catch (IllegalArgumentException | ParseException nex) {
      LOG.debug("Could not parse query '{}'. Reason: {}", queryString, nex.getLocalizedMessage());
      return ResponseEntity.badRequest().body(makeErrorResponse(query,
          "Could not parse query string: '" + queryString + "'", nex));
    } catch (IllegalStateException sex) {
      LOG.debug("There was an error processing the HTTP request: {}", sex.getMessage());
      return ResponseEntity.unprocessableEntity().body(makeErrorResponse(query,
          "Could not send the HTTP request to the Elasticsearch server", sex));
    }
  }

  /**
   * Enables posting a JSON query, using Elasticsearch DSL.
   *
   * <p>This is meant to be used mostly internally for research and fine-tuning, as there are
   * several limitations (most notably, the {@literal "fields"} option cannot be used) in the
   * parsing of the results.
   *
   * <p>However, one advantage (wrt hitting directly ES endpoints) is that the response is in a
   * format that is compatible with our IR, so it can be directly used by any UI
   * component; also, it does not require accessing directly the ES cluster (which may be a
   * security concern).
   *
   * @param body a JSON-formatted Elasticsearch DSL query.
   * @return the results of the query, if available. Much can go wrong here.
   */
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> postJson(@RequestBody String body) {

    try {
      JsonObject query = Json.createReader(new StringReader(body)).readObject();
      return ResponseEntity.ok(searchService.queryDirect(query, null));
    } catch (JsonException | IllegalStateException ex) {
      LOG.error("Could not parse the query into valid JSON: {}\n{}", ex.getMessage(), body);
      return ResponseEntity.badRequest().body(
          makeErrorResponse(body, "Could not parse JSON query", ex));
    }
  }

  private Map<String, ?> makeErrorResponse(@Nullable Object query,
                                           @NotNull String errorMsg,
                                           @Nullable Throwable throwable) {
    Map<String, Object> result = Maps.newHashMap();
    if (query != null) {
      result.put("query", query);
    }
    result.put("error", errorMsg);
    Optional.ofNullable(throwable).ifPresent(t -> {
      result.put("cause", t.getMessage());
      if (t.getCause() != null) {
        result.put("caused_by", t.getCause().getMessage());
      }
    });
    return result;
  }
}
