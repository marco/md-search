// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.api;

import io.kapsules.search.api.data.SearchServiceMessage;
import io.kapsules.search.services.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static io.kapsules.search.api.data.Constants.*;


/**
 * Main endpoint for the {@literal /queue} endpoint.
 *
 * Meant for internal use where we might need to reindex the library or artifact. This endpoint helps
 * to put messages into the rabbitMQ queue to be indexed later.
 */
@RestController
@RequestMapping(QUEUE_ENDPOINT)
public class QueueApiController {

  private final QueueService queueService;

  @Autowired
  public QueueApiController(QueueService queueService) {
    this.queueService = queueService;
  }


  @RequestMapping(value = LIBRARIES_ENDPOINT, method = RequestMethod.POST)
  public ResponseEntity<?> addLibrariesToQueue(@RequestBody List<Long> ids) {
    queueService.addMessageToQueue(SearchServiceMessage.IndexType.LIBRARY, ids);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = ARTIFACTS_ENDPOINT, method = RequestMethod.POST)
  public ResponseEntity<?> addArtifactsToQueue(@RequestBody List<Long> ids) {
    queueService.addMessageToQueue(SearchServiceMessage.IndexType.ARTIFACT, ids);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
