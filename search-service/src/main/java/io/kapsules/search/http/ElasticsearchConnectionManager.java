// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.http;

import io.kapsules.search.config.ServicesConfiguration;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.json.JsonObject;
import java.io.Closeable;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Manages the HTTP client connections towards Elasticsearch by pooling connections.
 *
 * @see PoolingHttpClientConnectionManager
 *
 * Created by marco on 2/9/17.
 */
@Component
public class ElasticsearchConnectionManager implements Closeable {

  private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchConnectionManager.class);

  private final HttpRoute route;
  private final HttpClientConnectionManager clientConnectionManager;
  private final Integer requestTimeout;

  @Autowired
  public ElasticsearchConnectionManager(ServicesConfiguration servicesConfiguration) {
    HttpHost elasticsearchHost = new HttpHost(
        servicesConfiguration.getElasticsearch().getHostname(),
        servicesConfiguration.getElasticsearch().getPort());

    this.route = new HttpRoute(elasticsearchHost);

    PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
    cm.setMaxTotal(servicesConfiguration.maxTotalConcurrentConnections());
    cm.setDefaultMaxPerRoute(servicesConfiguration.defaultMaxConnectionsPerRoute());

    this.clientConnectionManager = cm;
    requestTimeout = servicesConfiguration.defaultTimeout();
  }

  public HttpResponse sendRequest(String method, String apiEndpoint, JsonObject body) throws IOException {
    return sendRequest(method, apiEndpoint, body.toString());
  }


  public HttpResponse sendRequest(String method, String apiEndpoint, String body) throws IOException {
    checkNotNull(method, "Must specifiy a GET or POST method");

    HttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest(method, apiEndpoint);
    request.addHeader("Content-Type", APPLICATION_JSON_VALUE);
    request.addHeader("Accept", APPLICATION_JSON_VALUE);

    RequestConfig config = RequestConfig.custom()
        .setConnectionRequestTimeout(requestTimeout)
        .build();

    if (body != null) {
      request.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
    }

    CloseableHttpClient client = HttpClients.custom()
        .setConnectionManager(clientConnectionManager)
        .setDefaultRequestConfig(config)
        .build();

    return client.execute(route.getTargetHost(), request);
  }

  @Override
  public void close() throws IOException {
    ((PoolingHttpClientConnectionManager) clientConnectionManager).close();
  }

  public HttpResponse get(String endpoint) throws IOException {
    return sendRequest(HttpMethod.GET.name(), endpoint, (String) null);
  }

  public HttpResponse sendRequest(String method, String endpoint) throws IOException {
    return sendRequest(method, endpoint, (String) null);
  }
}
