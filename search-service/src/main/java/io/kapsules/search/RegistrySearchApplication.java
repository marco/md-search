// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;

import static org.apache.commons.lang3.ObjectUtils.firstNonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.stripToNull;

@SpringBootApplication
public class RegistrySearchApplication {

  ///////////////////////////// Class Attributes \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  private static final Logger LOGGER = initializeSentryReleaseAwareLogger();

  ////////////////////////////// Class Methods \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  /**
   * Defers initialization (for as much as possible, in a static block of the {@code main} class)
   * in an attempt to sniff out the Sentry {@literal sentry.release}, or {@literal build.number},
   * or {@literal $SENTRY_RELEASE}, or {@literal $BUILD_NUMBER} (in that order)
   * from {@literal /git.properties} or the environment (as the {@literal $} indicates.
   * <p>
   *     We need Logback to be aware of the Sentry release in order to be able to resolve,
   *     or track the regression of, events generated solely by the logging appender.
   * </p>
   */
  static Logger initializeSentryReleaseAwareLogger() {
    final Properties p = new Properties();
    try (final InputStream stream = RegistrySearchApplication.class.getResourceAsStream("/git.properties")) {
      p.load(stream);
    } catch (Exception e) {
      // welp, too bad, we're going to have to boot up logback without sentry
      LoggerFactory.getLogger(RegistrySearchApplication.class).error("Unable to sniff out sentry release", e);
    }
    final Function<String, String> envEmptyToNull = envName -> stripToNull(System.getenv(envName));
    // WARNING: don't use MoreObjects.firstNonNull from Guava, as it NPEs if there are no "values"
    final String buildNumber = Optional.ofNullable(firstNonNull(
            // although it might be the case that $SENTRY_RELEASE in the env Just Works(tm)
            envEmptyToNull.apply("SENTRY_RELEASE"), envEmptyToNull.apply("BUILD_NUMBER")))
            .orElseGet(() -> p.getProperty("sentry.release", p.getProperty("build.number")));
    if (isNotBlank(buildNumber)) {
      System.setProperty("sentry.release", buildNumber);
    }
    // NOW boot up logback, so it will see that
    return LoggerFactory.getLogger(RegistrySearchApplication.class);
  }

  /**
   * Captures the current Spring Boot profiles, along with the URLs of relevant service endpoints
   * so we may display them at the end of the app boot-up process for extreme clarity.
   */
  private static String gatherRuntimeInformation(Environment env) throws IOException {
    final String preamble;
    if (env.getActiveProfiles().length == 0) {
      preamble = "\nWARNING\nNo Spring profile configured, running with default configuration\n";
    } else {
      preamble = "";
    }
    return String.format(
            "%s\n"
            + "Running with Spring profile(s): %s\n",
            preamble, Arrays.toString(env.getActiveProfiles()));
  }

  //////////////////////////////// Attributes \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  /////////////////////////////// Constructors \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  ////////////////////////////////// Methods \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  public static void main(String[] args) throws Exception {
    SpringApplication app = new SpringApplicationBuilder(RegistrySearchApplication.class).web(true).build();

    // we cannot close this in try-resources because the web environment implicitly
    // starts up handler threads which continue to run when flow-of-control exits the bottom of "main"
    @SuppressWarnings("resource")
    final Environment env = app.run(args).getEnvironment();
    final String hostAddress = InetAddress.getLocalHost().getHostAddress();
    // q.v. the application.yml where info: build: version: is Maven filtered to be pom.version+git+${BUILD_NUMBER}
    final String version = env.getProperty("info.build.version");

    final String sentryReleaseProp = System.getProperty("sentry.release");
    final String sentryRelease;
    if (isNotBlank(sentryReleaseProp)) {
      // if we have already sniffed it out, don't bother with this tomfoolery
      sentryRelease = sentryReleaseProp;
    } else {
      // and it is the BUILD_NUMBER that we report to Sentry
      sentryRelease = version.replaceFirst(".+\\.([^.]+)$", "$1");
      System.setProperty("sentry.release", sentryRelease);
    }

    final int managementPort = env.getProperty("management.port", Integer.class, -1);
    final String managementCtx = env.getProperty("management.context-path");
    final URI managementURI = new URI("http", null, hostAddress, managementPort, managementCtx, null, null);
    final URI infoURI = managementURI.resolve(managementURI.getPath() + "/info");
    final String port = env.getProperty("server.port", "8080");
    final String webappURI = env.getProperty("server.context-path", "");
    LOGGER.info(
            "{}\n"
                    + "Access URLs:\n"
                    + "----------------------------------------------------------\n"
                    + "\tLocal: \t\thttp://127.0.0.1:{}{}\n"
                    + "\tExternal: \thttp://{}:{}{}\n"
                    + "\tVersion: \t{} (also: {}) [sentry.release={}]\n"
                    + "----------------------------------------------------------",
            gatherRuntimeInformation(env),
            port, webappURI,
            hostAddress,
            port, webappURI, version, infoURI,
            sentryRelease
    );
  }
}
