// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.api.data.SearchResults;
import io.kapsules.search.http.ElasticsearchConnectionManager;
import io.kapsules.search.keys.SearchWeightKey;
import io.kapsules.search.parsing.SearchQuery;
import io.kapsules.search.parsing.SearchQueryRenderer;
import io.kapsules.util.JsonUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.json.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static io.kapsules.search.api.data.Constants.*;
import static io.kapsules.search.keys.SearchFilterKey.TYPE;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * This is the "core" service in that it provides the searching ability against the Elasticsearch
 * index.
 *
 * <p>It uses the ES API to submit a custom query (built via JSON) and collating the results to
 * be returned to the clients.
 *
 * @author marco
 */
@Service
public class ElasticsearchService implements SearchService {
  private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchService.class);
  private static final ObjectMapper MAPPER = new ObjectMapper();

  private SearchQueryRenderer<JsonObject> renderer;
  private final ElasticsearchConnectionManager connectionManager;
  private final Integer defaultResultsSize;

  @Autowired
  public ElasticsearchService(ElasticsearchConnectionManager connectionManager,
                              SearchQueryRenderer<JsonObject> renderer,
                              Integer defaultResultsSize) {
    this.renderer = renderer;
    this.connectionManager = connectionManager;
    this.defaultResultsSize = defaultResultsSize;
  }


  @Override
  public String getQueryTemplate() {
    Map<SearchWeightKey, Float> allZeroWeights = Maps.newEnumMap(SearchWeightKey.class);
    Arrays.asList(SearchWeightKey.values())
        .forEach(k -> allZeroWeights.put(k, 0.0F));

    SearchQuery dummy = SearchQuery.builder()
        .withTerm("###")
        .withWeights(allZeroWeights)
        .build();
    return buildJsonQuery(dummy).toString();
  }

  @Override
  public JsonObject buildJsonQuery(@NotNull SearchQuery query) {
    return renderer.render(query);
  }

  @Override
  public SearchResults search(SearchQuery query) {

    JsonObject json = renderer.render(query);
    LOG.debug("JSON Query: {}", json);

    SearchResults results = queryDirect(json, query.getFilters().get(TYPE));

    LOG.debug("Search terms '{}', found {} results",
        query.getMatchTerms(), results.getMetadata().getHits());

    LOG.debug("Found {} libraries, {} vulnerabilities", results.getMetadata().getLibrariesCount(),
        results.getMetadata().getArtifactsCount());
    return results;
  }

  @Override
  public <T> Optional<T> getDocumentById(@NotNull Long id, String index, Class<T> clazz) {
    HttpResponse response = null;
    try {
      response = connectionManager.get(
          "/" + join(Lists.newArrayList(MAIN_ES_INDEX, index, id.toString()), "/"));
      if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
        return getModelFromResponse(response, clazz);
      }
    } catch (Exception e) {
      LOG.error("Could not retrieve {} [{}]. Reason: {}", index, id, e.getMessage());
    } finally {
      closeHttpResponse(response);
    }
    return Optional.empty();
  }

  /**
   * Used to send directly to the Elasticsearch server a JSON query in ES DSL.
   *
   * @param query a search query formulated using Elasticsearch's custom DSL.
   * @return the search results from the query.
   */
  @Override
  public SearchResults queryDirect(JsonObject query, String documentType) {
    Integer offset = 0;
    Integer size = defaultResultsSize;

    try {
      offset = query.getInt(OFFSET);
      size = query.getInt(SIZE);
    } catch (NullPointerException npe) {
      // either of them is safe to ignore, as those values may not be in the query; this would
      // still be a valid query, returning the default number of hits (typically, 10) from offset 0.
    }

    final String endpoint = buildEndpoint(documentType);
    HttpResponse response = null;

    try {
      response = connectionManager.sendRequest(HttpMethod.POST.name(), endpoint, query);
      if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
        try {
          return parseResponse(response, offset, size);
        } catch (ParseException e) {
          // This is only useful during development / testing, otherwise it causes too much noise
          // in the logs.
          LOG.debug("There was a parsing error (offset: {}): {}", e.getErrorOffset(), e.getMessage());
          throw new JsonException("Could not parse the response into a valid search result", e);
        }
      } else {
        final String errorMessage = String.format("Got an error [%d] while posting JSON. Reason: %s; Content: %s",
            response.getStatusLine().getStatusCode(),
            response.getStatusLine().getReasonPhrase(),
            query.toString());
        throw new IllegalStateException(errorMessage);
      }
    } catch (Exception e) {
      LOG.error("Could not post JSON:{}\nReason: {}", JsonUtils.prettyPrint(query), e.getMessage());
      throw new IllegalStateException("Could not post JSON", e);
    } finally {
      closeHttpResponse(response);
    }
  }

  @VisibleForTesting
  String buildEndpoint(String documentType) {
    checkArgument(documentType == null || VULNERABILITY.equals(documentType)
            || ARTIFACT.equals(documentType) || LIBRARY.equals(documentType),
        "type: filter should specify either 'artifact' or 'library' (was: '%s')", documentType);

    if (VULNERABILITY.equals(documentType)) {
      documentType = ARTIFACT;
    }
    return "/"
        + (documentType == null ? join(Lists.newArrayList(MAIN_ES_INDEX, "_search"), "/")
        : join(Lists.newArrayList(MAIN_ES_INDEX, documentType, "_search"), "/"));
  }

  private <T> Optional<T> getModelFromResponse(HttpResponse response, Class<T> clazz) throws
      IOException {
    return Optional.ofNullable(response.getEntity()).map(entity -> {
      Map<?, ?> body;
      try {
        body = MAPPER.readValue(entity.getContent(), Map.class);
        if (body.containsKey(SOURCE_KEY)) {
          final Map<String, ?> source = (Map<String, ?>) body.get(SOURCE_KEY);
          if (source.containsKey(MODEL_KEY)) {
            return MAPPER.readValue(MAPPER.writeValueAsString(source.get(MODEL_KEY)), clazz);
          }
        }
      } catch (IOException e) {
        LOG.error("Could not parse response into a valid model ({}): {}",
            clazz.getName(), e.getMessage());
      }
      return null;
    });
  }

  private SearchResults parseResponse(HttpResponse response, Integer offset, Integer size)
      throws IOException, ParseException {
    final HttpEntity entity = response.getEntity();
    SearchResults results = null;
    if (entity != null) {
      JsonReader reader = Json.createReader(entity.getContent());
      JsonObject content = reader.readObject();

      // TODO: once we add metrics to the service, we may need to store the actual original query
      // alongside the time it took to serve it.
      LOG.debug("Query took: {} ms", content.getJsonNumber("took").longValue());

      long hitsCount = ((JsonNumber) JsonUtils.get(content, "hits.total")).longValue();
      if (hitsCount == 0) {
        // There were no results, the response from the Elasticsearch server cannot be parsed and
        // we must terminate here.
        return SearchResults.EMPTY;
      }
      float maxScore = ((JsonNumber) JsonUtils.get(content, "hits.max_score")).bigDecimalValue().floatValue();
      float minScore = 0f;

      results = new SearchResults(hitsCount, size, offset, maxScore);

      // Hits is an array of results, whose _source.model is either a Library or an Artifact.
      final JsonArray hits = (JsonArray) JsonUtils.get(content, "hits.hits");
      for (JsonValue hit : hits) {
        JsonObject object = (JsonObject) hit;

        final String type = ((JsonString) JsonUtils.get(object, TYPE_KEY)).getString();
        final Float score = ((JsonNumber) JsonUtils.get(object, SCORE_KEY)).bigDecimalValue().floatValue();

        // As hits are always returned by ES in ranking score descending, we keep updating and
        // we'll be left with the lowest at the end.
        minScore = score;

        String id = ((JsonString) JsonUtils.get(object, "_id")).getString();
        LOG.debug("{}: {}, score: {}", id, type, score);

        try {
          if (ARTIFACT.equals(type)) {
            results.append(JsonUtils.get(object, SOURCE_KEY, ArtifactModelWrapper.class), score);
          } else {
            LOG.error("Unexpected result type: {}", type);
          }
        } catch (NullPointerException nex) {
          // This only happens if we cannot find the _source.model object in the returned
          // response, which in turns indicates a deeper problem with the query: we must give up
          // here.
          throw new ParseException("Could not parse a valid 'model' in the response from server",
              Integer.parseInt(id));
        }
      }
      results.setLowestScore(minScore);
    }
    return results;
  }

  // Helper to close any http response that has been opened.
  private void closeHttpResponse(HttpResponse response) {
    if (response != null) {
      EntityUtils.consumeQuietly(response.getEntity());
    }
  }
}
