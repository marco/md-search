// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.data;

import io.kapsules.search.keys.LibraryIndexEntity;
import org.springframework.data.repository.CrudRepository;

/**
 *
 */
public interface LibraryIndexRedisRepository extends CrudRepository<LibraryIndexEntity, Long> {

  ////////////////////////////////// Methods \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

}
