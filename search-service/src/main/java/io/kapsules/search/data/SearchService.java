// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import io.kapsules.search.api.data.SearchResults;
import io.kapsules.search.parsing.SearchQuery;
import io.kapsules.search.parsing.QueryParser;
import io.kapsules.search.parsing.SimpleQueryParser;

import javax.annotation.Nullable;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * The main search service interface API, it provides the ability to run a custom query via
 * Elasticsearch API, by building a JSON query from a {@link SearchQuery}.
 *
 * <p>In turn, the query can be built from a string query such as, for example:
 * <pre>
 *   sql injection language:java wr:0.4
 * </pre>
 * by
 * {@link QueryParser#parse(String) parsing} it
 * into its representation as a {@link SearchQuery}.
 *
 * @author marco
 * @see SimpleQueryParser
 */
public interface SearchService {

  /**
   * Mostly of interest for testing and debugging purposes.
   *
   * @return the template used to build the JSON query object from the internal
   *    {@link SearchQuery query representation}.
   */
  @NotNull String getQueryTemplate();

  /**
   * Used to build the actual JSON representation of the Elasticsearch API query from the
   * internal representation as a SearchQuery object.
   *
   * <p>Can also be used for diagnostic and debugging purposes.
   *
   * @param query the query we wish to run against Elasticsearh.
   * @return the JSON representation after converting {@literal query}
   */
  @NotNull
  JsonObject buildJsonQuery(@NotNull SearchQuery query);

  /**
   * Executes the query against the search engine and returns the results.
   *
   * @param query the query to run
   * @return the results of the query, if any.
   */
  @NotNull
  SearchResults search(@NotNull SearchQuery query);

  /**
   * Find a document data by ID.
   *
   * @param id the ID of the document.
   * @param index the name of the index under which the document was stored by ES.
   * @param clazz required by the {@link com.fasterxml.jackson.databind.ObjectMapper mapper} to
   *              parse the returned JSON.
   * @param  <T> the type of the document returned (either an artifact or a library).
   * @return if found, the library model; an empty optional if not.
   */
  <T> Optional<T> getDocumentById(@NotNull Long id, String index, Class<T> clazz);

  /**
   * Sends a JSON query directly to the Elasticsearch service and returns the JSON response.
   *
   * @param query a search query formulated using Elasticsearch's custom DSL.
   * @param documentType an optional filter, will execute the query solely against the given type
   *    of document (can be {@literal null})
   * @return the results of the query, if any.
   */
  SearchResults queryDirect(@NotNull JsonObject query, @Nullable String documentType);
}
