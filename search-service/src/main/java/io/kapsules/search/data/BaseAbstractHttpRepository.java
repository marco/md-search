// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kapsules.search.api.data.Constants;
import io.kapsules.search.api.data.ElasticSearchHttpException;
import io.kapsules.search.api.data.IndexableModel;
import io.kapsules.search.http.ElasticsearchConnectionManager;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static io.kapsules.search.api.data.Constants.MAIN_ES_INDEX;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * Base class for the HTTP-based repository classes.
 *
 * @author marco
 */
abstract class BaseAbstractHttpRepository<T extends IndexableModel> implements Repository<T> {
  protected static final Logger LOG = LoggerFactory.getLogger(BaseAbstractHttpRepository.class);
  protected static final ObjectMapper MAPPER = new ObjectMapper();
  protected final ElasticsearchConnectionManager connectionManager;

  BaseAbstractHttpRepository(ElasticsearchConnectionManager connectionManager) {
    this.connectionManager = connectionManager;
  }

  // In a perfect world, we would have had this here:
  //     public class ElasticsearchResponseType {
  //        ...
  //        T source;
  //        ...
  //     }
  // Alas, we live in a world that is, at best, perfectible.
  // We cannot use T in a static context; and Jackson won't deserialize an inner
  // (non-static) class.
  // See the two concrete repository to see for yourself what this means in practice.


  protected abstract String documentType();

  protected String buildEndpoint(Long id) {
    return join("/", MAIN_ES_INDEX, "/", documentType(), "/", id);
  }

  @Override
  public boolean save(T artifact) {
    HttpResponse response = null;
    try {
      String body = MAPPER.writeValueAsString(artifact);
      response = connectionManager.sendRequest(HttpMethod.PUT.name(),
          buildEndpoint(artifact.getId()), body);

      // Elasticsearch will helpfully return a status code that is HTTP compliant.
      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
        LOG.debug("Created new {} '{}'", documentType(), artifact.getId());
      } else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        LOG.debug("Updated {} '{}'", documentType(), artifact.getId());
      } else if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST) {
        throw new ElasticSearchHttpException(documentType(), artifact.getId(), response);
      }
      return true;
    } catch (JsonProcessingException e) {
      LOG.error("Could not convert {}, id = {}, error = '{}'", documentType(), artifact.getId(),
          e.getMessage());
      throw new IllegalStateException("Cannot convert artifact to JSON", e);
    } catch (IOException e) {
      LOG.error("Could not send the object to Elasticsearch server: {}", e.getMessage());
      return false;
    } finally {
      closeHttpResponse(response);
    }
  }

  @Override
  public boolean delete(Long id) {
    HttpResponse response = null;
    try {
      response = connectionManager.sendRequest(HttpMethod.DELETE.name(), buildEndpoint(id),
          (String) null);
      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
        LOG.warn("Attempting to delete non-existing {}, id = {}", documentType(), id);
      }
      return response.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
    } catch (Exception e) {
      LOG.error("Could not delete {}, id = {}, error = '{}'", documentType(), id, e.getMessage());
      return false;
    } finally {
      closeHttpResponse(response);
    }
  }

  @Override
  public Optional<Map<String, ?>> getMetadata(Long id) {
    HttpResponse response = null;
    String url = buildEndpoint(id) + Constants.SOURCE_EXCLUDE_MODEL;

    try {
      response = connectionManager.get(url);
      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        Map<String, Object> metadata = MAPPER.readValue(response.getEntity().getContent(),
            new TypeReference<Map<String, Object>>() { });
        LOG.debug("Retrieved metadata for id = {}, metadata = {}", id, metadata);
        return Optional.of(metadata);
      }
    } catch (IOException e) {
      LOG.error("GET for metadata failed, error =  '{}'", e.getMessage());

    } finally {
      closeHttpResponse(response);
    }
    return Optional.empty();
  }


  /**
   * Helper to close any http response that has been opened; failure to do so will cause an error
   * due the pool resources exhaustion (and a timeout error).
   *
   * @param response the response to close, it can be null or empty.
   */
  private void closeHttpResponse(HttpResponse response) {
    if (response != null) {
      EntityUtils.consumeQuietly(response.getEntity());
    }
  }
}
