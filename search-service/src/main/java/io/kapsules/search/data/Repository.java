// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import java.util.Map;
import java.util.Optional;

/**
 * This maps 1/1 to Spring Data repository interface, which we will not be using as it is not
 * compatible with Elasticsearch 5.
 *
 * @author marco
 */
public interface Repository<T> {

  boolean save(T artifact);

  Optional<T> findOne(Long id);

  /**
   * If the item exists, it retrieves it from Elasticsearch, without the {@literal "model"} field.
   *
   * @param id the unique ID for the artifact (library/vulnerability).
   * @return the metadata about this entity, but without the actual model data.
   */
  Optional<Map<String, ?>> getMetadata(Long id);

  boolean delete(Long id);
}
