// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.search.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.kapsules.search.api.data.ArtifactModelWrapper;
import io.kapsules.search.http.ElasticsearchConnectionManager;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static io.kapsules.search.api.data.Constants.*;


/**
 * Replaces Spring Data repository handling all CRUD actions towards the ES artifact index.
 *
 * @author marco
 */
@Component
public class ArtifactHttpRepository extends BaseAbstractHttpRepository<ArtifactModelWrapper> {

  // Due to some pecularities of inner non-static classes, Jackson is unable to deserialize them.
  // Further using generics, complicates matters greatly, with the ultimate result that a
  // ClassCastException is thrown.
  // Long story short, instead of a ElasticResponseType<T> in the base abstract class, we will
  // use two (almost) identical static classes here and for the LibraryHttpRepository one too.
  //
  // Such is life in the land of Java and statically typed languages.
  @Data
  @JsonIgnoreProperties(ignoreUnknown = true)
  public static class ElasticsearchResponseType {
    @JsonProperty("_id")
    String id;

    @JsonProperty(SOURCE_KEY)
    ArtifactModelWrapper source;

    @JsonProperty(TYPE_KEY)
    String type;
  }

  public ArtifactHttpRepository(ElasticsearchConnectionManager connectionManager) {
    super(connectionManager);
  }

  @Override
  protected String documentType() {
    return ARTIFACT;
  }

  @Override
  public Optional<ArtifactModelWrapper> findOne(Long id) {
    HttpResponse response = null;
    try {
      response = connectionManager.get(buildEndpoint(id));
      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        ElasticsearchResponseType result = MAPPER.readValue(response.getEntity().getContent(),
            ElasticsearchResponseType.class);
        closeHttpResponse(response);
        if (result != null && id.equals(Long.parseLong(result.getId())) && ARTIFACT.equals(result
            .getType())) {
          // We still want to avoid an NPE, in case the "source" element is null or empty.
          return Optional.ofNullable(result.getSource());
        }
      }
    } catch (Exception e) {
      LOG.error("Could not execute the request for artifact '{}': {}", id, e.getMessage());
    } finally {
      closeHttpResponse(response);
    }
    return Optional.empty();
  }

  // Helper to close any http response that has been opened.
  private void closeHttpResponse(HttpResponse response) {
    if (response != null) {
      EntityUtils.consumeQuietly(response.getEntity());
    }
  }
}
