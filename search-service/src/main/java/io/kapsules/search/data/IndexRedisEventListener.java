// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.data;

import com.google.common.base.Strings;
import io.kapsules.search.api.data.SearchServiceMessage;
import io.kapsules.search.api.data.SearchServiceMessage.IndexType;
import io.kapsules.search.keys.LibraryIndexEntity;
import io.kapsules.search.keys.VulnerabilityIndexEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.RedisKeyExpiredEvent;
import org.springframework.stereotype.Component;

import static io.kapsules.search.api.data.Constants.LIBRARY_KEYSPACE;
import static io.kapsules.search.api.data.Constants.VULNS_KEYSPACE;

/**
 *
 */
@Component
public class IndexRedisEventListener implements ApplicationListener<RedisKeyExpiredEvent<?>> {


  private static final Logger LOG = LoggerFactory.getLogger(IndexRedisEventListener.class);


  private final RabbitOperations indexDebouncedQueueSender;


  @Autowired
  public IndexRedisEventListener(RabbitOperations indexDebouncedQueueSender) {
    this.indexDebouncedQueueSender = indexDebouncedQueueSender;
  }


  /**
   * This event listener waits for a message to expire in redis.
   * When it finds an expiration it will check the 'keyspace' and route the message accordingly.
   */
  @Override
  public void onApplicationEvent(RedisKeyExpiredEvent<?> event) {
    final String keyspace = event.getKeyspace();
    final Object value = event.getValue();

    if (Strings.isNullOrEmpty(keyspace)) {
      LOG.debug("Received event without a keyspace. Ignoring the event");
      return;
    }

    if (value == null) {
      LOG.debug("Another service processed this expired message: ignoring.");
      return;
    }

    LOG.debug("Received expire event on keyspace {} for key = {}.", keyspace, value);
    switch (keyspace) {
      case LIBRARY_KEYSPACE:
        final LibraryIndexEntity libraryIndexEntity = ((LibraryIndexEntity) value);
        indexDebouncedQueueSender.convertAndSend(new SearchServiceMessage(IndexType.LIBRARY,
            libraryIndexEntity.getId()));
        break;
      case VULNS_KEYSPACE:
        final VulnerabilityIndexEntity vulnerabilityIndexEntity = ((VulnerabilityIndexEntity) value);
        indexDebouncedQueueSender.convertAndSend(new SearchServiceMessage(IndexType.ARTIFACT,
            vulnerabilityIndexEntity.getId()));
        break;
      default:
        LOG.warn("Received event with an unknown keyspace ({}). Ignoring the event", keyspace);
    }
  }
}
