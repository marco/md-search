// Copyright (c) 2019 Marco Massenzio. All rights reserved.

package io.kapsules.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import javax.json.*;
import javax.json.stream.JsonGenerator;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <h1>JsonUtils</h1>
 *
 * <p>Utility methods to manipulate JSON can be found here.
 *
 * @author mmassenzio@apple.com, 2/2/17
 */
public class JsonUtils {

  private static final ObjectMapper MAPPER = new ObjectMapper();

  /**
   * Extracts the value from the enclosing JSON, at the given path.
   *
   * <p>If any of the path's segments contains a `.` (e.g., {@literal model.year}),
   * replace it with a `%` sign (that is, use {@literal "bool.match.must.model%year"}).
   *
   * @param json the enclosing JSON object
   * @param path the dot-separated path to the desired value.
   * @return the value, if any
   * @throws ParseException if the path does not exist
   */
  // TODO: if any of the path segments is an int, it should instead descend into an array and get
  //       the n-th object and continue the traversal.
  public static JsonValue get(JsonObject json, String path) throws ParseException {
    List<String> segments = Arrays.stream(path.split("\\."))
        .map(segment -> segment.replace('%', '.'))
        .collect(Collectors.toList());

    JsonValue value = json;
    for (String segment : segments) {
      if (value.getValueType() != JsonValue.ValueType.OBJECT) {
        throw new ParseException("Path " + path + " does not exist in object " + json.toString(), 0);
      }
      value = ((JsonObject) value).get(segment);
      if (value == null) {
        break;
      }

    }
    return value;
  }

  /**
   * Convenience method to convert from a JSON Object to a Java object of type T.
   *
   * <p><strong>Use with care</strong>: so many things can go wrong here.
   *
   * @param json the JSON body
   * @param clazz the type expected that the JSON should be encoding.
   * @param <T> the actual type - Java being Java.
   * @return the Java object that was JSON-encoded, assuming all went well.
   * @throws IOException if Jackson cannot convert the JSON to the desired T type.
   */
  public static <T> T convertFrom(JsonValue json, Class<T> clazz) throws IOException {
    return MAPPER.readValue(json.toString(), clazz);
  }

  /**
   * Utility method to extract an object of type T, embedded a few levels down into an enclosing
   * JSON object.
   *
   * @param json the enclosing JSON
   * @param path the dot-separated path to the desired object (e.g., {@literal hits._source.model}
   * @param clazz the usual Java workaround generics
   * @param <T> the expected type of the object which is the value at the {@literal path}
   * @return all going well, the Java object that was encoded in JSON.
   * @throws ParseException if the {@literal path} does not exist
   * @throws IOException if Jackson cannot decode your JSON into an object of type T
   */
  public static <T> T get(JsonObject json, String path, Class<T> clazz) throws ParseException, IOException {
    return convertFrom(get(json, path), clazz);
  }

  public static String prettyPrint(JsonStructure json) {

    StringWriter stringWriter = new StringWriter();
    Map<String, Boolean> config = Maps.newHashMap();
    config.put(JsonGenerator.PRETTY_PRINTING, true);

    JsonWriterFactory writerFactory = Json.createWriterFactory(config);
    JsonWriter jsonWriter = writerFactory.createWriter(stringWriter);

    jsonWriter.write(json);
    jsonWriter.close();

    return stringWriter.toString();
  }
}
