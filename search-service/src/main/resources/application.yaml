# Application Configuration file.
# Created M. Massenzio, 2016-10-20

spring:
  jackson:
    serialization:
      write_dates_as_timestamps: false


services:
  elasticsearch:
      hostname: "es.kapsules.io"
      port: 80

  maxConnectionsPerRoute: 50
  maxTotalConcurrentConnections: 10
  defaultTimeout: 250


search:
  validateWeights: false
  # We don't need to specify this, as we are currently not validating the weights.
  # balancedWeightsSumTotal: 99.33 (or whatever...)
  rebalanceWeights: false
  maxErrors: 50
  defaultResultsSize: 25

  # Score ranking methodology when combining function_score results.
  # See: https://www.elastic.co/guide/en/elasticsearch/guide/2.x/function-score-query.html
  boostMode: sum
  scoreMode: sum

  # The scoring value to give if the _enhanced field is missing.
  factorIfEnhancedMissing: 0.0

  # The scoring value to give if the _severity field is missing.
  factorIfSeverityMissing: 0.0

  # The scoring value to give if the _popularity field is missing.
  factorIfPopularityMissing: 0.0

  matchFields:
      - name
      - title
      - overview
      - teardown
      - description
      - coordinate1
      - coordinate2
      - artifactComponents.coordOne
      - artifactComponents.coordTwo

  # Fields that no public should be able to see
  excludeFields:
      # Artifact
      - model.artifactLinks
      # Library
      - model.versions.links
      - model.versions.publicMethods

  weights:
    DEFAULT:
        RELEVANCE: 0.1
        TIME: 0.5
        SEVERITY: 0.05
        ENHANCED: 0.35
        POPULARITY: 0.5
        VULN_COUNT: 0.05
        # model.id boost value when we are searching for an exact match on the ID
        EXACT_ID: 20
    LIBRARY:
        RELEVANCE: 0.1
        TIME: 0.4
        SEVERITY: 0.0
        ENHANCED: 0.0
        POPULARITY: 0.5
        VULN_COUNT: 0.5
        EXACT_ID: 20
    ARTIFACT:
        RELEVANCE: 0.1
        TIME: 0.5
        SEVERITY: 0.1
        ENHANCED: 0.3
        POPULARITY: 0.5
        VULN_COUNT: 0.0
        EXACT_ID: 20


  scoring:

    # Decay Functions
    # See: https://www.elastic.co/guide/en/elasticsearch/guide/master/images/elas_1705.png)
    #
    # Scoring based on most recent, uses the "exp" curve to penalize older artifacts.
    #
    #      origin:     current time (as reported by System.currentTimeMillis())
    #      offset:     604800000  -- 1 week (in msec, configurable)
    #      scale:      7776000000 -- 90 days (in msec, configurable)
    recent:
      offset: 604800000
      scale: 7776000000

    # The following field configure "boost" factors if specific fields are present, and are based
    # on the fields' values.
    #
    # The generic form for the query is:
    #
    #   { "field_value_factor": {
    #       "field":"_enhanced", "modifier":"none", "missing":0.0, "factor":2.0
    #     }
    #   }
    #
    # The values are configured here apart from the `factor` which is extracted from the `weights`
    # in the query (or the default values, see above).
    #
    field_value_factors:

      # Ranking by Popularity
      # See: https://www.elastic.co/guide/en/elasticsearch/guide/master/boosting-by-popularity.html
      #
      # Uses the following formula to boost the scoring based on the `_popularity` value:
      #
      #   score = _score + log(1 + wp * _popularity)
      #
      # whether adding or multiplying is driven by the `search.scoreMode` property; `wp` is the value
      # of the `POPULARITY` weight (e.g., "wp:2.3").
      #
      # If `search.max_boost` is greater than 0, it will cap the value at which this factor adds to
      # the overall score.
      #
      -
        field: _popularity
        enabled: true
        modifier: log1p
        missing: 0.0
        weightKey: wp

      # Ranking by Severity
      -
        field: _severity
        enabled: true
        modifier: none
        missing: 0.0
        weightKey: ws

      -
        field: _enhanced
        enabled: true
        modifier: none
        missing: 0.0
        weightKey: we

      # The higher the vuln cont, the higher the score
      -
        field: _vulnCount
        enabled: true
        modifier: none
        missing: 0.0
        weightKey: wv

  # Specify language type to exclude from search result. However if user specifically search
  # for the language eg. language:php, it would overwrite this and show user php results.
  excludeLanguages:
    # - php


management:
  security:
    enabled: false

server:
  tomcat:
    protocol-header: x-forwarded-proto
    port-header: x-forwarded-port
    remote-ip-header: x-forwarded-for
  compression:
    enabled: true
    mime-types: application/json,text/plain

info:
  build:
    artifact:  ${project.artifactId}
    version: "${project.version}+${git.commit.id.abbrev}.${build.number}"  # replaced by the resource filtering in pom.xml

---

# Local development profile - requires the dev-env project to be active
# and all services to be running in containers (./run-dev-environment)
#
spring:
  profiles: dev-local

  # Local RabbitMQ configuration.
  rabbitmq:
    host: localhost
    username: local
    password: local
    virtual-host: /

services:
  elasticsearch:
      hostname: "localhost"
      port: 9200


  maxConnectionsPerRoute: 50
  maxTotalConcurrentConnections: 10
  defaultTimeout: 25

---
spring:
  profiles: qa

  # TODO: should instead point to the QA instance of RMQ in ops2
  rabbitmq:
    host: localhost
    username: local
    password: local
    virtual-host: /


server.port: 9099

services:
  elasticsearch:
      hostname: "es.kapsules.io"
      port: 80
---
spring:
  profiles: prod

logging:
  level:
    root: ERROR
    io.kapsules.search: WARN
