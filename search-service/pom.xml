<!-- Copyright (c) 2019 Marco Massenzio. All rights reserved. -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <artifactId>search-service</artifactId>
  <name>search-service</name>
  <packaging>jar</packaging>

  <parent>
    <groupId>io.kapsules.search</groupId>
    <artifactId>search-parent</artifactId>
    <version>1.0.41-SNAPSHOT</version>
  </parent>

  <properties>
    <!--<main.basedir>${basedir}/../..</main.basedir>-->
    <start-class>io.kapsules.search.RegistrySearchApplication</start-class>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.build.timestamp.format>EEE, dd MMM yyyy HH:mm:ss Z (z)</maven.build.timestamp.format>
    <env>local</env>
  </properties>

  <dependencies>
    <dependency>
      <groupId>javax.json</groupId>
      <artifactId>javax.json-api</artifactId>
    </dependency>

    <dependency>
      <groupId>org.glassfish</groupId>
      <artifactId>javax.json</artifactId>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
    </dependency>

    <dependency>
      <groupId>io.prometheus</groupId>
      <artifactId>simpleclient_spring_boot</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-hateoas</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-amqp</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>

    <!--
      SpringBoot annotations configuration processor
      See: https://github.com/spring-projects/gradle-plugins/tree/master/propdeps-plugin
     -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-configuration-processor</artifactId>
    </dependency>

    <dependency>
      <groupId>com.getsentry.raven</groupId>
      <artifactId>raven-logback</artifactId>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>

    <!-- Test dependencies -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.springframework.restdocs</groupId>
      <artifactId>spring-restdocs-mockmvc</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>io.kapsules.search</groupId>
      <artifactId>search-objects</artifactId>
      <version>1.0.41-SNAPSHOT</version>
      <scope>compile</scope>
    </dependency>
      <dependency>
          <groupId>com.google.code.findbugs</groupId>
          <artifactId>jsr305</artifactId>
          <version>3.0.1</version>
      </dependency>
      <dependency>
          <groupId>javax.inject</groupId>
          <artifactId>javax.inject</artifactId>
          <version>1</version>
          <scope>test</scope>
      </dependency>

  </dependencies>


  <build>
    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
        <includes>
          <include>application*</include>
          <include>logback*.xml</include>
          <include>*.properties</include>
        </includes>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>false</filtering>
        <includes>
          <include>**/*.sql</include>
        </includes>
      </resource>
    </resources>

    <plugins>
      <!--
      ====
      == Inject git commit properties into the build
      == so they will be replaced by "copy-resources"
      =-->
      <plugin>
        <groupId>pl.project13.maven</groupId>
        <artifactId>git-commit-id-plugin</artifactId>
        <version>2.1.11</version>
        <executions>
          <execution>
            <id>git-metadata</id>
            <phase>generate-resources</phase>
            <goals>
              <goal>revision</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <abbrevLength>8</abbrevLength>
          <dateFormat>yyyy-MM-dd'T'HH:mm:SSXXX</dateFormat>
          <failOnNoGitDirectory>false</failOnNoGitDirectory>
          <failOnUnableToExtractRepoInfo>false</failOnUnableToExtractRepoInfo>
          <verbose>false</verbose>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <version>${spring-boot.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>repackage</goal>
            </goals>
            <configuration>
              <mainClass>${start-class}</mainClass>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
          <compilerArgs>
            <arg>-Xlint:all</arg>
          </compilerArgs>
          <showWarnings>true</showWarnings>
          <showDeprecation>true</showDeprecation>
          <encoding>${project.build.sourceEncoding}</encoding>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>2.8.2</version>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
      </plugin>

    </plugins>
  </build>

</project>
