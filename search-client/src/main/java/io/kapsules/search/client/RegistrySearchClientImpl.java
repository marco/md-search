// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.kapsules.search.api.data.Constants;
import io.kapsules.search.api.data.SearchResults;
import org.springframework.data.domain.Pageable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static io.kapsules.search.api.data.Constants.QUERY_PARAM_KEY;


public class RegistrySearchClientImpl implements RegistrySearchClient {

  private static final ObjectMapper JSON = new ObjectMapper();

  private final RestTemplate restTemplate;
  private final String searchUrl;

  public RegistrySearchClientImpl(URI serverUrl) {
    this.restTemplate = new RestTemplate();
    this.searchUrl = serverUrl + Constants.SEARCH_ENDPOINT;
  }

  public RegistrySearchClientImpl(RestTemplate restTemplate, URI serverUrl) {
    this.restTemplate = restTemplate;
    this.searchUrl = serverUrl + Constants.SEARCH_ENDPOINT;
  }

  @Override
  public SearchResults search(String query, boolean publicView, Pageable pageable) {
    final URI endpoint = UriComponentsBuilder.fromHttpUrl(searchUrl)
            .queryParam(QUERY_PARAM_KEY, query)
            .queryParam("publicView", publicView)
            .queryParam("page", pageable.getPageNumber())
            .queryParam("size", pageable.getPageSize())
            .build().toUri();

    return restTemplate.getForObject(endpoint, SearchResults.class);
  }
}
