// Copyright (c) 2019 Marco Massenzio. All rights reserved.
package io.kapsules.search.client;

import io.kapsules.search.api.data.SearchResults;
import org.springframework.data.domain.Pageable;

/**
 *
 */
public interface RegistrySearchClient {

  /**
   * This basically calls the same endpoint as search. Except that it will hide all information
   * except what registry/search2 page needs to know.
   * @param query query string
   * @param publicView if set to true, hides artifacts not in released stage, also will hide
   *                   certain attributes from library and artifact that we do not want to expose
   *                   to public
   * @param pageable page detail
   * @return search result paginated
   */
  SearchResults search(String query, boolean publicView, Pageable pageable);

}
