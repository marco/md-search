# Registry Search Service

Project     | How-To Indexing service
:---        | ---
Author      | [M. Massenzio](m.massenzio@gmail.com)
Version     | 1.0.25
Created     | 2016-10-20
Updated     | 2019-01-02

---

__Contents__

1. [Goals](#goal)
1. [Architecture](#architecture)
1. [Deployment](#deployment)
    - [Running the Registry-Search Service](#running-the-registry-search-service)
1. [Indexing data](#indexing-data)
1. [Testing](#testing)
    - [Lint & Unit tests](#lint-&-unit-tests)
    - [Functional tests](#functional-tests)
    - [Integration Tests](#integration-tests)
1. [Search Basics](#search-basics)
    - [Relevance Matching](#relevance-matching)
    - [Filters](#filters)
    - [Scoring Weights](#scoring-weights)
    - [Combining Scores](#combining-scores)
1. [REST API](#rest-api)
    - [Index](#index)
    - [Search](#search)
    - [Queue](#queue)
- [References](#references)
- [APPENDIX 1 - JSON Query](#appendix-1---json-query)
- [APPENDIX 2 - Functional Test Containers Configuration](#appendix-2---functional-test-containers-configuration)

---

# Goal

This service exposes a simple Search API that connects to an ElasticSearch backend to enable user searches across Libraries and Vulnerabilities (Artifacts).

# Architecture

This is a [Spring Boot](http://spring.io) application that uses Spring REST to expose a simple API at the frontend and Spring Repositories to connect to ES at the backend; the service is stateless and fully horizontally scalable (there are no "sticky" user sessions).

The storage and search engine is [Elasticsearch](https://www.elastic.co) and the queries in the syntax are translated into "native" ES queries and the results are returned in a JSON format compatible with existing systems.

The service also connects to a [RabbitMQ](https://www.rabbitmq.com) messaging backend: this
triggers updates to the indexes.

---

# Running the Registry-Search Service

## Before you start

### Running Elasticsearch (ES)

The easiest way, as usual, is to run it inside a Docker container:

    $ docker run --rm -d -v $HOME/local/esdata:/usr/share/elasticsearch/data \
        -p 9200:9200 --name elasticsearch -h elasticsearch elasticsearch:5

notice the `--name` and `--host` clauses: if you run the RS API server inside a container,
remember to use the `--link elasticsearch` option and then configure the
`-Dservices.elasticsearch.hostname=elasticsearch` property.


### Building as a Container

Use the `Dockerfile` and ideally match the `version` in the `POM` (see the `Version` tag at the top of this document or `pom.xml`):

    $ docker build -t md-search:$VERSION .

### Running the service

By far the easiest, during development, is to use the IntelliJ `RegistrySearchApplication` run
profile (under `.idea/runConfigurations`).

However, RS should really be run inside a container:

    $ docker run --rm -d --name registry-search --link elaticsearch \
        -e SERVICES_ELASTICSEARCH_HOSTNAME=elaticsearch \
        -p 8080:8080 registry-search:0.2.1

the RS server will then be available at [http://localhost:8080/search?q=sql+injection]().

__Before__ executing search queries, though, __you need to load the data to be searched__.


## Indexing data

To load the libraries / artifacts to be searched, we need to use the `/index/{collection}` endpoint (see also [REST API](#REST_API) below for more details).

You can either use [Postman](https://www.getpostman.com/) and upload the two collections in the 
`/api-collections` folder.

Alternatively, use something like `curl` to send a request such as:

    POST /index/artifact

    [
        292,294,298,350,374,377,437,
        ...
        575,576,577,578,579,581 ...
    ]

(the Postman collection already has all the "valid" indexes, however, you can load any number of them and it will simply ignore the ones that don't match an existing ID).

Similarly for the libraries:

    POST /index/library

    [
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16, 17, 18,
        ...
        998, 999
    ]

the response will contain a list of all the valid/missing IDs.

#### Using RMQ

If you have a running instance of RMQ locally, you can also use the `python/rmq.py` script - run this for more details:

    $ ./python/rmq.py --help

# Testing

## Lint & Unit tests

We strongly encourage developers to install git hooks that will run a set of local tests (linters & unit) in addition to building the project prior to every commit:

    ln -s ../../scripts/hooks/pre-commit .git/hooks/pre-commit

These are standalone tests that can be run without any other service running.

## Functional tests

### Running functional tests

These require a running Elasticsearch instance, loading a "known good" data set and a running RMQ (it won't be exercised by the tests, but it is required for RS to run without complaining about a missing connector).

Running the tests is as simple as:

    $ ./scripts/functional

While these are not mandatory prior to every commit (they are slightly more expensive to run than unit tests) it is __strongly__ recommended that they are run __prior__ to pushing to github and submitting a PR: please avoid if you can to break the build - where's the fun in that?

#### Building the Test Data

The "known good" data set is kept in the `python/testdata.json` file, and is uploaded to ES via the `python/load_test_data.py` script; however, this data is not maintained manually, but it is rather derived from a list of IDs of Libraries/Vulnerabilities, maintained in `python/test_ids.json`.

This latter file must be manually maintained and IDs must be added/removed as needed when adding or editing existing functional tests (those are typically classes named `**/*IT.java`).

After any modification to the IDs list, the `python/reindex_testdata.py` script must be run:

    $ ./python/reindex_testdata.py

this will do the needful and, if necessary, will backup the existing test data before overwriting it.

**Rebuilding the IDs List**
This should _never_ be required, but if, for whatever reason `test_ids.json` gets lost or corrupted, use `python/rebuild_ids.py` to restore it from the test data - the process is imperfect and based on heuristics; so keep an eye on tests  afterwards.

#### Verifying the Test Process

In order to verify that the process of building the container and loading the test data progresses as intended, there is a `python/buildrun` script that does exactly that: run an ES container, extracts the data from the JSON files and then uploads it to it: the console output should provide a good indication as to whether it all went according to plan.

Additionally, by running it like this:

    $ ./python/buildrun -k

with a `-k` (`keep`) flag, it will leave the ES container running and mapped to port 9220 on the host (`localhost:9220`) so it can then be used to either verify the data is in the expected state, or even run the functional tests from within IntelliJ (see `.idea/runConfigurations/All_in_registry_search_service.xml`).

## Integration Tests

    TODO: we currently do not have integration tests configured

---

## Relevance Matching

The fundamental search mechanism employed by Elasticsearch is the [term frequency/inverse document frequency](https://www.elastic.co/guide/en/elasticsearch/guide/master/relevance-intro.html) algorithm based on:

>__Term frequency__

>How often does the term appear in the field? The more often, the more relevant. A field containing five mentions of the same term is more likely to be relevant than a field containing just one mention.

>__Inverse document frequency__

>How often does each term appear in the index? The more often, the less relevant. Terms that appear in many documents have a lower weight than more-uncommon terms.

Further, search query terms are "stemmed" and punctuation and other symbols are removed (in other words, `sql-injection`, `sql_injection` and `sql injection` are all equivalent).

A more detailed explanation of the algorithm used by Elasticsearch can be found [here](https://www.elastic.co/guide/en/elasticsearch/guide/master/scoring-theory.html).

For our "multi-match" query on the basic terms (in other words, excluding [filters](#Filters) and [weights](#Weights)) we use the following fields:

```
  matchFields:
      - name
      - title
      - overview
      - teardown
      - description
      - coordinate1
      - coordinate2
      - artifactComponents.coordOne
      - artifactComponents.coordTwo
```

These are configured in the `resources/application.yaml` file.

The TF;IDF score (multiplied by the [relevance weight (`wr`)](#relevance)) forms the initial basis to rank the results returned by ES.

## Filters

A query range may be further limited by the use of _filters_ (as shown above, in the `"filter"` clause), of the form:

    filter:value

for example:

    date parsing language:java license:MIT

We have currently defined the following _filters_:

      TYPE, LANGUAGE, LICENSE, SOURCE,
      SEVERITY, VULNERABLE_METHOD, ENHANCED,
      CVE_YEAR, CVE_ID, STAGE

which map, respectively, to the following fields:

Filter               | Model field
---                  | ---
  type               | _index_ (either `library` or `vulnerability`)
  language           | `languageType`, `language`
  license            | `versions.licenseInfoModels.license`
  source             | `coordinateType` (e.g., "MAVEN" or "BOWERS")
  stage              | Reserved for internal use (5)


(2) We simply use the non-emptiness of "boolean" filters as the criterion: while we do require that the filter is formatted as shown above, the actual value will be ignored.

In other words:

    enhanced:false

will have the exact opposite effect that one would expect.

(3) we assume that a vulnerability is "enhanced" if the `"teardown"` field is non-empty.

(4) Will filter out all libraries __without__ any known vulnerabile method (i.e., only those with at least __one__ vulnerable method will be returned).

(5) used something like `stage:DRAFT` to filter in only those docs which are at the given state
---

Note that some of these filters may be incompatible: for example it does not make sense to filter for `severity:7 type:library`; in this case, the query will simply return an empty resultset.


---

## Scoring Weights

_Note_
>this was initially based on the [Search Ranking document](https://goo.gl/FdIFNH), but it has eventually evolved beyond the scope of that document; more importantly, we no longer restrict the weights to add up to 1.0.

Elasticsearch DSL allows to modify the results' scores by adding multiple criteria (the [filters](#Filters) explained above) and further fine-tune the rankings by adding "boost factors" (see the [Appendix](#Appendix) to see what this means in practice).

They are used in a query like this:

    json api type:library wr:3 wt:0.5 wp:10

the meaning of the above will be explained in the following.

First off, the meaning of the weights:

```
wr  relevance       adjusts the "raw" score (based of IF/TDF Elasticsearch algorithm)
wt  time            used to give more (or less) importance to newly discovered vulnerabilities
we  enhanced        "boost" factor for those vulnerabilities that have a non-empty "teardown" (base: either 1 or 0)
ws  severity        multiplier for the `_severity` field (only applies to vulnerabilities)
wp  popularity      1p factor (see below) for the `_popularity` field (only applies to libraries)
```

The default values are configured as follows (in the `application.yaml` configuration file):

```
  defaultWeights:
      RELEVANCE: 3.0
      TIME: 0.4
      SEVERITY: 0.5
      ENHANCED: 0.0
      POPULARITY: 0.5
```

### Boosting Relevance

The base `_score` can be further boosted (or dampened) by using the `wr` factor: this is simply applied as a scaling factor to the raw TF/IDF score:

    new_score = wr * _score

This is the resulting JSON:

```
"query": {
        "bool": {
          "should": [
            {
              "multi_match": {
                "query": "api csv",
                "fields": [
                  "model.name",
                  "model.title",
                  "model.overview",
                  "model.teardown",
                  "model.description",
                  "model.artifactComponents.coordOne",
                  "model.artifactComponents.coordTwo"
                ]
              }
            }
          ],
          "boost": wr,
```

as you can see the fields we "multi-match" against are those defined in the `application.yaml` file (see [Relevance Matching](#Relevance_Matching)]

### Popularity Ranking

The ["Popularity" ranking](https://www.elastic.co/guide/en/elasticsearch/guide/master/boosting-by-popularity.html) we employ uses the `log1p` formula with a multiplication factor (the `wp` weight) and a `sum` "boost mode":

```
"field_value_factor": {
    "field": "_popularity",
    "modifier": "log1p",
    "missing": 0.0,
    "factor": 0.5
}
...
"boost_mode": "sum"
```
resulting in an adjusted score `new_score` equal to:

     new_score = _score + log(1 + wp * _popularity)


### Time Relevance

Under the assumption that most recent vulnerabilities are more of interest of old ones, we weight more highly vulnerabilities whose `disclosureDate` is most recent (that field is extracted into the `_when` metadata field):

```
  "exp": {
    "_when": {
      "origin": <<time of query>>,
      "offset": 604800000,
      "scale": 7776000000
    }
  ...
  "weight": wt
```

This is based on the concept of [decay functions](https://www.elastic.co/guide/en/elasticsearch/guide/2.x/decay-functions.html) and uses the `exponential` function, with the following parameters:

![Decay Functions](https://www.elastic.co/guide/en/elasticsearch/guide/2.x/images/elas_1705.png)

    origin:     current time (as reported by System.currentTimeMillis())
    offset:     604800000  -- 1 week (in msec, configurable)
    scale:      7776000000 -- 90 days (in msec, configurable)

In other words, all vulnerabilities discovered in the past week will have their score increased by the same amount; and those around 3 months old will only add half: the maximum amount that we will add for "recency" is the weight `wt`.

### Severity

This simply scales the `_severity` field's numeric value (see the explanation for the [`severity` filter](#Filters) for the fields used in computing this value).

### Enhanced Vulnerabilities

We derive a binary "raw" value (1.0 or 0.0) for vulnerabilities, based on whether they have (or not) a non-empty `"teardown"` field, and then we scale that by the `we` weight.

## Combining Scores

All the various function scores, adjusted by their weights, are then finally combined together by summing them, as defined in:

    "score_mode": "sum"

the resulting score is then used to rank the results with are returned in descending order, starting from the `"offset"`-th result and at most `"size"` of them.

---

# REST API

The frontend exposes a simple REST API that enables a client to submit a string-based query (such as `commons language:java license:apache`) which will be converted into a suitable ElasticSearch query and the JSON results returned with minimal filtering.

## Index

In order to conduct searches, we need to upload and "index" the data; in order to do so, the `/index` endpoint can be used to instruct the `registry-service` to fetch the data from the Platform endpoint(s) and upload it to Elasticsearch servers.

> __NOTE__ In the following, and across all this API, the `{id}`s used are those as known to the Platform API (in fact, they are the IDs as known to the Postgres DB).

There are two virtually identical endpoints for libraries and vulnerabilities ("artifacts") which expose both a `POST` and a `GET` method:

#### Fetch and Index

    POST /index/library

    [ 1234, 99876, 1055]

will fetch the given Documents whose IDs match, and will push them to ES for indexing.

Response:

    200 OK

```json
{
  "SUCCESS": [ 1234, 99876, 1055 ],
  "FAILURE": [],
  "TIMEOUT": []
}
```

And similarly for artifacts:

    POST /index/artifact

    [1054, 876, 87654321]

Response:

    200 OK

```json
{
  "SUCCESS": [ 876, 1054 ],
  "FAILURE": [ 87654321 ],
  "TIMEOUT": []
}
```

> __NOTE__ We cannot return a `201 CREATED` as this requires also a `location` header with a URI to be returned, which in this case would not be appropriate, as there are in principle multiple URIs (one for each ID) that would need to be returned.

#### Retrieve One

More for debugging and testing purposes, there is also a `GET` endpoint to retrieve an individual library/artifact which may have been previously indexed: this should really only be used when search results fail to return an expected result and we need to verify that the data has, indeed, been uploaded correctly.

    GET /index/library/24521

will return:

    200 OK

```json
{
    "links": { },
    "id": 24521,
    "name": "betfair-api",
    "description": "A library for interacting with the Betfair API",
    "creationDate": 1450740501217,
    "updatedDate": 1477269192235,
    "author": "mattgi",
    "authorUrl": "https://github.com/mattgi/",
    ...
    }
    ...
}
```

and, obviously, a `404 NOT FOUND` if the library's ID cannot be found.

Artifacts work exactly the same way:

    GET /index/artifact/678

## Search

### Search Query

This is the core of the service, and is accessed via the `/search` endpoint with an optional (but probably important) `q` query argument and, additonally "pagination" ones (`offset` and `pageSize`):

    GET /search?q=apacheds%20type:library%20language:java%20license:apache

would return a list of results of the following form:

```json
{
  "metadata": {
    "hits": 216,
    "page": 1,
    "pageSize": 25,
    "totalPages": 9,
    "libraries_count": 25,
    "artifacts_count": 0,
    "high_score": 23.845007,
    "low_score": 11.742828
  },
  "contents": [
    {
      "type": "library",
      "hitScore": 23.845007,
      "_enhanced": 0,
      "_severity": null,
      "_vulnerableMethods": false,
      "model": { ... }
      "id": 263,
      "_version": null,
      "_vulnCount": null,
      "_popularity": null,
      "_when": "2016-10-22T14:33:06.571+0000"
    },
    {
      "type": "library",
      "hitScore": 21.413778,
      "_enhanced": 0,
      "_severity": null,
      "_vulnerableMethods": false,
      "model": { ... }
      ...
    },
    ...
  ]
}
```

### Direct JSON Post

There may be instances in which we may want to execute a search against the Elasticsearch server, using ES DSL query language directly (this may typically be necessary when diagnosing a reported issue, or just exploring new options).

In this case, we can directly `POST` the JSON to RS and it will be "passed-through" and the results returned in a format compatible with our own internal representation.

For example:

```json

POST http://localhost:8080/search

{
  "query": {
    "bool": {
      "should": [
        {"match": {"model.name": "sql injection" } },
        {"match": {"model.title": "sql injection" } },
        {"match": {"model.overview": "sql injection" } },
        {"match": {"model.teardown": "sql injection" } },
        {"match": {"model.description": "sql injection" } }
      ],
      "filter": [
      	 { "match": { "model.versions.licenseInfoModels.name": "BSD3" } }
      ]
    }
  },
  "from": 0,
  "size": 10,
  "_source": ["model.name", "model.description", "model.versions.*"]
}
```

would return:

```json
{
  "metadata": {
    "hits": 67,
    "page": 1,
    "pageSize": 10,
    "totalPages": 7,
    "libraries_count": 10,
    "artifacts_count": 0,
    "high_score": 11.6826725,
    "low_score": 0
  },
  "contents": [
    {
      "type": "library",
      "hitScore": 11.6826725,
      "_enhanced": 0,
      "_severity": null,
      "_vulnerableMethods": false,
      "model": {
        "links": {},
        "id": null,
        "name": "BBQSQL",
      ...
    },
    ...
  ]
}
```

Please be aware that __the contents of the posted JSON are not filtered or analyzed at all__ so errors and any other misconfiguration will most likely result in meaningless results.


### Get Document

If the ID (the internal unique identifier used in the PostgreSQL Librarian and Vulnerabilities services) is known, the corresponding documents stored in Elasticsearch can be retrieved via the following endpoints:

    GET /search/library/1234
    GET /search/artifact/9876

either way the resulting document is returned; for example:

```json
GET http://localhost:8080/search/library/24521

200 OK

{
  "links": {},
  "id": 24521,
  "name": "betfair-api",
  "description": "A library for interacting with the Betfair API",
  "creationDate": "2015-12-21T23:28:21.217+0000",
  "updatedDate": "2016-10-24T00:33:12.235+0000",
  "author": "mattgi",
  "authorUrl": "https://github.com/mattgi/",
  "languageType": "JS",
  "coordinateType": "NPM",
  "coordinate1": "betfair-api",
  ...
}
```


## Queue

Accessible at the `/queue` endpoint, it is meant for internal use where we might need to reindex the library or artifact. This endpoint helps to put messages into the rabbitMQ queue to be indexed later.

It only supports `POST` requests for either libraries or artifacts, respectively:

    POST /queue/library

    [ ... IDs of libraries to queue ... ]

and

    POST /queue/artifact

    [ ... IDs of vulnerabilities to queue ... ]


# References

1. [Search ranking](https://goo.gl/rAHXWb)
2. [Elasticsearch documentation](https://www.elastic.co/guide/en/elasticsearch/reference)
4. [Decay Functions](https://www.elastic.co/guide/en/elasticsearch/guide/master/decay-functions.html)

---

# APPENDIX 1 - JSON Query


Query string:

    apacheds type:library language:java license:apache

is parsed as:

    Terms: apacheds, Filters: {TYPE=library, LANGUAGE=java, LICENSE=apache},
    Weights: {TIME=0.4, RELEVANCE=3.0, SEVERITY=0.5,
        ENHANCED=0.0, POPULARITY=0.5},
    Released only: true, Public view: {}

resulting in the following JSON Query:

```json
{
  "query": {
    "function_score": {
      "query": {
        "bool": {
          "should": [
            {
              "multi_match": {
                "query": "apacheds",
                "fields": [
                  "model.name",
                  "model.title",
                  "model.overview",
                  "model.teardown",
                  "model.description",
                  "model.coordinate1",
                  "model.coordinate2",
                  "model.artifactComponents.coordOne",
                  "model.artifactComponents.coordTwo"
                ]
              }
            }
          ],
          "boost": 3.0,
          "must": [
            {
              "bool": {
                "should": [
                  {
                    "match": {
                      "model.language": "java"
                    }
                  },
                  {
                    "match": {
                      "model.languageType": "java"
                    }
                  }
                ]
              }
            },
            {
              "match": {
                "model.versions.licenseInfoModels.license": "apache"
              }
            }
          ],
          "must_not": [
            {
              "match": {
                "model.stage": "REJECTED"
              }
            },
            {
              "match": {
                "model.stage": "REVIEW"
              }
            },
            {
              "match": {
                "model.stage": "DRAFT"
              }
            },
            {
              "match": {
                "model.stage": "QA"
              }
            }
          ]
        }
      },
      "functions": [
        {
          "exp": {
            "_when": {
              "origin": 1493708215282,
              "offset": 604800000,
              "scale": 7776000000
            }
          },
          "weight": 0.4000000059604645
        },
        {
          "field_value_factor": {
            "field": "_popularity",
            "modifier": "log1p",
            "missing": 0.0,
            "factor": 0.5
          }
        },
        {
          "field_value_factor": {
            "field": "_severity",
            "modifier": "none",
            "missing": 0.0,
            "factor": 0.5
          }
        },
        {
          "field_value_factor": {
            "field": "_enhanced",
            "modifier": "none",
            "missing": 0.0,
            "factor": 0.0
          }
        }
      ],
      "score_mode": "sum",
      "boost_mode": "sum"
    }
  },
  "_source": {
    "excludes": [
      "model.artifactLinks",
      "model.versions.links",
      "model.versions.publicMethods"
    ]
  },
  "from": 0,
  "size": 25
}
```

For more example queries see either the `sample_queries.json` file or the sample queries saved in the Postman collections (in the `/api-collections` folder).

# APPENDIX 2 - Functional Test Containers Configuration


__Python base image__

    $ cd python && docker build -t python:3.5 -f Dockerfile .

Remember to add the private registry to the `insecure-registries` table in Docker's Preferences.

__Python test runner container__

    # Build the container that will upload the test data
    $ cd python && docker build \
        -t rs-functional-tests:0.1 \
        -f Dockerfile-functional .

    # Run it against an existing and running Elasticsearch container (ES_HOST)
    $ docker run --rm --link ${ES_HOST} \
        -e ES_HOST=${ES_HOST} \
        -e ES_PORT=9200 \
        rs-functional-tests:0.1



You can optionally run the Docker Web UI from [here](https://github.com/kwk/docker-registry-frontend):

    docker run -d -e ENV_DOCKER_REGISTRY_HOST=... \
        -e ENV_DOCKER_REGISTRY_PORT=5000 \
        -e ENV_DOCKER_REGISTRY_USE_SSL=1 \
        -p 8088:80 \
        konradkleine/docker-registry-frontend:v2
